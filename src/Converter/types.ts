import { Content, PhrasingContent } from 'mdast';
import { Descendant, Element as SlateElement, Text as SlateText } from 'slate';
import { BlockFormatEnum } from '../RichEditor/baseTypes';

type SlateTextDecoration = Omit<SlateText, 'text'>;
type SlateBlockDecoration = Omit<SlateElement, 'type' | 'children'>;

export type ConvertDecoration = {
  text?: SlateTextDecoration;
  block?: SlateBlockDecoration;
};

export type MdastNodeType = Pick<Content, 'type'>['type'];

export type TRemarkToSlateConverter = (children: Content[], decoration?: ConvertDecoration) => Descendant[];

export type TElementToRemarkConverter = (element: Descendant[]) => Content[];

export type TextToRemarkConvertorInfo = {
  mdastNodeType: Pick<PhrasingContent, 'type'>['type'];
  check: (source: SlateText) => boolean;
  modifier?: (source: SlateText, node: PhrasingContent) => PhrasingContent;
}

export type ElementToRemarkConvertorInfo = (element: Descendant, next: TElementToRemarkConverter) => Content[];

export type ElementConvertors = {
  fromRemark: null | {
    mdastNodeType: MdastNodeType | MdastNodeType[];
    /**
     * Additional check if we need to use this converter.
     * For example, we need additional checking in case if node type is container directive,
     * we need to check directive name
     * */
    check?: (source: Content) => boolean;
    convert: (
      source: Content,
      next: TRemarkToSlateConverter,
      decoration: ConvertDecoration
    ) => Descendant | Descendant[];
  };
  toRemark: null | TextToRemarkConvertorInfo | ElementToRemarkConvertorInfo;
}

export type FromRemarkConverters = {
  [nodeType in MdastNodeType]?: Array<Exclude<ElementConvertors['fromRemark'], null>>
}
export type ToRemarkConverters = {
  text: TextToRemarkConvertorInfo[];
  elements: {
    [elementType in BlockFormatEnum]?: ElementToRemarkConvertorInfo;
  }
}

export type Converters = {
  fromRemark: FromRemarkConverters;
  toRemark: ToRemarkConverters;
};
