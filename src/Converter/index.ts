import { Descendant, Node as SlateNode } from 'slate';
import { Plugin, unified } from 'unified';
import remarkDirective from 'remark-directive';
import remarkGfm from 'remark-gfm';
import { Node as UnifiedNode } from 'unified/lib';
import remarkParse from 'remark-parse';
import rehypeParse from 'rehype-parse';
import rehypeRemark from 'rehype-remark';
import remarkStringify from 'remark-stringify';
import { constructConverters, createRemarkToSlateCompiler, createSlateToRemarkCompiler } from './helpers';
import { RichEditorPlugin } from '../RichEditor/plugins/types';
import { BlockFormatEnum } from '../RichEditor/baseTypes';

interface ConverterConfig {
  plugins: RichEditorPlugin[];
}

export default class Converter {
  readonly #remarkToSlateCompiler: Plugin<unknown[], UnifiedNode, Descendant[]>;
  readonly #slateToRemarkCompiler: Plugin<unknown[], Descendant, UnifiedNode[]>;

  constructor(config: ConverterConfig) {
    const converters = constructConverters(config.plugins);

    this.#remarkToSlateCompiler = createRemarkToSlateCompiler(converters.fromRemark);
    this.#slateToRemarkCompiler = createSlateToRemarkCompiler(converters.toRemark);
  }

  fromRemark(remarkString: string): Descendant[] {
    if (!remarkString) {
      return [{
        type: BlockFormatEnum.Paragraph,
        children: [{ text: '' }],
      }];
    }

    return unified()
      .use(remarkParse)
      .use(remarkGfm)
      .use(remarkDirective)
      .use(this.#remarkToSlateCompiler)
      .processSync(remarkString)
      .result;
  }

  toRemark(slateValue: Descendant[]): string {
    const toRemarkProcessor = unified()
      .use(remarkGfm)
      .use(remarkDirective)
      .use(remarkStringify)
      .use<unknown[], UnifiedNode, string>(this.#slateToRemarkCompiler);

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    const mdastTree = toRemarkProcessor.runSync({ type: 'root', children: slateValue });
    return toRemarkProcessor.stringify(mdastTree);
  }

  async fromHtml(htmlString: string): Promise<Descendant[]> {
    if (!htmlString) {
      return [{
        type: BlockFormatEnum.Paragraph,
        children: [{ text: '' }],
      }];
    }

    const remarkString = await unified()
      .use(rehypeParse)
      .use(rehypeRemark)
      .use(remarkStringify)
      .process(htmlString);

    return this.fromRemark(String(remarkString));
  }

  // eslint-disable-next-line class-methods-use-this
  toPlain(slateValue: Descendant[]): string {
    return slateValue.map((node) => SlateNode.string(node)).join('\n');
  }
}
