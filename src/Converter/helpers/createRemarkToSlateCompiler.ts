import { Plugin } from 'unified';
import { Node as UnifiedNode } from 'unified/lib';
import { Descendant } from 'slate';
import { Content, Parent } from 'mdast';
import { ConvertDecoration, FromRemarkConverters } from '../types';
import { BlockFormatEnum } from '../../RichEditor/baseTypes';

class Builder {
  readonly #nodeConverters: FromRemarkConverters;

  constructor(nodeConverters: FromRemarkConverters) {
    this.#nodeConverters = nodeConverters;

    this.buildSlateNode = this.buildSlateNode.bind(this);
    this.convertNodes = this.convertNodes.bind(this);
    this.buildSlateRoot = this.buildSlateRoot.bind(this);
  }

  buildSlateNode(node: Content, decoration: ConvertDecoration): Descendant[] {
    if (node.type === 'text') return [{ text: node.value, ...decoration.text }];

    if (node.type === 'paragraph') {
      // Check if it's line with only space (blank line)
      const children = node.children.length === 1 && node.children[0].type === 'text' && node.children[0].value === ' '
        ? [{ text: '' }]
        : this.convertNodes(node.children, decoration);

      return [{
        ...decoration.block,
        type: BlockFormatEnum.Paragraph,
        children,
      }];
    }

    const converters = this.#nodeConverters[node.type];
    if (converters) {
      const converter = converters.length === 1 && typeof converters[0].check !== 'function'
        ? converters[0]
        : converters.find(({ check }) => check?.(node));

      if (!converter) return [];

      const convertingRes = converter.convert(node, this.convertNodes, decoration);
      return Array.isArray(convertingRes) ? convertingRes : [convertingRes];
    }

    return [];
  }

  convertNodes(nodes: Content[], decoration: ConvertDecoration = {}): Descendant[] {
    if (nodes.length === 0) {
      return [{ text: '', ...decoration.text }];
    }

    return nodes.reduce<Descendant[]>((res, child) => {
      res.push(...this.buildSlateNode(child, decoration));
      return res;
    }, []);
  }

  buildSlateRoot(root: Parent): Descendant[] {
    return this.convertNodes(root.children);
  }
}

export default (
  nodeConverters: FromRemarkConverters,
): Plugin<unknown[], UnifiedNode, Descendant[]> => function Compiler() {
  const builder = new Builder(nodeConverters);

  this.Compiler = (node: Parent): Descendant[] => builder.buildSlateRoot(node);
};
