import { RichEditorPlugin } from '../../RichEditor/plugins/types';
import { Converters, FromRemarkConverters, ToRemarkConverters } from '../types';
import { BlockFormatEnum } from '../../RichEditor/baseTypes';

const { warn } = console;

export default (plugins: RichEditorPlugin[]): Converters => {
  const fromRemark: FromRemarkConverters = {};
  const toRemark: ToRemarkConverters = {
    text: [],
    elements: {},
  };

  plugins.forEach((plugin) => {
    if ('utilityOnly' in plugin) return;

    plugin.elements?.forEach((element) => {
      if (!element.convertors) {
        warn(`No converters provided for node "${element.formatName}" in plugin "${plugin.name}".`);
        return;
      }

      if (element.convertors.fromRemark) {
        const mdastNodeTypes = typeof element.convertors.fromRemark.mdastNodeType === 'string'
          ? [element.convertors.fromRemark.mdastNodeType]
          : element.convertors.fromRemark.mdastNodeType;

        mdastNodeTypes.forEach((mdastNodeType) => {
          if (!fromRemark[mdastNodeType]) fromRemark[mdastNodeType] = [];

          if (element.convertors.fromRemark) fromRemark[mdastNodeType]?.push(element.convertors.fromRemark);
        });
      } else if (element.convertors.fromRemark !== null) {
        warn(`Missed converter from remark for node "${element.formatName}" in plugin "${plugin.name}". If no needs in converter for this type of node you should specify null.`);
      }

      if (element.convertors.toRemark) {
        if (typeof element.convertors.toRemark === 'function') {
          toRemark.elements[element.formatName as BlockFormatEnum] = element.convertors.toRemark;
        } else if (element.convertors.toRemark.mdastNodeType) {
          toRemark.text.push(element.convertors.toRemark);
        } else {
          warn(`Invalid converter to remark for node "${element.formatName}" in plugin "${plugin.name}".`);
        }
      } else if (element.convertors.toRemark !== null) {
        warn(`Missed converter to remark for node "${element.formatName}" in plugin "${plugin.name}". If no needs in converter for this type of node you should specify null.`);
      }
    });
  });

  return { fromRemark, toRemark };
};
