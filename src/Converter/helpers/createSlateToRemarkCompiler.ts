import { Plugin } from 'unified';
import { Node as UnifiedNode } from 'unified/lib';
import {
  Descendant,
  Element as SlateElement,
  Text as SlateText,
  Node as SlateNode,
} from 'slate';
import {
  Content, Paragraph, Parent, PhrasingContent,
} from 'mdast';
import { ToRemarkConverters } from '../types';
import { BlockFormatEnum } from '../../RichEditor/baseTypes';

class Builder {
  readonly #nodeConverters: ToRemarkConverters;

  constructor(nodeConverters: ToRemarkConverters) {
    this.#nodeConverters = nodeConverters;

    this.buildSlateNode = this.buildSlateNode.bind(this);
    this.convertNodes = this.convertNodes.bind(this);
    this.buildMdastRoot = this.buildMdastRoot.bind(this);
  }

  buildSlateNode(node: Descendant): Content[] {
    if (SlateText.isText(node)) {
      let children: Content = { type: 'text', value: node.text };

      this.#nodeConverters.text.forEach((textConvertationInfo) => {
        if (textConvertationInfo.check(node)) {
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          children = typeof textConvertationInfo.modifier === 'function'
            ? textConvertationInfo.modifier(node, children as PhrasingContent)
            : { type: textConvertationInfo.mdastNodeType, children: [children] };
        }
      });

      return [children];
    }

    if (SlateElement.isElement(node)) {
      if (node.type === BlockFormatEnum.Paragraph) {
        return [{
          type: 'paragraph',
          children: !SlateNode.string(node)
            ? [{ type: 'text', value: ' ' }]
            : this.convertNodes(node.children) as Paragraph['children'],
        }];
      }

      const converter = this.#nodeConverters.elements[node.type];
      if (!converter) return [];

      return converter(node, this.convertNodes);
    }

    return [];
  }

  convertNodes(nodes: Descendant[]): Content[] {
    if (nodes.length === 0) {
      return [{ type: 'text', value: '' }];
    }

    return nodes.reduce<Content[]>((res, child) => {
      res.push(...this.buildSlateNode(child));
      return res;
    }, []);
  }

  buildMdastRoot(slateValue: Descendant[]): Parent {
    return {
      type: 'root',
      children: this.convertNodes(slateValue),
    };
  }
}

export default (nodeConverters: ToRemarkConverters): Plugin<unknown[], Descendant, UnifiedNode[]> => () => {
  const builder = new Builder(nodeConverters);

  return (node: { type: 'root', children: Descendant[] }) => builder.buildMdastRoot(node.children);
};
