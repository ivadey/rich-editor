import { withReact } from 'slate-react';
import {
  createEditor,
  Editor,
  Element,
  Node,
  Transforms,
} from 'slate';
import { RichEditorPlugin } from '../plugins/types';
import withInlineMenu from '../inlineMenuExtension/withInlineMenu';
import { BlockFormatEnum } from '../baseTypes';

const withParagraphs = (editor: Editor) => {
  // Base normalizing rules:
  // 1. paragraphs elements can contain only inline elements or text
  // 2. If value is empty – need to create empty paragraph

  const { normalizeNode } = editor;

  // eslint-disable-next-line no-param-reassign
  editor.normalizeNode = (entry) => {
    const [node, path] = entry;

    if (Editor.isEditor(node) && node.children.length === 0) {
      Transforms.insertNodes(editor, {
        type: BlockFormatEnum.Paragraph,
        children: [{ text: '' }],
      });
    }

    // If the element is a paragraph, ensure its children are valid.
    if (Element.isElement(node) && node.type === BlockFormatEnum.Paragraph) {
      for (const [child, childPath] of Node.children(editor, path)) {
        if (Element.isElement(child) && !editor.isInline(child)) {
          Transforms.unwrapNodes(editor, { at: childPath });
          return;
        }
      }
    }

    // Fall back to the original `normalizeNode` to enforce other constraints.
    normalizeNode(entry);
  };

  return editor;
};

export default (plugins: RichEditorPlugin[]): Editor => {
  let editor = withParagraphs(withInlineMenu(withReact(createEditor())));

  plugins.forEach(({ editorWrapper, normalizeNodes }) => {
    if (typeof editorWrapper === 'function') editor = editorWrapper(editor);
    if (typeof normalizeNodes === 'function') {
      const originalNormalizeNodes = editor.normalizeNode;
      editor.normalizeNode = (entry) => {
        if (!normalizeNodes(editor, entry)) originalNormalizeNodes(entry);
      };
    }
  });

  // todo: check if all plugins provided converter helpers
  // todo: check if all hotkeys in plugins is unique and don't intersect with reserved
  // todo: check if all plugins provided render functions

  return editor;
};
