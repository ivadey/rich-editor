import LocalizedStrings from 'react-localization';
import { IStrings } from './interface';
import ruStrings from './ru';

const strings = new LocalizedStrings<IStrings>({
  ru: ruStrings,
});

export default strings;
