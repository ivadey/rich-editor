/* eslint-disable max-len */
import { IStrings } from './interface';

const ruStrings: IStrings = {
  undo: 'Отменить',
  redo: 'Восстановить',

  normalText: 'Обычный текст',
  headingN: 'Заголовок {0}',

  bold: 'Жирный',
  italic: 'Курсив',
  underline: 'Подчеркивание',
  strikethrough: 'Зачеркнутый',
  subscript: 'Подстрочный текст',
  superscript: 'Надстрочный текст',

  link: 'Ссылка',
  enterLink: 'Укажите ссылку',
  inlineCode: 'Код',

  numberedList: 'Нумерованный список',
  bulletedList: 'Маркированный список',
  checklist: 'Чек лист',

  codeSnippet: 'Блок кода',
  selectLanguage: 'Выберите язык',
  notSelected: 'Не выбрано',
  blockquote: 'Цитата',

  alignLeft: 'По левому краю',
  alignCenter: 'По центру',
  alignRight: 'По правому краю',
  alignJustify: 'По краям',
  wrapLeft: 'Завернуть влево',
  wrapRight: 'Завернуть вправо',

  table: 'Таблица',

  admonition: 'Информационный блок',
  title: 'Заголовок',
  enterText: 'Введите текст',
  note: 'Примечание',
  warning: 'Предупреждение',
  success: 'Успешно',
  error: 'Ошибка',

  mention: 'Упоминание',

  divider: 'Разделитель',

  accordion: 'Спойлер',

  carousel: 'Карусель',
  addImage: 'Добавить изображение',
  removeImage: 'Удалить изображение',

  tooltip: 'Тултип',
  edit: 'Редактировать',
  enterTooltipText: 'Введите текст подсказки',

  typeSomething: 'Начните вводить что-нибудь',
  remove: 'Удалить',

  image: 'Изображение',
  changeCaption: 'Изменить подпись',
  enterImageLink: 'Укажите ссылку на изображение',
  wrongLink: 'Некорректная ссылка',

  anchor: 'Якорь',
  specifyAnchorValue: 'Укажите значение якоря',
  suchAnchorAlreadyExist: 'Такой якорь уже создан',
  anchorSlugCanStartOrEndOnlyWithSymbolOrDigit: 'Значение якоря должно начинаться и заканчиваться с буквы или цифры',
  anchorSlugCanContainOnlySymbolsAndDigits: 'Значение якоря может содержать только буквы латинского алфавита, цифры и знак "-"',
};

export default ruStrings;
