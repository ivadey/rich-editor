export interface IStrings {
  undo: string;
  redo: string;

  normalText: string;
  headingN: string;

  bold: string;
  italic: string;
  underline: string;
  strikethrough: string;
  subscript: string;
  superscript: string;

  link: string;
  enterLink: string;
  inlineCode: string;

  numberedList: string;
  bulletedList: string;
  checklist: string;

  codeSnippet: string;
  selectLanguage: string;
  notSelected: string;
  blockquote: string;

  alignLeft: string;
  alignCenter: string;
  alignRight: string;
  alignJustify: string;
  wrapLeft: string;
  wrapRight: string;

  table: string;

  admonition: string;
  title: string;
  enterText: string;
  note: string;
  warning: string;
  success: string;
  error: string;

  mention: string;

  divider: string;

  accordion: string;

  carousel: string;
  addImage: string;
  removeImage: string;

  tooltip: string;
  edit: string;
  enterTooltipText: string;

  typeSomething: string;
  remove: string;

  image: string;
  changeCaption: string;
  enterImageLink: string;
  wrongLink: string;

  anchor: string;
  specifyAnchorValue: string;
  suchAnchorAlreadyExist: string;
  anchorSlugCanStartOrEndOnlyWithSymbolOrDigit: string;
  anchorSlugCanContainOnlySymbolsAndDigits: string;
}
