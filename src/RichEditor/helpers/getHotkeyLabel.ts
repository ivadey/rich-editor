export default (hotkey: string): string => {
  // eslint-disable-next-line deprecation/deprecation
  const modSymbol = navigator.platform.toUpperCase().indexOf('MAC') > -1 ? '⌘' : 'ctrl';

  // eslint-disable-next-line deprecation/deprecation
  const altSymbol = navigator.platform.toUpperCase().indexOf('MAC') > -1 ? 'option' : 'alt';

  return hotkey.replace('mod', modSymbol).replace('alt', altSymbol);
};
