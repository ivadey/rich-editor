import { Editor } from 'slate';
import { BlockElementDescription, InlineElementDescription, RichEditorPlugin } from '../plugins/types';
import { InlineMenuDescription } from '../inlineMenuExtension';

export interface ParsedPlugins {
  blocks: BlockElementDescription[];
  leafs: InlineElementDescription[];
  hotkeysList: string[];
  hotkeysHandlers: { [key: string]: (editor: Editor) => void };
  formatsToClearOnEnter: BlockElementDescription['formatName'][];
  aboveExitBreaks: BlockElementDescription['formatName'][];
  belowExitBreaks: BlockElementDescription['formatName'][];
  inlineMenuTriggers: InlineMenuDescription[];
  decorators: Required<RichEditorPlugin>['decorate'][];
  onMountedHandlers: Array<(editor: Editor) => void>;
}

const isFunction = (el: unknown) => typeof el === 'function';

export default (editor: Editor, plugins: RichEditorPlugin[]): ParsedPlugins => {
  // todo: add error/warning if some of hotkeys are intersected or use reserved hotkeys such as 'mod+enter'
  const hotkeysList: ParsedPlugins['hotkeysList'] = [];
  let hotkeysHandlers: ParsedPlugins['hotkeysHandlers'] = {};
  plugins.map((p) => p.hotkeys).filter(Boolean).forEach((hotkeys) => {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    hotkeysList.push(...Object.keys(hotkeys!));
    hotkeysHandlers = { ...hotkeysHandlers, ...hotkeys };
  });

  const leafs = plugins
    .map((plugin) => ('elements' in plugin ? plugin.elements : null))
    .filter(Boolean)
    .flat()
    .filter((element) => element?.type === 'inline') as InlineElementDescription[];

  const blocks = plugins
    .map((plugin) => ('elements' in plugin ? plugin.elements : null))
    .filter(Boolean)
    .flat()
    .filter((element) => element?.type === 'block') as BlockElementDescription[];

  const formatsToClearOnEnter = blocks
    .filter((block) => block.clearFormatOnEnter)
    .map((block) => block.formatName);

  const aboveExitBreaks = blocks
    .filter((block) => block.enableAboveExitBreak)
    .map((block) => block.formatName);

  const belowExitBreaks = blocks
    .filter((block) => block.enableBelowExitBreak)
    .map((block) => block.formatName);

  const inlineMenuTriggers = plugins
    .map((plugin) => plugin.inlineMenu?.(editor))
    .filter(Boolean) as InlineMenuDescription[];

  const decorators = plugins
    .map((plugin) => plugin.decorate)
    .filter(isFunction) as ParsedPlugins['decorators'];

  const onMountedHandlers = plugins
    .map((plugin) => plugin.onMounted)
    .filter((onMountedHandler) => typeof onMountedHandler === 'function') as ParsedPlugins['onMountedHandlers'];

  return {
    blocks,
    leafs,
    hotkeysList,
    hotkeysHandlers,
    formatsToClearOnEnter,
    aboveExitBreaks,
    belowExitBreaks,
    inlineMenuTriggers,
    decorators,
    onMountedHandlers,
  };
};
