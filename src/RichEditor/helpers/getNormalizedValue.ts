import { Descendant, Transforms } from 'slate';
import { RichEditorPlugin } from '../plugins/types';
import createCustomEditor from './createCustomEditor';

export default (value: Descendant[], plugins: RichEditorPlugin[]): Descendant[] => {
  const editor = createCustomEditor(plugins);
  Transforms.insertNodes(editor, value);

  return editor.children;
};
