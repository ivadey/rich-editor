import React from 'react';
import Tippy from '@tippyjs/react';
import { useSlate } from 'slate-react';
import Icon, { TIconName } from '../Icon';
import classNames from '../../helpers/classNames';
import { IToolbarButtonProps } from './types';
import getHotkeyLabel from '../../helpers/getHotkeyLabel';

const ToolbarButton: React.FC<IToolbarButtonProps> = ({
  icon, active, tooltip, hotkeyHint, disabled, onClick,
}) => {
  const editor = useSlate();

  let tooltipContent = tooltip;

  if (hotkeyHint) tooltipContent += ` ${getHotkeyLabel(hotkeyHint)}`;

  const isActive = typeof active === 'function' ? active(editor) : active;
  const isDisabled = typeof disabled === 'function' ? disabled(editor) : disabled;

  let content = (
    <button
      type="button"
      className={classNames(
        're-toolbar__button',
        isActive ? 'active' : '',
        isDisabled ? 'disabled' : '',
      )}
      onMouseDown={(e) => {
        e.preventDefault();
        onClick(editor, e);
      }}
      disabled={isDisabled}
    >
      {typeof icon === 'string' ? <Icon icon={icon as TIconName} /> : icon}
    </button>
  );

  if (tooltipContent) {
    content = (
      <Tippy
        content={(
          <span className="re-toolbar__tooltip">
            {tooltipContent}
          </span>
        )}
        placement="bottom"
        duration={[300, 50]}
      >
        {content}
      </Tippy>
    );
  }

  return content;
};

ToolbarButton.displayName = 'ToolbarButton';

export default ToolbarButton;
