import React from 'react';

const ToolbarDivider = () => <div className="re-toolbar__toolbar-divider" />;

ToolbarDivider.displayName = 'ToolbarDivider';

export default ToolbarDivider;
