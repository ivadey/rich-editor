import React from 'react';
import ToolbarDivider from './ToolbarDivider';
import ToolbarButton from './ToolbarButton';
import { IToolbarProps, IToolbarButtonProps } from './types';

const Toolbar: React.FC<IToolbarProps> = ({ className, buttons, position }) => {
  const classes = ['re-toolbar', className];

  if (position === 'bottom') classes.push('bottom-placed');
  else classes.push('top-placed');

  return (
    <div className={classes.join(' ')}>
      {buttons.map((buttonsGroup, ind) => (
        // eslint-disable-next-line react/no-array-index-key
        <React.Fragment key={ind}>
          {buttonsGroup.map((button, subInd) => (
            // eslint-disable-next-line react/no-array-index-key
            <React.Fragment key={subInd}>
              {React.isValidElement(button) ? button : (
                // eslint-disable-next-line react/jsx-props-no-spreading
                <ToolbarButton {...(button as IToolbarButtonProps)} />
              )}
            </React.Fragment>
          ))}

          {ind < buttons.length - 1 ? (
            <ToolbarDivider />
          ) : null}
        </React.Fragment>
      ))}
    </div>
  );
};

Toolbar.displayName = 'Toolbar';

export default Toolbar;
