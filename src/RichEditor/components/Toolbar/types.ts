import { Editor } from 'slate';
import React from 'react';
import { TIconName } from '../Icon';

export interface IToolbarButtonProps {
  icon: TIconName | React.ReactNode;
  active?: boolean | ((editor: Editor) => boolean);
  disabled?: boolean | ((editor: Editor) => boolean);
  tooltip?: string;
  hotkeyHint?: string;
  onClick: (
    editor: Editor,
    event: React.MouseEvent<HTMLButtonElement> | React.KeyboardEvent<HTMLButtonElement>
  ) => void;
}

export interface IToolbarProps {
  className?: string;
  buttons: Array<Array<React.ReactNode | IToolbarButtonProps>>;
  position?: 'top' | 'bottom';
}
