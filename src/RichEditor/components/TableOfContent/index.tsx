import React from 'react';
import { classNames } from '../../helpers';
import { TableOfContentInfo } from './types';

interface TableOfContentProps {
  className?: string;
  itemClassName?: string;
  tableOfContent: TableOfContentInfo[];
}

const TableOfContent: React.FC<TableOfContentProps> = ({
  className,
  itemClassName,
  tableOfContent,
}) => {
  let minLevel = tableOfContent[0].level;
  tableOfContent.forEach((item) => {
    if (item.level < minLevel)minLevel = item.level;
  });

  return (
    <ul className={classNames(className, 're-table-of-contents')}>
      {tableOfContent.map((heading) => (
        <li
          key={heading.slug}
          className={classNames('re-table-of-contents__item', itemClassName)}
          style={{ marginLeft: `${heading.level - minLevel}rem` }}
        >
          <a href={`#${heading.slug}`}>{heading.text}</a>
        </li>
      ))}
    </ul>
  );
};

TableOfContent.displayName = 'TableOfContent';

export default TableOfContent;
