import { getTextSlug } from '../../helpers';
import { TableOfContentInfo } from './types';

const headingsTags = [
  { tagName: 'h1', level: 1 },
  { tagName: 'h2', level: 2 },
  { tagName: 'h3', level: 3 },
  { tagName: 'h4', level: 4 },
  { tagName: 'h5', level: 5 },
  { tagName: 'h6', level: 6 },
];

export default (tocDepth: number): TableOfContentInfo[] => {
  const editableArea = document.querySelector('.re-editable-area');

  if (!editableArea || tocDepth < 1 || tocDepth > 6) return [];

  const tableOfContentInfo: TableOfContentInfo[] = [];
  const allowedTags = headingsTags.slice(0, tocDepth).map((tag) => tag.tagName);
  const headings = editableArea.querySelectorAll(allowedTags.join(', '));

  headings.forEach((heading) => heading.removeAttribute('id'));

  headings.forEach((heading) => {
    const headingSlug = getTextSlug(heading.textContent || '');
    heading.setAttribute('id', headingSlug);

    tableOfContentInfo.push({
      text: heading.textContent || '',
      slug: headingSlug,
      level: headingsTags.find((tag) => tag.tagName === heading.tagName.toLowerCase())?.level ?? 1,
    });
  });

  return tableOfContentInfo;
};
