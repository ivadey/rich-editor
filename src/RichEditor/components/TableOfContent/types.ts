export interface TableOfContentInfo {
  text: string;
  slug: string;
  level: number;
}
