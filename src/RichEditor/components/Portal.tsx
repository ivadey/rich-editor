import React from 'react';
import ReactDOM from 'react-dom';

const Portal: React.FC<React.PropsWithChildren> = ({ children }) => (typeof document === 'object'
  ? ReactDOM.createPortal(children, document.body)
  : null);

export default Portal;
