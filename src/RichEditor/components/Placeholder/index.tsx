import { useSelected, useSlate } from 'slate-react';
import { Editor, Range as SlateRange, Element as SlateElement } from 'slate';
import React from 'react';

const Placeholder: React.FC<{ text: string }> = ({ text }) => {
  const editor = useSlate();
  const selected = useSelected();

  let needToRenderPlaceholder = false;
  if (editor.selection) {
    const isAnyVoidElements = Array.from(Editor.nodes(editor, {
      at: editor.selection.focus,
      match: (node) => SlateElement.isElement(node) && editor.isVoid(node),
    })).length > 0;

    needToRenderPlaceholder = (
      selected
      && !Editor.string(editor, editor.selection.focus.path)
      && !isAnyVoidElements
      && SlateRange.isCollapsed(editor.selection)
    );
  }

  if (!needToRenderPlaceholder) return null;

  return (
    <span
      className="re-placeholder"
      contentEditable={false}
    >
      {text}
    </span>
  );
};

Placeholder.displayName = 'Placeholder';

export default Placeholder;
