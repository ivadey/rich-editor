import React, { ReactElement } from 'react';
import { Element as SlateElement } from 'slate';
import { RenderElementProps } from 'slate-react';
import { Popover } from 'react-tiny-popover';
import Toolbar from '../Toolbar';
import { IToolbarProps } from '../Toolbar/types';

export interface IFloatingMenuProps {
  buttons: IToolbarProps['buttons'] | React.ReactElement;
  onMouseEnter?: (event: React.MouseEvent<HTMLDivElement>) => void;
  onMouseLeave?: (event: React.MouseEvent<HTMLDivElement>) => void;
  element: SlateElement;
  elementRef: RenderElementProps['attributes']['ref'];
  children: ReactElement;
}

const FloatingMenu: React.FC<IFloatingMenuProps> = ({
  buttons, onMouseEnter, onMouseLeave, element, elementRef, children,
}) => (
  <Popover
    isOpen
    containerClassName="re-floating-menu__container"
    padding={5}
    content={(
      <div
        className="re-floating-menu__content"
        onMouseEnter={onMouseEnter}
        onMouseLeave={onMouseLeave}
      >
        {Array.isArray(buttons)
          ? <Toolbar buttons={buttons} />
          : React.cloneElement(buttons, { element, elementRef })}
      </div>
    )}
  >
    {children}
  </Popover>
);

FloatingMenu.displayName = 'FloatingMenu';

export default FloatingMenu;
