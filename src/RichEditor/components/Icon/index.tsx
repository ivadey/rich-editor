import React from 'react';
import icons from './icons';

export type TIconName = keyof typeof icons;

export interface IIconProps {
  icon: TIconName;
}

const Icon = ({ icon }: IIconProps) => {
  const [width, height, path] = icons[icon];

  return (
    <svg
      aria-hidden="true"
      focusable="false"
      data-icon={icon}
      className="re-editor-icon"
      role="img"
      xmlns="http://www.w3.org/2000/svg"
      viewBox={`0 0 ${width} ${height}`}
    >
      <path fill="currentColor" d={path as string} />
    </svg>
  );
};

Icon.displayName = 'Icon';

export default Icon;
