import bars from './bars';
import bell from './bell';
import magnifyingGlass from './magnifying-glass';
import times from './times';
import timesCircle from './times-circle';
import bold from './bold';
import italic from './italic';
import underline from './underline';
import undo from './undo';
import redo from './redo';
import codeSimple from './code-simple';
import rectangleCode from './rectangle-code';
import listOl from './list-ol';
import listUl from './list-ul';
import blockQuote from './block-quote';
import link from './link';
import squareCheck from './square-check';
import image from './image';
import table from './table';
import alignLeft from './align-left';
import alignCenter from './align-center';
import alignRight from './align-right';
import alignJustify from './align-justify';
import strikethrough from './strikethrough';
import caretDown from './caret-down';
import check from './check';
import memo from './memo';
import circleInfo from './circle-info';
import triangleExclamation from './triangle-exclamation';
import circleCheck from './circle-check';
import circleExclamation from './circle-exclamation';
import at from './at';
import trash from './trash';
import rectangleVerticalHistory from './rectangle-vertical-history';
import messageLines from './message-lines';
import edit from './edit';
import plus from './plus';
import fileDashedLine from './file-dashed-line';
import h1 from './h1';
import h2 from './h2';
import h3 from './h3';
import h4 from './h4';
import h5 from './h5';
import h6 from './h6';
import anchor from './anchor';
import imageAlignCenter from './image-align-center';
import imageAlignLeft from './image-align-left';
import imageAlignRight from './image-align-right';
import imageFloatLeft from './image-float-left';
import imageFloatRight from './image-float-right';
import subscript from './subscript';
import superscript from './superscript';
import circleCaretDown from './circle-caret-down';
import chevronRight from './chevron-right';
import imageSlash from './image-slash';

export default {
  bars,
  bell,
  'magnifying-glass': magnifyingGlass,
  times,
  'times-circle': timesCircle,
  bold,
  italic,
  underline,
  undo,
  redo,
  'code-simple': codeSimple,
  'rectangle-code': rectangleCode,
  'list-ol': listOl,
  'list-ul': listUl,
  'block-quote': blockQuote,
  link,
  'square-check': squareCheck,
  image,
  table,
  'align-left': alignLeft,
  'align-center': alignCenter,
  'align-right': alignRight,
  'align-justify': alignJustify,
  strikethrough,
  'caret-down': caretDown,
  check,
  memo,
  'circle-info': circleInfo,
  'triangle-exclamation': triangleExclamation,
  'circle-check': circleCheck,
  'circle-exclamation': circleExclamation,
  at,
  trash,
  'rectangle-vertical-history': rectangleVerticalHistory,
  'message-lines': messageLines,
  edit,
  plus,
  'file-dashed-line': fileDashedLine,
  xmark: times,
  h1,
  h2,
  h3,
  h4,
  h5,
  h6,
  anchor,
  'image-align-center': imageAlignCenter,
  'image-align-left': imageAlignLeft,
  'image-align-right': imageAlignRight,
  'image-float-left': imageFloatLeft,
  'image-float-right': imageFloatRight,
  subscript,
  superscript,
  'circle-caret-down': circleCaretDown,
  'chevron-right': chevronRight,
  'image-slash': imageSlash,
};
