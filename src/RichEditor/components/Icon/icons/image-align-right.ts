/*eslint-disable*/
export default [
  448,
  512,
  `M 424 40
    C 437.3 40 448 50.75 448 64
    C 448 77.25 437.3 88 424 88
    H 24
    C 10.75 88 0 77.25 0 64
    C 0 50.75 10.75 40 24 40
    H 424
z
M 224 168
    H 424
    a 24 24 0 0 1 24 24
    V 344
    a 24 24 0 0 1 -24 24
    H 224
    a 24 24 0 0 1 -24 -24
    V 192
    a 24 24 0 0 1 24 -24
z
M 424 424
    C 437.3 424 448 434.7 448 448
    C 448 461.3 437.3 472 424 472
    H 24
    C 10.75 472 0 461.3 0 448
    C 0 434.7 10.75 424 24 424
    H 424
z`
]
