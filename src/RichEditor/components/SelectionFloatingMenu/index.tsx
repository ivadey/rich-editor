import React, { useEffect, useRef } from 'react';
import {
  Editor,
  Range as SlateRange,
} from 'slate';
import { useFocused, useReadOnly, useSlate } from 'slate-react';
import { IToolbarProps } from '../Toolbar/types';
import Toolbar from '../Toolbar';
import Portal from '../Portal';

type EditorInfo = {
  editor: Editor;
  isReadonly: boolean;
  isFocused: boolean;
}

type GetSelectionFloatingMenuFunc = (editorInfo: EditorInfo) => IToolbarProps['buttons'] | React.ReactNode;

export type SelectionFloatingMenuProps = {
  buttons?: IToolbarProps['buttons'] | GetSelectionFloatingMenuFunc | React.ReactNode;
};

const SelectionFloatingMenu: React.FC<SelectionFloatingMenuProps> = ({ buttons }) => {
  const editor = useSlate();
  const isReadonly = useReadOnly();
  const isFocused = useFocused();

  const menuContainerRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const { current: menuContainer } = menuContainerRef;
    const { selection } = editor;

    if (!menuContainer) return;

    if (!selection || !isFocused || SlateRange.isCollapsed(selection) || !Editor.string(editor, selection)) {
      menuContainer.removeAttribute('style');
      return;
    }

    const domSelection = window.getSelection();
    if (!domSelection) return;

    const domRange = domSelection.getRangeAt(0);
    const rect = domRange.getBoundingClientRect();

    const top = rect.top + window.scrollY - menuContainer.offsetHeight;
    const left = rect.left + window.scrollX - (menuContainer.offsetWidth / 2) + (rect.width / 2);

    menuContainer.style.opacity = '1';
    menuContainer.style.top = `${top}px`;
    menuContainer.style.left = `${left}px`;
  });

  const toolbarButtons = typeof buttons === 'function'
    ? buttons({ editor, isReadonly, isFocused })
    : buttons;

  if (!toolbarButtons || !editor.selection || SlateRange.isCollapsed(editor.selection)) return null;

  return (
    <Portal>
      <div
        ref={menuContainerRef}
        className="re-selection-floating-menu"
      >
        {Array.isArray(toolbarButtons)
          ? <Toolbar buttons={toolbarButtons} />
          : toolbarButtons}
      </div>
    </Portal>
  );
};

SelectionFloatingMenu.displayName = 'SelectionFloatingMenu';

export default SelectionFloatingMenu;
