import React from 'react';

interface ISelectProps {
  options: Array<{ id: string | number; label: string; }>;
  value?: string | number;
  onChange: (value: string | number) => void;
}

const Select: React.FC<ISelectProps> = ({ options, value, onChange }) => (
  <select
    className="re-select"
    contentEditable={false}
    value={value}
    onChange={(e) => onChange(e.target.value)}
  >
    {options.map((option) => (
      <option key={option.id} value={option.id}>
        {option.label}
      </option>
    ))}
  </select>
);

Select.displayName = 'Select';

export default Select;
