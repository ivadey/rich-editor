/* eslint-disable no-param-reassign */
import { Editor } from 'slate';
import inlineMenuStore from './inlineMenuStore';
import { InlineMenuOption } from './types';

export default (editor: Editor): Editor => {
  editor.openInlineMenu = (options: InlineMenuOption[], onSelect: (selected: InlineMenuOption) => void) => {
    inlineMenuStore.open(options, onSelect);
  };

  editor.closeInlineMenu = () => { inlineMenuStore.close(); };

  editor.isInlineMenuOpened = () => inlineMenuStore.isOpened;

  return editor;
};
