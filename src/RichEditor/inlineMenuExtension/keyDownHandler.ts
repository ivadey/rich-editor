import React from 'react';
import { BasePoint, Editor, Range as SlateRange } from 'slate';
import inlineMenuStore from './inlineMenuStore';
import { InlineMenuDescription } from './types';

let allowSpaces: boolean;
let caretPosition = 0;

const closeMenu = () => {
  inlineMenuStore.close();
  allowSpaces = false;
  caretPosition = 0;
};

const filterOptions = (editor: Editor, position: BasePoint, pressedKey?: string) => {
  let searchQuery = caretPosition > 2 ? editor.getTextBefore(position, 'word') ?? '' : '';

  if (!pressedKey) searchQuery = searchQuery.slice(0, searchQuery.length - 1);
  else searchQuery += pressedKey;

  searchQuery += editor.getTextAfter(position, 'word') ?? '';

  inlineMenuStore.search(searchQuery);
};

export default (
  editor: Editor,
  menuTriggers: InlineMenuDescription[],
  event: React.KeyboardEvent<HTMLDivElement>,
): boolean => {
  const { isOpened } = inlineMenuStore;
  const { selection } = editor;

  if (!selection || !SlateRange.isCollapsed(selection)) {
    if (isOpened) closeMenu();
    return false;
  }

  const [start] = SlateRange.edges(selection);

  const prevCharacter = editor.getTextBefore(start, 'character');
  const nextCharacter = editor.getTextAfter(start, 'character');

  if (isOpened) {
    if (event.code === 'ArrowDown') {
      event.preventDefault();
      inlineMenuStore.selectNext();
      return true;
    }

    if (event.code === 'ArrowUp') {
      event.preventDefault();
      inlineMenuStore.selectPrev();
      return true;
    }

    if (event.code === 'ArrowLeft' || event.code === 'Backspace') {
      caretPosition -= 1;

      if (caretPosition < 1) closeMenu();
      else if (event.code === 'Backspace') filterOptions(editor, start);

      return false;
    }

    if (event.code === 'ArrowRight' && (!nextCharacter || nextCharacter === '' || nextCharacter === ' ')) {
      closeMenu();
      return false;
    }

    if (event.code === 'Escape') {
      event.preventDefault();
      closeMenu();
      return true;
    }

    if (event.code === 'Space' && !allowSpaces) {
      closeMenu();
      return true;
    }

    if (event.code === 'Tab' || event.code === 'Enter') {
      if (inlineMenuStore.selectedOption) {
        event.preventDefault();
        inlineMenuStore.onSelect(inlineMenuStore.selectedOption);
        return true;
      }

      inlineMenuStore.close();
      return false;
    }

    if (/^[\da-zA-Zа-яёА-ЯЁ]$/.test(event.key)) {
      caretPosition += 1;
      filterOptions(editor, start, event.key);
    }
  } else if (selection && SlateRange.isCollapsed(selection)) {
    const { element: currentElement } = editor.getCurrentElement('lowest');
    const currentString = editor.getCurrentString();

    if (prevCharacter && prevCharacter !== ' ') return false;
    if (nextCharacter && nextCharacter !== ' ') return false;

    const inlineMenu = menuTriggers.find(({ trigger }) => event.key === trigger);
    if (inlineMenu) {
      if (inlineMenu.onlyOnEmptyLines && currentString !== '') return false;
      if (inlineMenu.allowOnlyInBlocks) {
        const isCurrentBlockValid = inlineMenu.allowOnlyInBlocks
          .some((allowedBlockType) => allowedBlockType === currentElement?.type);
        if (!isCurrentBlockValid) return false;
      }

      allowSpaces = !!inlineMenu.allowSpaces;
      caretPosition = 1;

      editor.openInlineMenu(inlineMenu.getOptions(), (selected) => {
        if (inlineMenuStore.searchQuery) Editor.deleteBackward(editor, { unit: 'word' });
        Editor.deleteBackward(editor, { unit: 'character' });
        inlineMenu.onSelect(selected);
      });
    }
  }

  return false;
};
