import React, { useEffect, useRef } from 'react';
import { ReactEditor, useSlate } from 'slate-react';
import { BaseRange } from 'slate';
import { observer } from 'mobx-react';
import { Icon, TIconName } from '../components';
import { classNames } from '../helpers';
import inlineMenuStore from './inlineMenuStore';

const InlineMenuPopover: React.FC = observer(() => {
  const editor = useSlate();
  const inlineMenuContainerRef = useRef<HTMLUListElement>(null);

  const { isOpened, optionsToDisplay, selectedOptionIndex } = inlineMenuStore;

  useEffect(() => {
    if (!isOpened) return;

    if (editor?.selection && inlineMenuContainerRef.current) {
      const { current: el } = inlineMenuContainerRef;
      const domRange = ReactEditor.toDOMRange(editor, editor.selection as BaseRange);
      const rect = domRange.getBoundingClientRect();

      el.style.top = `${rect.top + window.scrollY + 24}px`;
      el.style.left = `${rect.left + window.scrollX}px`;
    }
  }, [editor, isOpened]);

  useEffect(() => {
    const onMouseClick = (event: MouseEvent | TouchEvent) => {
      if (isOpened && (!event.target || !inlineMenuContainerRef.current?.contains(event.target as Node))) {
        inlineMenuStore.close();
      }
    };

    document.addEventListener('click', onMouseClick);
    document.addEventListener('touchstart', onMouseClick);

    return () => {
      document.removeEventListener('click', onMouseClick);
      document.removeEventListener('touchstart', onMouseClick);
    };
  }, [isOpened]);

  return (
    <ul
      ref={inlineMenuContainerRef}
      className={classNames(
        're-inline-menu',
        isOpened ? '' : 'hidden',
      )}
    >
      {(optionsToDisplay || []).map((option, optionInd) => (
        <li
          key={option.id}
          className={classNames(
            're-inline-menu__item',
            selectedOptionIndex === optionInd ? 'active' : '',
          )}
        >
          <button
            type="button"
            onClick={() => inlineMenuStore.onSelect(option)}
          >
            {typeof option.icon === 'string' ? <Icon icon={option.icon as TIconName} /> : option.icon}

            <div className="re-inline-menu__item__content">
              <span>{option.title}</span>
              <span>{option.description}</span>
            </div>

            {!!option.after && (
              <div>
                {option.after}
              </div>
            )}
          </button>
        </li>
      ))}
    </ul>
  );
});

InlineMenuPopover.displayName = 'InlineMenuPopover';

export default InlineMenuPopover;
