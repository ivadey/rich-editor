import { makeAutoObservable } from 'mobx';
import { InlineMenuOption } from './types';

class InlineMenuStore {
  isOpened: boolean;
  options: InlineMenuOption[];
  selectedOptionIndex: number;
  onSelect: (selected: InlineMenuOption) => void;
  searchQuery: string;

  constructor() {
    this.isOpened = false;
    this.options = [];
    this.selectedOptionIndex = -1;
    this.onSelect = () => null;
    this.searchQuery = '';

    makeAutoObservable(this);
  }

  open(options: InlineMenuOption[], onSelect: (selected: InlineMenuOption) => void) {
    if (!Array.isArray(options) || !options.length || typeof onSelect !== 'function') return;

    this.isOpened = true;
    this.selectedOptionIndex = 0;
    this.options = options;
    this.onSelect = (selected) => {
      onSelect(selected);
      this.close();
    };
  }

  close() {
    this.isOpened = false;
    this.selectedOptionIndex = -1;
    this.options = [];
    this.onSelect = () => null;
    this.searchQuery = '';
  }

  selectNext() {
    if (this.selectedOptionIndex === this.options.length - 1) this.selectedOptionIndex = 0;
    else this.selectedOptionIndex += 1;
  }

  selectPrev() {
    if (this.selectedOptionIndex === 0) this.selectedOptionIndex = this.options.length - 1;
    else this.selectedOptionIndex -= 1;
  }

  search(searchQuery: string) {
    this.searchQuery = searchQuery.toLowerCase();
    this.selectedOptionIndex = 0;
  }

  get optionsToDisplay() {
    return this.options.filter((option) => option.title.toLowerCase().indexOf(this.searchQuery) > -1);
  }

  get selectedOption() {
    return this.optionsToDisplay[this.selectedOptionIndex];
  }
}

export default new InlineMenuStore();
