import React from 'react';
import { BaseEditor } from 'slate';
import { TIconName } from '../components';
import { BlockFormatEnum } from '../baseTypes';

export interface InlineMenuEditor extends BaseEditor {
  openInlineMenu: (options: InlineMenuOption[], onSelect: (selected: InlineMenuOption) => void) => void;
  closeInlineMenu: () => void;
  isInlineMenuOpened: () => boolean;
}

export type InlineMenuOption = {
  id: string | number;
  icon: React.ReactNode | TIconName;
  title: string;
  description?: string;
  after?: React.ReactNode;
}

export interface InlineMenuDescription {
  trigger: string;
  onlyOnEmptyLines?: boolean;
  allowSpaces?: boolean;
  allowOnlyInBlocks?: BlockFormatEnum[];
  getOptions: () => InlineMenuOption[];
  onSelect: (selectedOption: InlineMenuOption) => void;
}
