import { strings } from '../../../helpers';
import { BlockFormatEnum } from '../../../baseTypes';
import { IToolbarButtonProps } from '../../../components/Toolbar/types';

const AdmonitionButton: IToolbarButtonProps = {
  icon: 'memo',
  tooltip: strings.admonition,
  active: (editor) => editor.isElementSelected(BlockFormatEnum.Admonition),
  onClick: (editor) => (editor.isElementSelected(BlockFormatEnum.Admonition) ? null : editor.insertAdmonition()),
};

export {
  // eslint-disable-next-line import/prefer-default-export
  AdmonitionButton,
};
