/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { RenderElementProps } from 'slate-react';
import { AdmonitionElement } from '../types';

const Admonition = ({ attributes, children, element }: RenderElementProps): JSX.Element => {
  const { admonitionType = 'error' } = (element as AdmonitionElement);

  const className = ['re-admonition'];

  if (admonitionType === 'tip') className.push('tip');
  else if (admonitionType === 'warning') className.push('warning');
  else if (admonitionType === 'success') className.push('success');
  else if (admonitionType === 'error') className.push('error');
  else className.push('primary');

  return (
    <div className={className.join(' ')} {...attributes}>
      {children}
    </div>
  );
};

Admonition.displayName = 'Admonition';

export default Admonition;
