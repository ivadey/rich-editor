/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { Editor } from 'slate';
import {
  ReactEditor, RenderElementProps, useSelected, useSlate,
} from 'slate-react';
import { strings } from '../../../../helpers';

const AdmonitionContent = ({ attributes, children, element }: RenderElementProps): JSX.Element => {
  const editor = useSlate();
  const selected = useSelected();
  const currentPath = ReactEditor.findPath(editor, element);
  const text = Editor.string(editor, currentPath);

  return (
    <div className="relative" {...attributes}>
      {children}
      {!text && !selected && (
        <span
          className="re-admonition__content"
          contentEditable={false}
        >
          {strings.enterText}
        </span>
      )}
    </div>
  );
};

AdmonitionContent.displayName = 'AdmonitionContent';

export default AdmonitionContent;
