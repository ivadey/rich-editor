/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { Node as SlateNode, Text as SlateText } from 'slate';
import { ReactEditor, RenderElementProps, useSlate } from 'slate-react';
import { Icon, TIconName } from '../../../../components';
import { strings } from '../../../../helpers';
import { AdmonitionElement, AdmonitionType } from '../types';

const icons: { [admonitionType in AdmonitionType]: TIconName } = {
  tip: 'circle-info',
  warning: 'triangle-exclamation',
  success: 'circle-check',
  error: 'circle-exclamation',
};

const AdmonitionTitle = ({ attributes, children, element }: RenderElementProps): JSX.Element => {
  const editor = useSlate();
  const path = ReactEditor.findPath(editor, element);
  const { admonitionType } = SlateNode.parent(editor, path) as AdmonitionElement;

  const title = (element.children[0] as SlateText).text;

  let placeholder = strings.title;
  if (admonitionType === 'tip') placeholder = strings.note;
  else if (admonitionType === 'warning') placeholder = strings.warning;
  else if (admonitionType === 'success') placeholder = strings.success;
  else if (admonitionType === 'error') placeholder = strings.error;

  return (
    <div className="re-admonition__title-container" {...attributes}>
      <span className="re-admonition__title-icon" contentEditable={false}>
        <Icon icon={icons[admonitionType]} />
      </span>
      <div className="re-admonition__title-container">
        {children}
        {!title && (
          <span
            className="re-admonition__title-text"
            contentEditable={false}
          >
            {placeholder}
          </span>
        )}
      </div>
    </div>
  );
};

AdmonitionTitle.displayName = 'AdmonitionTitle';

export default AdmonitionTitle;
