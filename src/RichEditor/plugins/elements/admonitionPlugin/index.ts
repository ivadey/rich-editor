import React from 'react';
import { Element as SlateElement, Node as SlateNode } from 'slate';
import { ContainerDirective } from 'mdast-util-directive';
import { Content } from 'mdast';
import { ElementsPlugin } from '../../types';
import { AdmonitionButton } from './toolbarButtons';
import { Admonition, AdmonitionTitle, AdmonitionContent } from './elements';
import editorWrapper from './editorWrapper';
import floatingMenu from './floatingMenu';
import { BlockFormatEnum } from '../../../baseTypes';
import { AdmonitionElement, AdmonitionType } from './types';
import { IToolbarButtonProps } from '../../../components/Toolbar/types';
import normalizeNodes from './normalizeNodes';

interface AdmonitionPlugin extends ElementsPlugin {
  toolbarButtons: {
    admonition: IToolbarButtonProps | React.ReactNode;
  }
}

const admonitionPlugin: AdmonitionPlugin = {
  name: 'Admonition plugin',
  editorWrapper,
  toolbarButtons: {
    admonition: AdmonitionButton,
  },
  elements: [
    {
      type: 'block',
      formatName: BlockFormatEnum.Admonition,
      floatingMenu,
      render: (props) => React.createElement(Admonition, props),
      enableAboveExitBreak: true,
      enableBelowExitBreak: true,
      convertors: {
        fromRemark: {
          mdastNodeType: 'containerDirective',
          check: (source) => ['tip', 'warning', 'error', 'success'].indexOf((source as ContainerDirective).name) > -1,
          convert: (source, next, decoration) => [{
            type: BlockFormatEnum.Admonition,
            admonitionType: (source as ContainerDirective).name as AdmonitionType,
            children: [{
              type: BlockFormatEnum.AdmonitionTitle,
              children: [{
                text: (source as ContainerDirective).attributes?.title ?? '',
              }],
            }, {
              type: BlockFormatEnum.AdmonitionContent,
              children: next((source as ContainerDirective).children, decoration),
            }] as AdmonitionElement['children'],
          }],
        },
        toRemark: (element, next) => {
          const name = (element as AdmonitionElement).admonitionType;
          let title = '';
          (element as AdmonitionElement).children.forEach((child) => {
            if (SlateElement.isElementType(child, BlockFormatEnum.AdmonitionTitle)) {
              title = SlateNode.string(child);
            }
          });

          return [{
            type: 'containerDirective',
            name,
            attributes: { title },
            children: next((element as AdmonitionElement).children),
          }] as Content[];
        },
      },
    },
    {
      type: 'block',
      formatName: BlockFormatEnum.AdmonitionTitle,
      render: (props) => React.createElement(AdmonitionTitle, props),
      convertors: {
        fromRemark: null,
        toRemark: null,
      },
    },
    {
      type: 'block',
      formatName: BlockFormatEnum.AdmonitionContent,
      render: (props) => React.createElement(AdmonitionContent, props),
      convertors: {
        fromRemark: null,
        toRemark: (source, next) => [{
          type: 'paragraph',
          children: next((source as SlateElement).children),
        }] as Content[],
      },
    },
  ],
  normalizeNodes,
};

export default admonitionPlugin;
