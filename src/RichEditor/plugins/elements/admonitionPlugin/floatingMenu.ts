import { IToolbarProps } from '../../../components/Toolbar/types';
import { strings } from '../../../helpers';
import { AdmonitionType } from './types';

const admonitionTypeButtons = [{
  type: 'tip',
  icon: 'circle-info',
  tooltip: strings.note,
}, {
  type: 'warning',
  icon: 'triangle-exclamation',
  tooltip: strings.warning,
}, {
  type: 'success',
  icon: 'circle-check',
  tooltip: strings.success,
}, {
  type: 'error',
  icon: 'circle-exclamation',
  tooltip: strings.error,
}];

export default [
  admonitionTypeButtons.map((admonitionType) => ({
    icon: admonitionType.icon,
    tooltip: admonitionType.tooltip,
    onClick: (editor) => editor.changeAdmonitionType(admonitionType.type as AdmonitionType),
  })),
  [
    {
      icon: 'trash',
      tooltip: strings.remove,
      onClick: (editor) => editor.removeAdmonition(),
    },
  ],
] as IToolbarProps['buttons'];
