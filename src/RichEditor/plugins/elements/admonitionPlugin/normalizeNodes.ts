import {
  Editor,
  Element as SlateElement,
  Node as SlateNode,
  NodeEntry,
  Transforms,
} from 'slate';
import { BlockFormatEnum } from '../../../baseTypes';
import { AdmonitionContentElement, AdmonitionElement } from './types';

// Rules
// 1. Admonition element should always contain title and content elements
// 2. Admonition title may contain only formatted text
// 3. Admonition content may contain only block elements

export default (editor: Editor, entry: NodeEntry) => {
  const [node, path] = entry;

  if (SlateElement.isElementType<AdmonitionElement>(node, BlockFormatEnum.Admonition)) {
    const isTitleExist = node.children
      .some((child) => SlateElement.isElementType(child, BlockFormatEnum.AdmonitionTitle));
    const isContentExist = node.children
      .some((child) => SlateElement.isElementType(child, BlockFormatEnum.AdmonitionContent));

    // Add admonition title if not exist
    if (!isTitleExist) {
      Transforms.insertNodes(editor, {
        type: BlockFormatEnum.AdmonitionTitle,
        admonitionType: node.admonitionType,
        children: [{ text: '' }],
      }, { at: [...path, 0] });

      return true;
    }

    // Add admonition content if not exist
    if (!isContentExist) {
      Transforms.insertNodes(editor, {
        type: BlockFormatEnum.AdmonitionContent,
        admonitionType: node.admonitionType,
        children: [{
          type: BlockFormatEnum.Paragraph,
          children: [{ text: '' }],
        }],
      }, { at: [...path, 1] });

      return true;
    }

    // Copy all other elements on admonition content section
    for (const [child, childPath] of SlateNode.children(editor, path)) {
      if (
        !SlateElement.isElementType(child, BlockFormatEnum.AdmonitionTitle)
        && !SlateElement.isElementType(child, BlockFormatEnum.AdmonitionContent)
      ) {
        // const [admonitionContentMatch] = Editor.nodes<AdmonitionContentElement>(editor, {
        const [admonitionContentMatch] = Editor.nodes<AdmonitionContentElement>(editor, {
          match: (n) => SlateElement.isElementType(n, BlockFormatEnum.AdmonitionContent),
          mode: 'lowest',
          at: path,
        });
        const [admonitionContentElement, admonitionContentPath] = admonitionContentMatch;

        Editor.withoutNormalizing(editor, () => {
          const isEmpty = !SlateNode.string(child)
            && !(SlateElement.isElement(child) && editor.isVoid(child));

          if (!isEmpty) {
            Transforms.insertNodes(editor, child, {
              at: [...admonitionContentPath, admonitionContentElement.children.length],
            });
          }
          Transforms.removeNodes(editor, { at: childPath });
        });

        return true;
      }
    }
  }

  if (SlateElement.isElementType(node, BlockFormatEnum.AdmonitionTitle)) {
    for (const [child, childPath] of SlateNode.children(editor, path)) {
      if (SlateElement.isElement(child) && !editor.isInline(child)) {
        Transforms.unwrapNodes(editor, { at: childPath });

        return true;
      }
    }
  }

  if (SlateElement.isElementType(node, BlockFormatEnum.AdmonitionContent)) {
    for (const [child, childPath] of SlateNode.children(editor, path)) {
      if (!SlateElement.isElement(child) || editor.isInline(child)) {
        Transforms.wrapNodes(editor, {
          type: BlockFormatEnum.Paragraph,
          children: [],
        }, { at: childPath });

        return true;
      }
    }
  }

  return false;
};
