/* eslint-disable no-param-reassign */
import {
  Descendant,
  Editor,
  Element as SlateElement,
  NodeEntry,
  Range as SlateRange,
  Transforms,
} from 'slate';
import { BlockFormatEnum } from '../../../baseTypes';
import { AdmonitionElement, AdmonitionType } from './types';

const editorWrapper = (editor: Editor) => {
  const { deleteBackward, deleteForward, insertBreak } = editor;

  const getSelectedAdmonition = (): NodeEntry<AdmonitionElement> | undefined => {
    const [selectedAdmonition] = Editor.nodes<AdmonitionElement>(editor, {
      match: (node) => !Editor.isEditor(node)
        && SlateElement.isElement(node)
        && node.type === BlockFormatEnum.Admonition,
      mode: 'lowest',
    });

    return selectedAdmonition;
  };
  const isAdmonitionElement = (elementType: BlockFormatEnum): boolean => [
    BlockFormatEnum.Admonition,
    BlockFormatEnum.AdmonitionTitle,
    BlockFormatEnum.AdmonitionContent,
  ].indexOf(elementType) > -1;

  editor.insertAdmonition = (type: AdmonitionType = 'tip') => {
    const admonitionNode: Descendant = {
      type: BlockFormatEnum.Admonition,
      admonitionType: type,
      children: [{
        type: BlockFormatEnum.AdmonitionTitle,
        children: [{ text: '' }],
      }, {
        type: BlockFormatEnum.AdmonitionContent,
        children: [{
          type: BlockFormatEnum.Paragraph,
          children: [{ text: '' }],
        }],
      }] as AdmonitionElement['children'],
    };

    if (editor.selection && editor.isBlockEmpty(editor.selection.anchor.path)) {
      const focusPath = editor.selection.focus.path;

      Editor.withoutNormalizing(editor, () => {
        Transforms.setNodes(editor, admonitionNode);
        Transforms.insertNodes<SlateElement>(editor, admonitionNode.children, { at: focusPath });
      });
    } else Transforms.insertNodes<SlateElement>(editor, admonitionNode);

    const [, path] = getSelectedAdmonition() || [];
    if (path) Transforms.select(editor, [...path, 0]);
  };
  editor.removeAdmonition = () => {
    const [admonition, path] = getSelectedAdmonition() || [];

    if (admonition) {
      Transforms.removeNodes(editor, { at: path });
      Transforms.insertNodes(editor, {
        type: BlockFormatEnum.Paragraph,
        children: [{ text: '' }],
      }, { at: path });
    }
  };

  editor.changeAdmonitionType = (type: AdmonitionType) => {
    if (!type) return;

    const [admonition, path] = getSelectedAdmonition() || [];

    if (admonition) Transforms.setNodes(editor, { admonitionType: type }, { at: path });
  };

  editor.deleteBackward = (unit) => {
    const { selection } = editor;

    if (selection && SlateRange.isCollapsed(selection)) {
      const [currentNodeEntry] = Editor.nodes<SlateElement>(editor, {
        match: (node) => !Editor.isEditor(node) && SlateElement.isElement(node),
        mode: 'lowest',
        at: selection.focus.path,
      });

      if (!currentNodeEntry) insertBreak();

      const [currentElement, path] = currentNodeEntry;

      if (currentElement.type === BlockFormatEnum.AdmonitionContent && !Editor.string(editor, path)) {
        const [, parentPath] = Editor.parent(editor, path);
        Transforms.select(editor, [...parentPath, 0]);
        return;
      }

      if (isAdmonitionElement(currentElement.type) && !Editor.string(editor, path)) return;
    }

    deleteBackward(unit);
  };
  editor.deleteForward = (unit) => {
    const { selection } = editor;

    if (selection && SlateRange.isCollapsed(selection)) {
      const [currentNodeEntry] = Editor.nodes<SlateElement>(editor, {
        match: (node) => !Editor.isEditor(node) && SlateElement.isElement(node),
        mode: 'lowest',
        at: selection.focus.path,
      });

      if (!currentNodeEntry) insertBreak();

      const [currentElement, path] = currentNodeEntry;

      if (isAdmonitionElement(currentElement.type) && !Editor.string(editor, path)) return;
    }

    deleteForward(unit);
  };
  editor.insertBreak = () => {
    const { selection } = editor;

    if (selection && SlateRange.isCollapsed(selection)) {
      const [currentNodeEntry] = Editor.nodes<SlateElement>(editor, {
        match: (node) => !Editor.isEditor(node) && SlateElement.isElement(node),
        mode: 'lowest',
        at: selection.focus.path,
      });

      if (!currentNodeEntry) insertBreak();

      const [currentElement, path] = currentNodeEntry;

      if (currentElement.type === BlockFormatEnum.AdmonitionTitle) {
        const [, parentPath] = Editor.parent(editor, path);
        Transforms.select(editor, [...parentPath, 1]);
        return;
      }
    }

    insertBreak();
  };

  return editor;
};

export default editorWrapper;
