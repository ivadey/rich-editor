import { BaseElement } from 'slate';
import { BlockFormatEnum } from '../../../baseTypes';

export interface AdmonitionEditor {
  insertAdmonition: (type?: AdmonitionType) => void;
  removeAdmonition: () => void;
  changeAdmonitionType: (type: AdmonitionType) => void;
}

export type AdmonitionType = 'tip' | 'warning' | 'success' | 'error'

export interface AdmonitionElement extends BaseElement {
  type: BlockFormatEnum.Admonition;
  admonitionType: AdmonitionType;
}

export interface AdmonitionTitleElement extends BaseElement {
  type: BlockFormatEnum.AdmonitionTitle;
  admonitionType: AdmonitionType;
}

export interface AdmonitionContentElement extends BaseElement {
  type: BlockFormatEnum.AdmonitionContent;
  admonitionType: AdmonitionType;
}
