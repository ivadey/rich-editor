import { BaseElement } from 'slate';
import { BlockFormatEnum } from '../../../baseTypes';

export interface DividerEditor {
  insertDivider: () => void;
}

export interface DividerElement extends BaseElement {
  type: BlockFormatEnum.Divider;
}
