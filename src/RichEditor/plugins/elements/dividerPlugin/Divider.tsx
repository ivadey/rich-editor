/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { RenderElementProps, useSelected } from 'slate-react';

const Divider = ({ attributes, children }: RenderElementProps): JSX.Element => {
  const selected = useSelected();

  const className = ['re-divider'];
  if (selected) className.push('selected');

  return (
    <div className={className.join(' ')} {...attributes}>
      {children}
    </div>
  );
};

Divider.displayName = 'Divider';

export default Divider;
