import { strings } from '../../../helpers';
import { IToolbarButtonProps } from '../../../components/Toolbar/types';

const DividerButton: IToolbarButtonProps = {
  icon: 'file-dashed-line',
  tooltip: strings.divider,
  onClick: (editor) => editor.insertDivider(),
  hotkeyHint: 'mod+shift+-',
};
export {
  // eslint-disable-next-line import/prefer-default-export
  DividerButton,
};
