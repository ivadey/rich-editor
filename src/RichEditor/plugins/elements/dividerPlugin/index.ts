import React from 'react';
import { ElementsPlugin } from '../../types';
import { DividerButton } from './toolbarButtons';
import { BlockFormatEnum } from '../../../baseTypes';
import editorWrapper from './editorWrapper';
import Divider from './Divider';
import { IToolbarButtonProps } from '../../../components/Toolbar/types';

interface DividerPlugin extends ElementsPlugin {
  toolbarButtons: {
    divider: IToolbarButtonProps | React.ReactNode;
  }
}

const dividerPlugin: DividerPlugin = {
  name: 'Divider plugin',
  editorWrapper,
  toolbarButtons: {
    divider: DividerButton,
  },
  elements: [
    {
      type: 'block',
      formatName: BlockFormatEnum.Divider,
      render: Divider,
      convertors: {
        fromRemark: {
          mdastNodeType: 'thematicBreak',
          convert: () => [{
            type: BlockFormatEnum.Divider,
            children: [{ text: '' }],
          }],
        },
        toRemark: () => [{ type: 'thematicBreak' }],
      },
    },
  ],
  hotkeys: {
    'mod+shift+-': (editor) => editor.insertDivider(),
  },
};

export default dividerPlugin;
