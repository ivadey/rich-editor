/* eslint-disable no-param-reassign */
import { Editor, Element as SlateElement, Transforms } from 'slate';
import { BlockFormatEnum } from '../../../baseTypes';

const editorWrapper = (editor: Editor) => {
  const { isVoid, insertBreak } = editor;

  editor.isVoid = (element) => (element.type === BlockFormatEnum.Divider ? true : isVoid(element));
  editor.insertBreak = () => {
    const { selection } = editor;

    if (selection) {
      const [divider] = Editor.nodes(editor, {
        match: (n) => !Editor.isEditor(n) && SlateElement.isElement(n) && n.type === BlockFormatEnum.Divider,
      });

      if (divider) {
        editor.insertNode({
          type: BlockFormatEnum.Paragraph,
          children: [{ text: '' }],
        });
        return;
      }
    }

    insertBreak();
  };

  editor.insertDivider = () => {
    Transforms.insertNodes(editor, [{
      type: BlockFormatEnum.Divider,
      children: [{ text: '' }],
    }, {
      type: BlockFormatEnum.Paragraph,
      children: [{ text: '' }],
    }]);
  };

  return editor;
};

export default editorWrapper;
