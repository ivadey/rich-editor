import React from 'react';
import { ElementsPlugin } from '../../types';
import editorWrapper from './editorWrapper';
import elements from './elements';
import { TooltipButton } from './toolbarButtons';
import { IToolbarButtonProps } from '../../../components/Toolbar/types';

interface TooltipPlugin extends ElementsPlugin {
  toolbarButtons: {
    tooltip: IToolbarButtonProps | React.ReactNode;
  }
}

const tooltipPlugin: TooltipPlugin = {
  name: 'Tooltip plugin',
  editorWrapper,
  toolbarButtons: {
    tooltip: TooltipButton,
  },
  elements,
};

export default tooltipPlugin;
