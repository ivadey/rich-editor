/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { RenderElementProps } from 'slate-react';
import Tippy from '@tippyjs/react';
import { TooltipElement } from '../types';

export interface TooltipRenderElementProps extends RenderElementProps {
  element: TooltipElement;
}

const Tooltip: React.FC<TooltipRenderElementProps> = ({ element, attributes, children }) => (
  <Tippy
    content={(
      <span className="re-tooltip">
        {element.tooltip}
      </span>
    )}
    placement="bottom"
    duration={[300, 50]}
  >
    <span className="re-tooltip__text" {...attributes}>
      <span contentEditable={false}>‎</span>
      {children}
    </span>
  </Tippy>
);

Tooltip.displayName = 'Tooltip';

export default Tooltip;
