import React from 'react';
import { TextDirective } from 'mdast-util-directive';
import { Content } from 'mdast';
import { BlockElementDescription, InlineElementDescription } from '../../../types';
import Tooltip, { TooltipRenderElementProps } from './Tooltip';
import { strings } from '../../../../helpers';
import { BlockFormatEnum } from '../../../../baseTypes';
import { TooltipElement } from '../types';
import dialog from '../../../../../dialog';

export default [{
  type: 'block',
  formatName: BlockFormatEnum.Tooltip,
  render: (props) => React.createElement(Tooltip, props as TooltipRenderElementProps),
  floatingMenu: [
    [
      {
        icon: 'edit',
        tooltip: strings.edit,
        onClick: (editor) => {
          dialog.prompt({ message: strings.enterTooltipText }).then((newTooltip) => {
            if (newTooltip) editor.changeTooltip(newTooltip);
          }).catch(() => null);
        },
      },
    ],
    [
      {
        icon: 'trash',
        tooltip: strings.remove,
        onClick: (editor) => editor.removeTooltip(),
      },
    ],
  ],
  convertors: {
    fromRemark: {
      mdastNodeType: 'textDirective',
      check: (source) => (source as TextDirective).name === 'tt',
      convert: (source, next, decoration) => ({
        type: BlockFormatEnum.Tooltip,
        tooltip: (source as TextDirective).attributes?.title ?? '',
        children: next((source as TextDirective).children, decoration),
      }),
    },
    toRemark: (element: TooltipElement, next) => [{
      type: 'textDirective',
      attributes: { title: element.tooltip },
      name: 'tt',
      children: next(element.children),
    }] as Content[],
  },
}] as Array<BlockElementDescription | InlineElementDescription>;
