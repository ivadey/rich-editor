/* eslint-disable no-param-reassign */
import {
  Editor,
  Element as SlateElement,
  Range as SlateRange,
  Transforms,
} from 'slate';
import { BlockFormatEnum } from '../../../baseTypes';
import { TooltipElement } from './types';

const isTooltipActive = (editor: Editor) => {
  const [tooltip] = Editor.nodes(editor, {
    match: (n) => !Editor.isEditor(n) && SlateElement.isElement(n) && n.type === BlockFormatEnum.Tooltip,
  });
  return !!tooltip;
};

const unwrapTooltip = (editor: Editor) => {
  Transforms.unwrapNodes(editor, {
    match: (n) => !Editor.isEditor(n) && SlateElement.isElement(n) && n.type === BlockFormatEnum.Tooltip,
  });
};

const editorWrapper = (editor: Editor) => {
  const { isInline } = editor;

  editor.isInline = (element) => element.type === BlockFormatEnum.Tooltip || isInline(element);

  const getSelectedTooltip = () => {
    const [selectedTooltip] = Editor.nodes<TooltipElement>(editor, {
      match: (node) => !Editor.isEditor(node)
        && SlateElement.isElement(node)
        && node.type === BlockFormatEnum.Tooltip,
    });

    return selectedTooltip;
  };

  editor.addTooltip = (tooltipText) => {
    if (!editor.selection || SlateRange.isCollapsed(editor.selection)) return;

    if (!tooltipText) return;

    if (isTooltipActive(editor)) {
      unwrapTooltip(editor);
    }

    const tooltipElement: TooltipElement = {
      type: BlockFormatEnum.Tooltip,
      tooltip: tooltipText,
      children: [],
    };

    Transforms.wrapNodes(editor, tooltipElement, { split: true });
    Transforms.collapse(editor, { edge: 'end' });
  };
  editor.changeTooltip = (text) => {
    const [selectedTooltip, path] = getSelectedTooltip() || [];

    if (selectedTooltip && text) {
      Transforms.setNodes(editor, { tooltip: text }, { at: path });
    }
  };
  editor.removeTooltip = () => {
    const [selectedTooltip] = getSelectedTooltip() || [];

    if (selectedTooltip) unwrapTooltip(editor);
  };

  return editor;
};

export default editorWrapper;
