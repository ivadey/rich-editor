import { BaseElement } from 'slate';
import { BlockFormatEnum } from '../../../baseTypes';

export interface TooltipEditor {
  addTooltip: (text: string) => void;
  removeTooltip: () => void;
  changeTooltip: (text: string) => void;
}

export interface TooltipElement extends BaseElement {
  type: BlockFormatEnum.Tooltip;
  tooltip: string;
}
