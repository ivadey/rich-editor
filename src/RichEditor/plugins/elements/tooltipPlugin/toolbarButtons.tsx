import { Range as SlateRange } from 'slate';
import { strings } from '../../../helpers';
import { BlockFormatEnum } from '../../../baseTypes';
import { IToolbarButtonProps } from '../../../components/Toolbar/types';
import dialog from '../../../../dialog';

const TooltipButton: IToolbarButtonProps = {
  icon: 'message-lines',
  tooltip: strings.tooltip,
  active: (editor) => editor.isElementSelected(BlockFormatEnum.Tooltip),
  disabled: (editor) => !editor.isElementSelected(BlockFormatEnum.Tooltip) && (
    !editor.selection || SlateRange.isCollapsed(editor.selection)
  ),
  onClick: (editor) => {
    dialog.prompt({ message: strings.enterTooltipText }).then((newTooltip) => {
      if (newTooltip) editor.addTooltip(newTooltip);
    }).catch(() => null);
  },
};

export {
  // eslint-disable-next-line import/prefer-default-export
  TooltipButton,
};
