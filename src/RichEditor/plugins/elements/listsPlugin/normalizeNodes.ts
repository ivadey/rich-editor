import {
  Editor, Element as SlateElement, Node as SlateNode, NodeEntry, Text as SlateText, Transforms,
} from 'slate';
import { BlockFormatEnum } from '../../../baseTypes';

// Rules
// 1. Lists can contain only list items as children, all others should be converted to list items
// 2. If list contain only inline elements then it should be converted to paragraph
// 3. ListComponent items can contain only inline elements, text or nested list
// 4. If list item have no parent then it should be converted to a paragraph
// 5. Todos lists items always should have boolean value in checked and other types a null

export default (editor: Editor, entry: NodeEntry) => {
  const [node, path] = entry;

  const isNodeAList = SlateElement.isElementType(node, BlockFormatEnum.BulletedList)
    || SlateElement.isElementType(node, BlockFormatEnum.NumberedList)
    || SlateElement.isElementType(node, BlockFormatEnum.TodoList);

  if (isNodeAList) {
    const isTodoList = SlateElement.isElementType(node, BlockFormatEnum.TodoList);

    for (const [child, childPath] of SlateNode.children(editor, path)) {
      if (SlateElement.isElement(child) && child.type !== BlockFormatEnum.ListItem) {
        Transforms.setNodes(editor, {
          type: BlockFormatEnum.ListItem,
          children: [],
        }, { at: childPath });

        return true;
      }

      if (SlateText.isText(child) || editor.isInline(child)) {
        Transforms.wrapNodes(editor, {
          type: BlockFormatEnum.ListItem,
          children: [child],
        }, { at: childPath });

        return true;
      }

      if (isTodoList && typeof child.checked !== 'boolean') {
        Transforms.setNodes(editor, {
          type: BlockFormatEnum.ListItem,
          checked: false,
          children: [],
        }, { at: childPath });

        return true;
      }

      if (!isTodoList && typeof child.checked === 'boolean') {
        Transforms.setNodes(editor, {
          type: BlockFormatEnum.ListItem,
          checked: null,
          children: [],
        }, { at: childPath });

        return true;
      }
    }
  }

  if (SlateElement.isElementType(node, BlockFormatEnum.ListItem)) {
    for (const [child, childPath] of SlateNode.children(editor, path)) {
      const isElement = SlateElement.isElement(child);
      const isInline = SlateText.isText(child) || editor.isInline(child);
      const isListElement = isElement
        && [BlockFormatEnum.NumberedList, BlockFormatEnum.BulletedList, BlockFormatEnum.TodoList].includes(child.type);

      if (!isInline && !isListElement) {
        Transforms.unwrapNodes(editor, { at: childPath });
        return true;
      }
    }

    const [parent] = Editor.parent(editor, path);
    if (
      !SlateElement.isElementType(parent, BlockFormatEnum.BulletedList)
      && !SlateElement.isElementType(parent, BlockFormatEnum.NumberedList)
      && !SlateElement.isElementType(parent, BlockFormatEnum.TodoList)
    ) {
      Transforms.setNodes(editor, { type: BlockFormatEnum.Paragraph });
      return true;
    }
  }

  return false;
};
