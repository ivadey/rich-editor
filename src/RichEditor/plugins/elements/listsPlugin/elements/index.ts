import React from 'react';
import { List, ListItem, Paragraph } from 'mdast';
import { Element as SlateElement } from 'slate';
import { BlockElementDescription } from '../../../types';
import { BlockFormatEnum } from '../../../../baseTypes';
import ListComponent from './ListComponent';
import ListItemComponent, { ListItemComponentProps } from './ListItemComponent';
import { ListItemElement } from '../types';

const fromRemarkNodeCheck = (source: List, nodeType: BlockFormatEnum) => {
  switch (nodeType) {
    case BlockFormatEnum.BulletedList:
      return !source.ordered && !source.children.some((child) => typeof child.checked === 'boolean');
    case BlockFormatEnum.NumberedList:
      return !!source.ordered;
    case BlockFormatEnum.TodoList:
      return !source.ordered && source.children.some((child) => typeof child.checked === 'boolean');
    default:
      return false;
  }
};

const lists = [
  BlockFormatEnum.NumberedList, BlockFormatEnum.BulletedList, BlockFormatEnum.TodoList,
].map((nodeType) => ({
  type: 'block',
  formatName: nodeType,
  render: (props) => React.createElement(ListComponent, props),
  convertors: {
    fromRemark: {
      mdastNodeType: 'list',
      check: (source) => fromRemarkNodeCheck(source as List, nodeType),
      convert: (source, next, decoration) => ({
        type: nodeType,
        children: next((source as List).children, decoration),
      }),
    },
    toRemark: (element, next) => [{
      type: 'list',
      ordered: nodeType === BlockFormatEnum.NumberedList,
      children: next((element as SlateElement).children) as List['children'],
    }],
  },
} as BlockElementDescription));

export default [
  // {
  //   type: 'block',
  //   formatName: BlockFormatEnum.NumberedList,
  //   render: (props) => React.createElement(ListComponent, props),
  //   convertors: {
  //     fromRemark: {
  //       mdastNodeType: 'list',
  //       check: (source) => !!(source as List).ordered,
  //       convert: (source, next, decoration) => ({
  //         type: BlockFormatEnum.NumberedList,
  //         children: next((source as List).children, decoration),
  //       }),
  //     },
  //     toRemark: (element, next) => [{
  //       type: 'list',
  //       ordered: true,
  //       children: next((element as SlateElement).children) as List['children'],
  //     }],
  //   },
  // },
  // {
  //   type: 'block',
  //   formatName: BlockFormatEnum.BulletedList,
  //   render: (props) => React.createElement(ListComponent, props),
  //   convertors: {
  //     fromRemark: {
  //       mdastNodeType: 'list',
  //       check: (source) => !(source as List).ordered,
  //       convert: (source, next, decoration) => ({
  //         type: BlockFormatEnum.BulletedList,
  //         children: next((source as List).children, decoration),
  //       }),
  //     },
  //     toRemark: (element, next) => [{
  //       type: 'list',
  //       children: next((element as SlateElement).children) as List['children'],
  //     }],
  //   },
  // },
  // {
  //   type: 'block',
  //   formatName: BlockFormatEnum.TodoList,
  //   render: (props) => React.createElement(ListComponent, props),
  //   convertors: {
  //     fromRemark: null,
  //     toRemark: null,
  //   },
  // },
  ...lists,
  {
    type: 'block',
    formatName: BlockFormatEnum.ListItem,
    render: (props) => React.createElement(ListItemComponent, props as ListItemComponentProps),
    convertors: {
      fromRemark: {
        mdastNodeType: 'listItem',
        convert: (source, next, decoration) => ({
          type: BlockFormatEnum.ListItem,
          checked: (source as ListItem).checked,
          children: next((source as ListItem).children, decoration),
        }),
      },
      toRemark: (element, next) => [{
        type: 'listItem',
        checked: typeof (element as ListItemElement).checked === 'boolean'
          ? (element as ListItemElement).checked
          : null,
        children: [{
          type: 'paragraph',
          children: next((element as SlateElement).children) as Paragraph['children'],
        }],
      }],
    },
  },
] as BlockElementDescription[];
