import React from 'react';
import { ReactEditor, RenderElementProps, useSlate } from 'slate-react';
import { Editor, Element as SlateElement, Transforms } from 'slate';
import { BlockFormatEnum } from '../../../../baseTypes';
import { ListItemElement } from '../types';
import { classNames } from '../../../../helpers';

export type ListItemComponentProps = RenderElementProps & {
  element: ListItemElement;
}

const ListItemComponent: React.FC<ListItemComponentProps> = ({ element, attributes, children }) => {
  const editor = useSlate();

  const isNestedListItem = element.children.some((el) => (
    SlateElement.isElementType(el, BlockFormatEnum.NumberedList)
    || SlateElement.isElementType(el, BlockFormatEnum.BulletedList)
    || SlateElement.isElementType(el, BlockFormatEnum.TodoList)
  ));

  const elementPath = ReactEditor.findPath(editor, element);
  const [parent] = Editor.parent(editor, elementPath);
  const isTodoItem = SlateElement.isElementType(parent, BlockFormatEnum.TodoList);

  return (
    <li
      className={classNames(
        isNestedListItem ? 're-list__item-with-nested-list' : '',
        isTodoItem ? 're-list__todo-item' : '',
      )}
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...attributes}
    >
      {isTodoItem && !isNestedListItem && (
        <button
          type="button"
          className={classNames(
            're-list__todo-item__checkbox',
            element.checked ? 'checked' : '',
          )}
          contentEditable={false}
          onClick={() => {
            Transforms.setNodes(editor, { checked: !element.checked }, { at: elementPath });
          }}
        >
          {element.checked && '✔'}
        </button>
      )}
      {children}
    </li>
  );
};

ListItemComponent.displayName = 'ListItemComponent';

export default ListItemComponent;
