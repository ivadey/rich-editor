import React from 'react';
import { RenderElementProps } from 'slate-react';
import { BlockFormatEnum } from '../../../../baseTypes';

const ListComponent: React.FC<RenderElementProps> = ({ element, attributes, children }) => {
  const className = ['re-list'];

  if (element.type === BlockFormatEnum.NumberedList) className.push('numbered');
  else if (element.type === BlockFormatEnum.BulletedList) className.push('bulleted');
  else if (element.type === BlockFormatEnum.TodoList) className.push('todo');

  const ListTagName = element.type === BlockFormatEnum.NumberedList ? 'ol' : 'ul';

  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <ListTagName className={className.join(' ')} {...attributes}>
      {children}
    </ListTagName>
  );
};

ListComponent.displayName = 'ListComponent';

export default ListComponent;
