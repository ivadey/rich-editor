import React from 'react';
import {
  Editor,
  Element as SlateElement,
  Range as SlateRange,
} from 'slate';
import isHotkey from 'is-hotkey';
import { ElementsPlugin } from '../../types';
import { BulletedListButton, NumberedListButton, TodoListButton } from './toolbarButtons';
import elements from './elements';
import { BlockFormatEnum } from '../../../baseTypes';
import editorWrapper from './editorWrapper';
import { ListElement } from './types';
import { IToolbarButtonProps } from '../../../components/Toolbar/types';
import normalizeNodes from './normalizeNodes';

interface ListsPlugin extends ElementsPlugin {
  toolbarButtons: {
    numberedList: IToolbarButtonProps | React.ReactNode;
    bulletedList: IToolbarButtonProps | React.ReactNode;
    todoList: IToolbarButtonProps | React.ReactNode;
  }
}

const listsPlugin: ListsPlugin = {
  name: 'Lists plugin',
  editorWrapper,
  toolbarButtons: {
    numberedList: NumberedListButton,
    bulletedList: BulletedListButton,
    todoList: TodoListButton,
  },
  elements,
  onKeyDownHandler: (event, editor) => {
    if (!editor.selection) return false;

    const [topLevelListMatch] = Array.from(
      Editor.nodes<ListElement>(editor, {
        at: Editor.unhangRange(editor, editor.selection),
        match: (node) => (
          SlateElement.isElementType(node, BlockFormatEnum.BulletedList)
          || SlateElement.isElementType(node, BlockFormatEnum.NumberedList)
          || SlateElement.isElementType(node, BlockFormatEnum.TodoList)
        ),
      }),
    );
    const [topLevelList] = topLevelListMatch || [];

    if (!topLevelList) return false;

    if (isHotkey('backspace', event)) {
      if (editor.selection && SlateRange.isCollapsed(editor.selection) && editor.selection.anchor.offset === 0) {
        const currentListDepth = editor.getCurrentListDepth();
        const currentListItemIndex = editor.getCurrentListItemIndex();

        if (currentListDepth === 1 && currentListItemIndex === 0) {
          event.preventDefault();
          editor.decreaseSelectedListItemDepth();
          return true;
        }
      }
    }

    if (isHotkey('tab', event)) {
      event.preventDefault();
      editor.increaseSelectedListItemDepth();
      return true;
    }

    if (isHotkey('enter', event) && !editor.getCurrentString()) {
      event.preventDefault();
      editor.decreaseSelectedListItemDepth();
      return true;
    }

    return false;
  },
  hotkeys: {
    'mod+shift+7': (editor) => editor.toggleList(BlockFormatEnum.NumberedList),
    'mod+shift+8': (editor) => editor.toggleList(BlockFormatEnum.BulletedList),
    'mod+shift+9': (editor) => editor.toggleList(BlockFormatEnum.TodoList),
    'shift+tab': (editor) => editor.decreaseSelectedListItemDepth(),
  },
  normalizeNodes,
};

export default listsPlugin;
