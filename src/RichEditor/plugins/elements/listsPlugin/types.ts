import { BaseElement } from 'slate';
import { BlockFormatEnum } from '../../../baseTypes';

export interface ListElement extends BaseElement {
  type: BlockFormatEnum.NumberedList | BlockFormatEnum.BulletedList | BlockFormatEnum.TodoList;
}

export interface ListItemElement extends BaseElement {
  type: BlockFormatEnum.ListItem;
  checked?: boolean | null;
}

export interface ListEditor {
  toggleList: (listType: ListElement['type']) => void;

  getCurrentListDepth: () => number;
  getCurrentListItemIndex: () => number;

  increaseSelectedListItemDepth: () => void;
  decreaseSelectedListItemDepth: () => void;
}
