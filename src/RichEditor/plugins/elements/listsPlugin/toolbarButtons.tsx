import { BlockFormatEnum } from '../../../baseTypes';
import { strings } from '../../../helpers';
import { IToolbarButtonProps } from '../../../components/Toolbar/types';

const NumberedListButton: IToolbarButtonProps = {
  icon: 'list-ol',
  active: (editor) => editor.isElementSelected(BlockFormatEnum.NumberedList),
  onClick: (editor) => editor.toggleList(BlockFormatEnum.NumberedList),
  tooltip: strings.numberedList,
  hotkeyHint: 'mod+shift+7',
};

const BulletedListButton: IToolbarButtonProps = {
  icon: 'list-ul',
  active: (editor) => editor.isElementSelected(BlockFormatEnum.BulletedList),
  onClick: (editor) => editor.toggleList(BlockFormatEnum.BulletedList),
  tooltip: strings.bulletedList,
  hotkeyHint: 'mod+shift+8',
};

const TodoListButton: IToolbarButtonProps = {
  icon: 'square-check',
  active: (editor) => editor.isElementSelected(BlockFormatEnum.TodoList),
  onClick: (editor) => editor.toggleList(BlockFormatEnum.TodoList),
  tooltip: strings.checklist,
  hotkeyHint: 'mod+shift+9',
};

export {
  NumberedListButton,
  BulletedListButton,
  TodoListButton,
};
