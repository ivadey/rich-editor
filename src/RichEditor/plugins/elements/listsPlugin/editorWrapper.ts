/* eslint-disable no-param-reassign */
import { Editor, Element as SlateElement, Transforms } from 'slate';
import { BlockFormatEnum } from '../../../baseTypes';
import { ListElement, ListItemElement } from './types';

const editorWrapper = (editor: Editor) => {
  const { insertBreak } = editor;
  editor.insertBreak = () => {
    insertBreak();

    const { element: currentElement } = editor.getCurrentElement('lowest');
    const isListItem = SlateElement.isElement(currentElement) && currentElement.type === BlockFormatEnum.ListItem;
    if (isListItem && currentElement.checked === true) {
      Transforms.setNodes(editor, { ...currentElement, checked: false });
    }
  };

  const toggleList = (type: ListElement['type']) => {
    const [currentListItemMatch] = Editor.nodes(editor, {
      match: (node) => SlateElement.isElementType(node, BlockFormatEnum.ListItem),
      mode: 'lowest',
    });

    Editor.withoutNormalizing(editor, () => {
      if (!currentListItemMatch) {
        Editor.withoutNormalizing(editor, () => {
          Transforms.setNodes(editor, { type: BlockFormatEnum.ListItem });
          Transforms.wrapNodes(editor, { type, children: [] });
        });
      } else {
        Transforms.unwrapNodes(editor, {
          match: (node) => SlateElement.isElementType(node, type),
          split: true,
        });
        Transforms.setNodes(editor, { type: BlockFormatEnum.Paragraph });
      }
    });
  };

  const getCurrentTopLevelList = () => {
    const { selection } = editor;
    if (!selection) return { element: null, path: null };

    const [match] = Array.from(
      Editor.nodes<ListElement>(editor, {
        at: Editor.unhangRange(editor, selection),
        match: (node) => (
          SlateElement.isElementType(node, BlockFormatEnum.BulletedList)
          || SlateElement.isElementType(node, BlockFormatEnum.NumberedList)
          || SlateElement.isElementType(node, BlockFormatEnum.TodoList)
        ),
      }),
    );

    return match ? { element: match[0], path: match[1] } : { element: null, path: null };
  };

  editor.toggleList = (listType) => toggleList(listType);

  editor.getCurrentListDepth = () => {
    const { path: currentTopLevelListPath } = getCurrentTopLevelList();
    const [currentSelectedListMatch] = Editor.nodes<ListItemElement>(editor, {
      match: (node) => (
        SlateElement.isElementType(node, BlockFormatEnum.NumberedList)
        || SlateElement.isElementType(node, BlockFormatEnum.BulletedList)
        || SlateElement.isElementType(node, BlockFormatEnum.TodoList)
      ),
      mode: 'lowest',
    });
    if (!currentSelectedListMatch || !currentTopLevelListPath) return 0;

    const [, currentSelectedListPath] = currentSelectedListMatch;

    const diff = currentSelectedListPath.length - currentTopLevelListPath.length;

    return Math.trunc(diff / 2) + 1;
  };
  editor.getCurrentListItemIndex = () => {
    const [listItemMatch] = Editor.nodes<ListItemElement>(editor, {
      match: (node) => SlateElement.isElementType(node, BlockFormatEnum.ListItem),
      mode: 'lowest',
    });

    if (listItemMatch) {
      const [, listItemPath] = listItemMatch;

      return listItemPath[listItemPath.length - 1];
    }

    return 0;
  };

  editor.increaseSelectedListItemDepth = () => {
    const [listItemMatch] = Editor.nodes<ListItemElement>(editor, {
      match: (node) => SlateElement.isElementType(node, BlockFormatEnum.ListItem),
      mode: 'lowest',
    });

    if (listItemMatch) {
      const [listItem, listItemPath] = listItemMatch;
      const [listItemParent] = Editor.parent(editor, listItemPath);

      Editor.withoutNormalizing(editor, () => {
        Transforms.wrapNodes(editor, {
          type: (listItemParent as ListElement).type,
          children: [listItem],
        });
        Transforms.wrapNodes(editor, {
          type: BlockFormatEnum.ListItem,
          children: [],
        }, { at: listItemPath });
      });
    }
  };

  editor.decreaseSelectedListItemDepth = () => {
    if (!editor.selection) return;

    const [listItemMatch] = Editor.nodes<ListItemElement>(editor, {
      match: (node) => SlateElement.isElementType(node, BlockFormatEnum.ListItem),
      mode: 'lowest',
    });
    if (!listItemMatch) return;

    const [, listItemPath] = listItemMatch;

    const [listItemParent] = Editor.parent(editor, listItemPath);
    if (!listItemParent) return;

    if (editor.getCurrentListDepth() === 1) {
      editor.toggleList((listItemParent as ListElement).type);
      return;
    }

    Editor.withoutNormalizing(editor, () => {
      Transforms.liftNodes(editor);
      Transforms.liftNodes(editor);
    });
  };

  return editor;
};

export default editorWrapper;
