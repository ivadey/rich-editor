import { InlineFormatEnum } from '../../../baseTypes';
import { strings } from '../../../helpers';
import { IToolbarButtonProps } from '../../../components/Toolbar/types';

const BoldFormatButton: IToolbarButtonProps = {
  icon: 'bold',
  active: (editor) => editor.isMarkActive(InlineFormatEnum.Bold),
  onClick: (editor) => editor.toggleMark(InlineFormatEnum.Bold),
  tooltip: strings.bold,
  hotkeyHint: 'mod+B',
};

const ItalicFormatButton: IToolbarButtonProps = {
  icon: 'italic',
  active: (editor) => editor.isMarkActive(InlineFormatEnum.Italic),
  onClick: (editor) => editor.toggleMark(InlineFormatEnum.Italic),
  tooltip: strings.italic,
  hotkeyHint: 'mod+I',
};

const UnderlineFormatButton: IToolbarButtonProps = {
  icon: 'underline',
  active: (editor) => editor.isMarkActive(InlineFormatEnum.Underline),
  onClick: (editor) => editor.toggleMark(InlineFormatEnum.Underline),
  tooltip: strings.underline,
  hotkeyHint: 'mod+U',
};

const StrikethroughFormatButton: IToolbarButtonProps = {
  icon: 'strikethrough',
  active: (editor) => editor.isMarkActive(InlineFormatEnum.Strikethrough),
  onClick: (editor) => editor.toggleMark(InlineFormatEnum.Strikethrough),
  tooltip: strings.strikethrough,
  hotkeyHint: 'mod+shift+S',
};

const SubscriptFormatButton: IToolbarButtonProps = {
  icon: 'subscript',
  active: (editor) => editor.isMarkActive(InlineFormatEnum.Subscript),
  onClick: (editor) => editor.toggleMark(InlineFormatEnum.Subscript),
  tooltip: strings.subscript,
};

const SuperscriptFormatButton: IToolbarButtonProps = {
  icon: 'superscript',
  active: (editor) => editor.isMarkActive(InlineFormatEnum.Superscript),
  onClick: (editor) => editor.toggleMark(InlineFormatEnum.Superscript),
  tooltip: strings.superscript,
};

export {
  BoldFormatButton,
  ItalicFormatButton,
  UnderlineFormatButton,
  StrikethroughFormatButton,
  SubscriptFormatButton,
  SuperscriptFormatButton,
};
