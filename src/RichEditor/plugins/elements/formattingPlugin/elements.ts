import { RenderLeafProps } from 'slate-react';
import React from 'react';
import { Delete, Emphasis, Strong } from 'mdast';
import { TextDirective } from 'mdast-util-directive';
import { InlineFormatEnum } from '../../../baseTypes';
import { InlineElementDescription } from '../../types';
import { ElementConvertors, MdastNodeType } from '../../../../Converter/types';

const elementFormats = [
  InlineFormatEnum.Bold,
  InlineFormatEnum.Italic,
  InlineFormatEnum.Underline,
  InlineFormatEnum.Strikethrough,
  InlineFormatEnum.Subscript,
  InlineFormatEnum.Superscript,
];
type MdastContentType = Strong | Emphasis | Delete;

const getRenderer = (format: InlineFormatEnum) => ({ children, leaf }: RenderLeafProps): JSX.Element => {
  if (format === InlineFormatEnum.Bold && leaf[format]) {
    return React.createElement('b', { className: 're-formatted-text_bold' }, children);
  }
  if (format === InlineFormatEnum.Italic && leaf[format]) {
    return React.createElement('i', { className: 're-formatted-text_italic' }, children);
  }
  if (format === InlineFormatEnum.Underline && leaf[format]) {
    return React.createElement('u', { className: 're-formatted-text_underline' }, children);
  }
  if (format === InlineFormatEnum.Strikethrough && leaf[format]) {
    return React.createElement('span', { className: 're-formatted-text_line-through' }, children);
  }
  if (format === InlineFormatEnum.Subscript && leaf[format]) {
    return React.createElement('sub', { className: 're-formatted-text_subscript' }, children);
  }
  if (format === InlineFormatEnum.Superscript && leaf[format]) {
    return React.createElement('sup', { className: 're-formatted-text_superscript' }, children);
  }
  return children;
};

const getFromRemarkConverter = (format: InlineFormatEnum): ElementConvertors['fromRemark'] => {
  let mdastNodeType: MdastNodeType;

  if (format === InlineFormatEnum.Bold) mdastNodeType = 'strong';
  else if (format === InlineFormatEnum.Italic) mdastNodeType = 'emphasis';
  else if (format === InlineFormatEnum.Underline) mdastNodeType = 'textDirective';
  else if (format === InlineFormatEnum.Subscript) mdastNodeType = 'textDirective';
  else if (format === InlineFormatEnum.Superscript) mdastNodeType = 'textDirective';
  else mdastNodeType = 'delete';

  return ({
    mdastNodeType,
    check: [InlineFormatEnum.Underline, InlineFormatEnum.Subscript, InlineFormatEnum.Superscript].includes(format)
      ? (source) => [format as string].includes((source as TextDirective).name)
      : undefined,
    convert: (
      source,
      next,
      decoration,
    ) => next((source as MdastContentType).children, {
      ...decoration,
      text: {
        ...decoration.text,
        [format]: true,
      },
    }),
  });
};

const getToRemarkConverter = (format: InlineFormatEnum): ElementConvertors['toRemark'] => {
  if (format === InlineFormatEnum.Underline) {
    return {
      mdastNodeType: 'textDirective',
      check: (source) => !!source[format],
      modifier: (source, node) => ({
        type: 'textDirective',
        name: InlineFormatEnum.Underline,
        children: [node],
      }),
    };
  }

  if (format === InlineFormatEnum.Subscript) {
    return {
      mdastNodeType: 'textDirective',
      check: (source) => !!source[format],
      modifier: (source, node) => ({
        type: 'textDirective',
        name: InlineFormatEnum.Subscript,
        children: [node],
      }),
    };
  }

  if (format === InlineFormatEnum.Superscript) {
    return {
      mdastNodeType: 'textDirective',
      check: (source) => !!source[format],
      modifier: (source, node) => ({
        type: 'textDirective',
        name: InlineFormatEnum.Superscript,
        children: [node],
      }),
    };
  }

  let mdastNodeType: MdastNodeType;

  if (format === InlineFormatEnum.Bold) mdastNodeType = 'strong';
  else if (format === InlineFormatEnum.Italic) mdastNodeType = 'emphasis';
  else mdastNodeType = 'delete';

  return ({
    mdastNodeType,
    check: (source) => !!source[format],
  });
};

export default elementFormats.map((format) => ({
  type: 'inline',
  formatName: format,
  render: getRenderer(format),
  convertors: {
    fromRemark: getFromRemarkConverter(format),
    toRemark: getToRemarkConverter(format),
  },
})) as InlineElementDescription[];
