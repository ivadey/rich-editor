import React from 'react';
import { ElementsPlugin } from '../../types';
import {
  BoldFormatButton, ItalicFormatButton, StrikethroughFormatButton, UnderlineFormatButton,
  SubscriptFormatButton, SuperscriptFormatButton,
} from './toolbarButtons';
import elements from './elements';
import { InlineFormatEnum } from '../../../baseTypes';
import { IToolbarButtonProps } from '../../../components/Toolbar/types';

interface FormattingPlugin extends ElementsPlugin {
  toolbarButtons: {
    bold: IToolbarButtonProps | React.ReactNode;
    italic: IToolbarButtonProps | React.ReactNode;
    underline: IToolbarButtonProps | React.ReactNode;
    strikethrough: IToolbarButtonProps | React.ReactNode;
    subscript: IToolbarButtonProps | React.ReactNode;
    superscript: IToolbarButtonProps | React.ReactNode;
  }
}

const formattingPlugin: FormattingPlugin = {
  name: 'Formatting plugin',
  toolbarButtons: {
    bold: BoldFormatButton,
    italic: ItalicFormatButton,
    underline: UnderlineFormatButton,
    strikethrough: StrikethroughFormatButton,
    subscript: SubscriptFormatButton,
    superscript: SuperscriptFormatButton,
  },
  elements,
  hotkeys: {
    'mod+b': (editor) => editor.toggleMark(InlineFormatEnum.Bold),
    'mod+i': (editor) => editor.toggleMark(InlineFormatEnum.Italic),
    'mod+u': (editor) => editor.toggleMark(InlineFormatEnum.Underline),
    'mod+shift+s': (editor) => editor.toggleMark(InlineFormatEnum.Strikethrough),
  },
};

export default formattingPlugin;
