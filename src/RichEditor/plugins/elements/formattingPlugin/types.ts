import { BaseText } from 'slate';
import { InlineFormatEnum } from '../../../baseTypes';

export interface FormattedText extends BaseText {
  [InlineFormatEnum.Bold]?: true;
  [InlineFormatEnum.Italic]?: true;
  [InlineFormatEnum.Underline]?: true;
  [InlineFormatEnum.Strikethrough]?: true;
  [InlineFormatEnum.InlineCode]?: true;
  [InlineFormatEnum.Subscript]?: true;
  [InlineFormatEnum.Superscript]?: true;
}
