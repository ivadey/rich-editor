import { RenderElementProps } from 'slate-react';
import React from 'react';
import { Content } from 'mdast';
import { ContainerDirective } from 'mdast-util-directive';
import { BlockElementDescription } from '../../../types';
import { BlockFormatEnum } from '../../../../baseTypes';
import { AlignElement } from '../types';

const alignFormats = [
  BlockFormatEnum.AlignLeft,
  BlockFormatEnum.AlignCenter,
  BlockFormatEnum.AlignRight,
  BlockFormatEnum.AlignJustify,
] as AlignElement['type'][];

const classes = {
  [BlockFormatEnum.AlignLeft]: 're-align-left',
  [BlockFormatEnum.AlignCenter]: 're-align-center',
  [BlockFormatEnum.AlignRight]: 're-align-right',
  [BlockFormatEnum.AlignJustify]: 're-align-justify',
} as Record<AlignElement['type'], string>;

export default alignFormats.map((format) => ({
  type: 'block',
  formatName: format,
  render: ({ children, element, attributes }: RenderElementProps) => React.createElement(
    'div',
    { className: classes[(element as AlignElement).type] ?? '', ...attributes },
    children,
  ),
  convertors: {
    fromRemark: {
      mdastNodeType: 'containerDirective',
      check: (source: ContainerDirective) => (
        source.name === 'p' && (source?.attributes?.class as string)?.indexOf?.(classes[format]) > -1
      ),
      convert: (source: ContainerDirective, next, decoration) => ({
        type: format,
        children: next(source.children, decoration),
      }),
    },
    toRemark: (element: AlignElement, next) => [{
      type: 'containerDirective',
      attributes: { class: classes[format] },
      name: 'p',
      children: next(element.children),
    }] as Content[],
  },
})) as BlockElementDescription[];
