import { BaseElement } from 'slate';
import { BlockFormatEnum } from '../../../baseTypes';

export interface AlignElement extends BaseElement {
  type: BlockFormatEnum.AlignLeft
    | BlockFormatEnum.AlignCenter
    | BlockFormatEnum.AlignRight
    | BlockFormatEnum.AlignJustify;
}

export interface AlignEditor {
  setTextAlign: (align?: AlignElement['type']) => void;
}
