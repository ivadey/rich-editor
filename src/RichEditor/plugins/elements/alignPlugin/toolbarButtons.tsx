import { BlockFormatEnum } from '../../../baseTypes';
import { strings } from '../../../helpers';
import { TIconName } from '../../../components';
import { AlignElement } from './types';
import { IToolbarButtonProps } from '../../../components/Toolbar/types';

const createButtonDefinition = (
  icon: TIconName,
  tooltip: string,
  format: AlignElement['type'],
  hotkeyHint?: string,
): IToolbarButtonProps => ({
  icon,
  tooltip,
  active: (editor) => editor.isElementSelected(format),
  onClick: (editor) => editor.setTextAlign(format),
  hotkeyHint,
});

const LeftAlignButton = createButtonDefinition(
  'align-left',
  strings.alignLeft,
  BlockFormatEnum.AlignLeft,
  'mod+shift+L',
);
const CenterAlignButton = createButtonDefinition(
  'align-center',
  strings.alignCenter,
  BlockFormatEnum.AlignCenter,
  'mod+shift+C',
);
const RightAlignButton = createButtonDefinition(
  'align-right',
  strings.alignRight,
  BlockFormatEnum.AlignRight,
  'mod+shift+R',
);
const JustifyAlignButton = createButtonDefinition(
  'align-justify',
  strings.alignJustify,
  BlockFormatEnum.AlignJustify,
  'mod+shift+J',
);

export {
  LeftAlignButton,
  CenterAlignButton,
  RightAlignButton,
  JustifyAlignButton,
};
