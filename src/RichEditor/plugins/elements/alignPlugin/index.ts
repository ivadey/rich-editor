import React from 'react';
import { ElementsPlugin } from '../../types';
import {
  CenterAlignButton, JustifyAlignButton, LeftAlignButton, RightAlignButton,
} from './toolbarButtons';
import editorWrapper from './editorWrapper';
import elements from './elements';
import { IToolbarButtonProps } from '../../../components/Toolbar/types';
import { BlockFormatEnum } from '../../../baseTypes';
import normalizeNodes from './normalizeNodes';

interface AlignPlugin extends ElementsPlugin {
  toolbarButtons: {
    left: IToolbarButtonProps | React.ReactNode;
    center: IToolbarButtonProps | React.ReactNode;
    right: IToolbarButtonProps | React.ReactNode;
    justify: IToolbarButtonProps | React.ReactNode;
  }
}

const alignPlugin: AlignPlugin = {
  name: 'Align plugin',
  editorWrapper,
  toolbarButtons: {
    left: LeftAlignButton,
    center: CenterAlignButton,
    right: RightAlignButton,
    justify: JustifyAlignButton,
  },
  elements,
  hotkeys: {
    'mod+shift+l': (editor) => editor.setTextAlign(BlockFormatEnum.AlignLeft),
    'mod+shift+c': (editor) => editor.setTextAlign(BlockFormatEnum.AlignCenter),
    'mod+shift+r': (editor) => editor.setTextAlign(BlockFormatEnum.AlignRight),
    'mod+shift+j': (editor) => editor.setTextAlign(BlockFormatEnum.AlignJustify),
  },
  normalizeNodes,
};

export default alignPlugin;
