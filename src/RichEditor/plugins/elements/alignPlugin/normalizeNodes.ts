import {
  Editor,
  Element as SlateElement,
  Node as SlateNode,
  NodeEntry,
  Text as SlateText,
  Transforms,
} from 'slate';
import { BlockFormatEnum } from '../../../baseTypes';

// Rules
// 1. Every text or inline node in align block should be wrapped into paragraph

export default (editor: Editor, entry: NodeEntry) => {
  const [node, path] = entry;

  const isAlignBlock = SlateElement.isElement(node) && [
    BlockFormatEnum.AlignLeft,
    BlockFormatEnum.AlignCenter,
    BlockFormatEnum.AlignRight,
    BlockFormatEnum.AlignJustify,
  ].includes(node.type);

  if (isAlignBlock) {
    for (const [child, childPath] of SlateNode.children(editor, path)) {
      if (SlateText.isText(child) || editor.isInline(child)) {
        Transforms.wrapNodes(editor, {
          type: BlockFormatEnum.Paragraph,
          children: [child],
        }, { at: childPath });
      }
    }
  }

  return false;
};
