/* eslint-disable no-param-reassign */
import { Editor, Element as SlateElement, Transforms } from 'slate';
import { BlockFormatEnum } from '../../../baseTypes';

const alignFormats = [
  BlockFormatEnum.AlignLeft,
  BlockFormatEnum.AlignCenter,
  BlockFormatEnum.AlignRight,
  BlockFormatEnum.AlignJustify,
];

const editorWrapper = (editor: Editor) => {
  editor.setTextAlign = (alignType) => {
    const [activeAlignment] = Editor.nodes<SlateElement>(editor, {
      match: (el) => SlateElement.isElement(el) && alignFormats.includes(el.type),
      mode: 'lowest',
    });

    if (activeAlignment) Transforms.liftNodes(editor);
    Transforms.wrapNodes(editor, { type: alignType || BlockFormatEnum.Paragraph, children: [] });
  };

  return editor;
};

export default editorWrapper;
