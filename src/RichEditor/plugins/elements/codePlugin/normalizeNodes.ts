import {
  Editor,
  Element as SlateElement,
  Node as SlateNode,
  NodeEntry,
  Text as SlateText,
  Transforms,
} from 'slate';
import { BlockFormatEnum } from '../../../baseTypes';

// Rules
// 1. Code blocks can contain only code lines
// 2. Code lines can contain only inline elements or text

export default (editor: Editor, entry: NodeEntry) => {
  const [node, path] = entry;

  if (SlateElement.isElementType(node, BlockFormatEnum.Code)) {
    for (const [child, childPath] of SlateNode.children(editor, path)) {
      if (SlateElement.isElement(child) && child.type !== BlockFormatEnum.CodeLine) {
        Transforms.unwrapNodes(editor, { at: childPath });
        Transforms.wrapNodes(editor, {
          type: BlockFormatEnum.CodeLine,
          children: [{ text: SlateNode.string(child) }],
        }, { at: childPath });

        return true;
      }

      if (SlateText.isText(child) || editor.isInline(child)) {
        Transforms.wrapNodes(editor, {
          type: BlockFormatEnum.CodeLine,
          children: [child],
        }, { at: childPath });

        return true;
      }
    }
  }

  if (SlateElement.isElementType(node, BlockFormatEnum.CodeLine)) {
    for (const [child, childPath] of SlateNode.children(editor, path)) {
      if (SlateElement.isElement(child) && !editor.isInline(child)) {
        Transforms.unwrapNodes(editor, { at: childPath });
        return true;
      }
    }
  }

  return false;
};
