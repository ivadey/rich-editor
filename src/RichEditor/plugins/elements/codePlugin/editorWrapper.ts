/* eslint-disable no-param-reassign */
import { Editor, Element as SlateElement, Transforms } from 'slate';
import { BlockFormatEnum } from '../../../baseTypes';
import { CodeElement } from './types';

const editorWrapper = (editor: Editor) => {
  const toggleCodeBlock = (isActive: boolean) => {
    Transforms.unwrapNodes(editor, {
      match: (node) => SlateElement.isElementType(node, BlockFormatEnum.Code),
      split: true,
    });

    Transforms.setNodes(editor, { type: isActive ? BlockFormatEnum.Paragraph : BlockFormatEnum.CodeLine });
    if (!isActive) Transforms.wrapNodes(editor, { type: BlockFormatEnum.Code, children: [] });
  };

  const getSelectedCodeBlock = () => {
    const [selectedCodeBlock] = Editor.nodes<CodeElement>(editor, {
      match: (node) => !Editor.isEditor(node)
        && SlateElement.isElement(node)
        && node.type === BlockFormatEnum.Code,
    });

    return selectedCodeBlock;
  };

  editor.insertCodeBlock = () => toggleCodeBlock(false);

  editor.changeCodeBlockLanguage = (language) => {
    const [codeBlock, path] = getSelectedCodeBlock() || [];

    if (codeBlock) Transforms.setNodes(editor, { lang: language }, { at: path });
  };

  editor.removeCodeBlock = () => toggleCodeBlock(true);

  return editor;
};

export default editorWrapper;
