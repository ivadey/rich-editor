import React from 'react';
import { Element as SlateElement, Text as SlateText } from 'slate';
import { InlineFormatEnum, BlockFormatEnum } from '../../../baseTypes';
import { BlockElementDescription, InlineElementDescription } from '../../types';
import { LanguageSelect } from './components';
// import './highlight.css';
import { codeBlockConverters, codeRenderer, inlineCodeConverters } from './helpers';

export default [{
  type: 'inline',
  formatName: InlineFormatEnum.InlineCode,
  render: ({ children, leaf }) => (leaf[InlineFormatEnum.InlineCode] ? React.createElement('code', {
    className: 're-inline-code',
    spellCheck: false,
  }, children) : children),
  convertors: inlineCodeConverters,
}, {
  type: 'block',
  formatName: BlockFormatEnum.Code,
  floatingMenu: [[LanguageSelect]],
  render: codeRenderer,
  enableAboveExitBreak: true,
  enableBelowExitBreak: true,
  convertors: codeBlockConverters,
}, {
  type: 'block',
  formatName: BlockFormatEnum.CodeLine,
  render: codeRenderer,
  convertors: {
    fromRemark: null,
    toRemark: (element: SlateElement) => [{
      type: 'inlineCode',
      value: (element.children[0] as SlateText).text,
    }],
  },
}, {
  // Only to highlight code snippets
  type: 'inline',
  formatName: InlineFormatEnum.CodeBlockHighlighting,
  render: ({ children, leaf }) => {
    const className: string[] = [];

    const { codeBlockHighlighting: highlight } = leaf;

    Object.keys(highlight || {}).forEach((token) => className.push(`token ${token}`));

    return React.createElement('span', { className: className.join(' ') }, children);
  },
  convertors: {
    fromRemark: null,
    toRemark: null,
  },
}] as Array<BlockElementDescription | InlineElementDescription>;
