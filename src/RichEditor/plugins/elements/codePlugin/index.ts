import React from 'react';
import {
  Editor,
  Element as SlateElement,
  Range as SlateRange,
} from 'slate';
import { ElementsPlugin } from '../../types';
import { CodeSnippetButton, InlineCodeButton } from './components';
import elements from './elements';
import editorWrapper from './editorWrapper';
import { BlockFormatEnum, InlineFormatEnum } from '../../../baseTypes';
import { IToolbarButtonProps } from '../../../components/Toolbar/types';
import { decorateCode } from './helpers';
import normalizeNodes from './normalizeNodes';

interface CodePlugin extends ElementsPlugin {
  toolbarButtons: {
    inlineCode: IToolbarButtonProps | React.ReactNode;
    codeSnippet: IToolbarButtonProps | React.ReactNode;
  }
}

const codePlugin: CodePlugin = {
  name: 'Code plugin',
  editorWrapper,
  toolbarButtons: {
    inlineCode: InlineCodeButton,
    codeSnippet: CodeSnippetButton,
  },
  elements,
  decorate: decorateCode,
  // todo: add 'shortcut' property to add block instead of custom onKeyDown handler and check it in onChange handler
  onKeyDownHandler: (event, editor) => {
    if (editor.selection && SlateRange.isCollapsed(editor.selection)) {
      const [currentNodeEntry] = Editor.nodes<SlateElement>(editor, {
        match: (node) => !Editor.isEditor(node) && SlateElement.isElement(node),
        at: editor.selection.focus.path,
      });

      if (currentNodeEntry && currentNodeEntry[0].type === BlockFormatEnum.Paragraph) {
        const elementText = Editor.string(editor, currentNodeEntry[1]);
        if (elementText === '``' && event.code === 'IntlBackslash') {
          event.preventDefault();

          editor.insertCodeBlock();
          Editor.deleteBackward(editor, { unit: 'line' });

          return true;
        }
      }
    }

    return false;
  },
  hotkeys: {
    'mod+shift+M': (editor) => editor.toggleMark(InlineFormatEnum.InlineCode),
    'alt+shift+N': (editor) => (!editor.isElementSelected(BlockFormatEnum.Code)
      ? editor.insertCodeBlock()
      : editor.removeCodeBlock()),
  },
  normalizeNodes,
};

export default codePlugin;
