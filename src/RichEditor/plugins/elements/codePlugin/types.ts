import { BaseElement } from 'slate';
import { BlockFormatEnum } from '../../../baseTypes';

export interface CodeEditor {
  insertCodeBlock: (language?: string) => void;
  changeCodeBlockLanguage: (language?: string) => void;
  removeCodeBlock: () => void;
}

export interface CodeElement extends BaseElement {
  type: BlockFormatEnum.Code;
  lang?: string;
}

export interface CodeLineElement extends BaseElement {
  type: BlockFormatEnum.CodeLine;
  lang?: string;
}

export type CodeSnippetHighlightedText = {
  text: string;
  // todo: think about better way to specify and organise this
  // https://prismjs.com/tokens.html
  codeBlockHighlighting?: {
    /** General */
    keyword: boolean;
    builtin: boolean;
    'class-name': boolean;
    function: boolean;
    boolean: boolean;
    number: boolean;
    string: boolean;
    char: boolean;
    symbol: boolean;
    regex: boolean;
    url: boolean;
    operator: boolean;
    variable: boolean;
    constant: boolean;
    property: boolean;
    punctuation: boolean;
    important: boolean;
    comment: boolean;
    /** Markup languages */
    tag: boolean;
    'attr-name': boolean;
    'attr-value': boolean;
    namespace: boolean;
    prolog: boolean;
    doctype: boolean;
    cdata: boolean;
    entity: boolean;
    /** Stylesheets */
    atrule: boolean;
    selector: boolean;
  };
}
