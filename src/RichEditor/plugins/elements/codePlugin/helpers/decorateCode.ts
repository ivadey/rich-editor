import {
  Editor,
  Element as SlateElement, Node as SlateNode, NodeEntry, Range as SlateRange, Text as SlateText,
} from 'slate';
import Prism, { Token } from 'prismjs';
import 'prismjs/components/prism-typescript';
import 'prismjs/components/prism-css';
import 'prismjs/components/prism-java';
import 'prismjs/components/prism-markup';
import 'prismjs/components/prism-bash';
import { BlockFormatEnum } from '../../../../baseTypes';

const getLength = (token: Token | string): number => {
  if (typeof token === 'string') {
    return token.length;
  } if (typeof token.content === 'string') {
    return token.content.length;
  }
  return !(token.content instanceof Token) ? token.content.reduce((l, t) => l + getLength(t), 0) : 0;
};

export default (editor: Editor, [node, path]: NodeEntry<SlateNode>): SlateRange[] => {
  const ranges: SlateRange[] = [];

  if (!SlateText.isText(node)) return ranges;

  const parentNode = SlateNode.parent(editor, path);
  if (!SlateElement.isElement(parentNode) || parentNode.type !== BlockFormatEnum.CodeLine) return ranges;

  const codeSnippetElement = SlateNode.parent(editor, path.slice(0, path.length - 1));
  if (!SlateElement.isElement(codeSnippetElement) || codeSnippetElement.type !== BlockFormatEnum.Code) return ranges;

  const tokens = Prism.tokenize(node.text, Prism.languages[codeSnippetElement.lang || 'markup']);

  let start = 0;

  for (const token of tokens) {
    const length = getLength(token);
    const end = start + length;

    if (typeof token !== 'string') {
      ranges.push({
        // todo: investigate how to fix ts error
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        codeBlockHighlighting: { [token.type]: true },
        anchor: { path, offset: start },
        focus: { path, offset: end },
      });
    }

    start = end;
  }

  return ranges;
};
