import { Descendant } from 'slate';
import { Code, InlineCode } from 'mdast';
import { BlockFormatEnum } from '../../../../baseTypes';
import { CodeElement } from '../types';
import { BlockElementDescription } from '../../../types';

export default {
  fromRemark: {
    mdastNodeType: 'code',
    convert: (source) => {
      const codeLines: Descendant[] = [];

      (source as Code).value.split('\n').forEach((codeLine) => {
        codeLines.push({
          type: BlockFormatEnum.CodeLine,
          children: [{ text: codeLine }],
        });
      });

      return ({
        type: BlockFormatEnum.Code,
        lang: (source as Code).lang,
        children: codeLines,
      });
    },
  },
  toRemark: (element: CodeElement, next) => [{
    type: 'code',
    lang: element.lang,
    value: (next(element.children) as InlineCode[])
      .map((codeLine) => codeLine.value)
      .join('\n'),
  }],
} as BlockElementDescription['convertors'];
