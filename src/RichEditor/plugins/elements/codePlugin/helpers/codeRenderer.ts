import { RenderElementProps } from 'slate-react';
import React from 'react';
import { BlockFormatEnum } from '../../../../baseTypes';

export default ({ element, children, attributes }: RenderElementProps) => {
  if (element.type === BlockFormatEnum.Code) {
    return React.createElement('pre', {
      className: 're-code-snippet',
      ...attributes,
    }, children);
  }

  if (element.type === BlockFormatEnum.CodeLine) {
    return React.createElement('code', {
      className: 're-code-snippet__line',
      spellCheck: false,
      ...attributes,
    }, children);
  }

  return null;
};
