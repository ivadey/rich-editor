import { InlineCode } from 'mdast';
import { InlineFormatEnum } from '../../../../baseTypes';
import { InlineElementDescription } from '../../../types';

export default {
  fromRemark: {
    mdastNodeType: 'inlineCode',
    convert: (source, next, decoration) => next([{
      ...source as InlineCode,
      type: 'text',
    }], { ...decoration, text: { ...decoration.text, [InlineFormatEnum.InlineCode]: true } }),
  },
  toRemark: {
    mdastNodeType: 'inlineCode',
    check: (source) => !!source[InlineFormatEnum.InlineCode],
    modifier: (source) => ({ type: 'inlineCode', value: source.text }),
  },
} as InlineElementDescription['convertors'];
