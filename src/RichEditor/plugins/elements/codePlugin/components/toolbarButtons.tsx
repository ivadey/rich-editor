import { InlineFormatEnum, BlockFormatEnum } from '../../../../baseTypes';
import { strings } from '../../../../helpers';
import { IToolbarButtonProps } from '../../../../components/Toolbar/types';

const InlineCodeButton: IToolbarButtonProps = {
  icon: 'code-simple',
  active: (editor) => editor.isMarkActive(InlineFormatEnum.InlineCode),
  onClick: (editor) => editor.toggleMark(InlineFormatEnum.InlineCode),
  tooltip: strings.inlineCode,
  hotkeyHint: 'mod+shift+M',
};

const CodeSnippetButton: IToolbarButtonProps = {
  icon: 'rectangle-code',
  active: (editor) => editor.isElementSelected(BlockFormatEnum.Code),
  tooltip: strings.codeSnippet,
  onClick: (editor) => (!editor.isElementSelected(BlockFormatEnum.Code)
    ? editor.insertCodeBlock()
    : editor.removeCodeBlock()),
  hotkeyHint: 'alt+shift+N',
};

export {
  InlineCodeButton,
  CodeSnippetButton,
};
