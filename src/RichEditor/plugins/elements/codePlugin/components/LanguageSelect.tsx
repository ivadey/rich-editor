import React from 'react';
import { useSlate } from 'slate-react';
import { Select } from '../../../../components';
import { CodeElement } from '../types';
import { strings } from '../../../../helpers';

const languagesOptions = [
  { id: 'markup', label: strings.notSelected },
  { id: 'javascript', label: 'javascript' },
  { id: 'typescript', label: 'typescript' },
  { id: 'java', label: 'java' },
  { id: 'bash', label: 'shell' },
  { id: 'html', label: 'html' },
  { id: 'css', label: 'css' },
];

const LanguageSelect: React.FC = () => {
  const editor = useSlate();

  const { element: currentElement } = editor.getCurrentElement<CodeElement>();

  if (!currentElement) return null;

  const handleSelect = (language: string) => editor.changeCodeBlockLanguage(language);

  return (
    <Select
      options={languagesOptions}
      value={currentElement.lang || ''}
      onChange={(lang) => handleSelect(lang as string)}
    />
  );
};

LanguageSelect.displayName = 'LanguageSelect';

export default <LanguageSelect />;
