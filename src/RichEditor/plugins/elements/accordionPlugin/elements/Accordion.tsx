/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { RenderElementProps } from 'slate-react';

const Accordion = ({ attributes, children }: RenderElementProps): JSX.Element => (
  <div className="re-accordion" {...attributes}>
    {children}
  </div>
);

Accordion.displayName = 'Accordion';

export default Accordion;
