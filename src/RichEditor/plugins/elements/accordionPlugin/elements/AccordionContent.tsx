/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { RenderElementProps } from 'slate-react';

const AccordionContent = ({ attributes, children }: RenderElementProps): JSX.Element => (
  <div className="re-accordion__content" {...attributes}>
    {children}
  </div>
);

AccordionContent.displayName = 'AccordionContent';

export default AccordionContent;
