/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { RenderElementProps } from 'slate-react';
import { Icon } from '../../../../components';

const AccordionTitle = ({ attributes, children }: RenderElementProps): JSX.Element => (
  <div className="re-accordion__title-container" {...attributes}>
    <button
      type="button"
      contentEditable={false}
      onClick={(event) => {
        let accordionElement = event.target as HTMLElement;
        while (!accordionElement.classList.contains('re-accordion')) {
          accordionElement = accordionElement.parentElement as HTMLElement;
        }
        accordionElement.classList.toggle('opened');
      }}
    >
      <Icon icon="chevron-right" />
    </button>
    <div className="re-accordion__title-container__title">{children}</div>
  </div>
);

AccordionTitle.displayName = 'AccordionTitle';

export default AccordionTitle;
