import { ContainerDirective } from 'mdast-util-directive';
import { Content } from 'mdast';
import { BlockElementDescription } from '../../../types';
import { BlockFormatEnum } from '../../../../baseTypes';
import Accordion from './Accordion';
import AccordionTitle from './AccordionTitle';
import AccordionContent from './AccordionContent';
import { AccordionContentElement, AccordionElement, AccordionTitleElement } from '../types';
import { strings } from '../../../../helpers';

export default [
  {
    type: 'block',
    formatName: BlockFormatEnum.Accordion,
    render: Accordion,
    enableAboveExitBreak: true,
    enableBelowExitBreak: true,
    convertors: {
      fromRemark: {
        mdastNodeType: 'containerDirective',
        check: (source) => (source as ContainerDirective).name === BlockFormatEnum.Accordion,
        convert: (source, next, decoration) => [{
          type: BlockFormatEnum.Accordion,
          children: next((source as ContainerDirective).children, decoration),
        }],
      },
      toRemark: (element: AccordionElement, next) => ([{
        type: 'containerDirective',
        name: BlockFormatEnum.Accordion,
        attributes: {},
        children: next(element.children),
      }] as Content[]),
    },
  },
  {
    type: 'block',
    formatName: BlockFormatEnum.AccordionTitle,
    render: AccordionTitle,
    floatingMenu: [
      [
        {
          icon: 'trash',
          tooltip: strings.remove,
          onClick: (editor) => editor.removeAccordion(),
        },
      ],
    ],
    convertors: {
      fromRemark: {
        mdastNodeType: 'containerDirective',
        check: (source) => (source as ContainerDirective).name === BlockFormatEnum.AccordionTitle,
        convert: (source, next, decoration) => [{
          type: BlockFormatEnum.AccordionTitle,
          children: next((source as ContainerDirective).children, decoration),
        }],
      },
      toRemark: (element: AccordionTitleElement, next) => ([{
        type: 'containerDirective',
        name: BlockFormatEnum.AccordionTitle,
        attributes: {},
        children: next(element.children),
      }] as Content[]),
    },
  },
  {
    type: 'block',
    formatName: BlockFormatEnum.AccordionContent,
    render: AccordionContent,
    convertors: {
      fromRemark: {
        mdastNodeType: 'containerDirective',
        check: (source) => (source as ContainerDirective).name === BlockFormatEnum.AccordionContent,
        convert: (source, next, decoration) => [{
          type: BlockFormatEnum.AccordionContent,
          children: next((source as ContainerDirective).children, decoration),
        }],
      },
      toRemark: (element: AccordionContentElement, next) => ([{
        type: 'containerDirective',
        name: BlockFormatEnum.AccordionContent,
        attributes: {},
        children: next(element.children),
      }] as Content[]),
    },
  },

] as BlockElementDescription[];
