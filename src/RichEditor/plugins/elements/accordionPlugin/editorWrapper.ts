/* eslint-disable no-param-reassign */
import {
  Descendant,
  Editor,
  Element as SlateElement,
  NodeEntry,
  Transforms,
} from 'slate';
import { BlockFormatEnum } from '../../../baseTypes';
import { AccordionElement } from './types';

const editorWrapper = (editor: Editor) => {
  const { insertBreak } = editor;

  editor.insertBreak = () => {
    if (editor.isElementSelected(BlockFormatEnum.AccordionTitle)) return;

    insertBreak();
  };

  const getCurrentAccordion = (): NodeEntry<AccordionElement> | undefined => {
    if (!editor.selection) return undefined;

    const [match] = Editor.nodes<AccordionElement>(editor, {
      match: (node) => SlateElement.isElementType(node, BlockFormatEnum.Accordion),
      mode: 'lowest',
      at: editor.selection.focus.path,
    });

    return match;
  };

  editor.insertAccordion = () => {
    const accordionNode: Descendant = {
      type: BlockFormatEnum.Accordion,
      children: [{
        type: BlockFormatEnum.AccordionTitle,
        children: [{ text: '' }],
      }, {
        type: BlockFormatEnum.AccordionContent,
        children: [{
          type: BlockFormatEnum.Paragraph,
          children: [{ text: '' }],
        }],
      }] as AccordionElement['children'],
    };

    if (editor.selection && editor.isBlockEmpty(editor.selection.anchor.path)) {
      const focusPath = editor.selection.focus.path;

      Editor.withoutNormalizing(editor, () => {
        Transforms.setNodes(editor, accordionNode);
        Transforms.insertNodes<SlateElement>(editor, accordionNode.children, { at: focusPath });
      });
    } else Transforms.insertNodes<SlateElement>(editor, accordionNode);

    const [, path] = getCurrentAccordion() || [];
    if (path) Transforms.select(editor, [...path, 0]);
  };

  editor.removeAccordion = () => {
    const [, path] = getCurrentAccordion() || [];
    if (path) Transforms.removeNodes(editor, { at: path });
  };

  return editor;
};

export default editorWrapper;
