import {
  Editor,
  Element as SlateElement,
  Node as SlateNode,
  NodeEntry,
  Transforms,
} from 'slate';
import { BlockFormatEnum } from '../../../baseTypes';
import { AccordionContentElement, AccordionElement } from './types';

// Rules
// 1. Accordion element should always contain AccordionTitle and AccordionContent elements
// todo 2. Allowed only one AccordionTitle and AccordionContent per Accordion
// 3. Accordion title may contain only formatted text
// 4. Accordion content may contain only block elements

export default (editor: Editor, entry: NodeEntry) => {
  const [node, path] = entry;

  if (SlateElement.isElementType<AccordionElement>(node, BlockFormatEnum.Accordion)) {
    const isTitleExist = node.children
      .some((child) => SlateElement.isElementType(child, BlockFormatEnum.AccordionTitle));
    const isContentExist = node.children
      .some((child) => SlateElement.isElementType(child, BlockFormatEnum.AccordionContent));

    // Add accordion title if not exist
    if (!isTitleExist) {
      Transforms.insertNodes(editor, {
        type: BlockFormatEnum.AccordionTitle,
        children: [{ text: '' }],
      }, { at: [...path, 0] });

      return true;
    }

    // Add accordion content if not exist
    if (!isContentExist) {
      Transforms.insertNodes(editor, {
        type: BlockFormatEnum.AccordionContent,
        children: [{
          type: BlockFormatEnum.Paragraph,
          children: [{ text: '' }],
        }],
      }, { at: [...path, 1] });

      return true;
    }

    // Copy all other elements on accordion content section
    for (const [child, childPath] of SlateNode.children(editor, path)) {
      if (
        !SlateElement.isElementType(child, BlockFormatEnum.AccordionTitle)
        && !SlateElement.isElementType(child, BlockFormatEnum.AccordionContent)
      ) {
        const [accordionContentMatch] = Editor.nodes<AccordionContentElement>(editor, {
          match: (n) => SlateElement.isElementType(n, BlockFormatEnum.AccordionContent),
          at: path,
        });
        const [accordionContentElement, accordionContentPath] = accordionContentMatch;

        Editor.withoutNormalizing(editor, () => {
          const isEmpty = !SlateNode.string(child)
            && !(SlateElement.isElement(child) && editor.isVoid(child));

          if (!isEmpty) {
            Transforms.insertNodes(editor, child, {
              at: [...accordionContentPath, accordionContentElement.children.length],
            });
          }
          Transforms.removeNodes(editor, { at: childPath });
        });

        return true;
      }
    }
  }

  if (SlateElement.isElementType(node, BlockFormatEnum.AccordionTitle)) {
    for (const [child, childPath] of SlateNode.children(editor, path)) {
      if (SlateElement.isElement(child) && !editor.isInline(child)) {
        Transforms.unwrapNodes(editor, { at: childPath });

        return true;
      }
    }
  }

  if (SlateElement.isElementType(node, BlockFormatEnum.AccordionContent)) {
    for (const [child, childPath] of SlateNode.children(editor, path)) {
      if (!SlateElement.isElement(child) || editor.isInline(child)) {
        Transforms.wrapNodes(editor, {
          type: BlockFormatEnum.Paragraph,
          children: [],
        }, { at: childPath });

        return true;
      }
    }
  }

  return false;
};
