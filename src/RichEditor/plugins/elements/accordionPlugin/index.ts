import React from 'react';
import { ElementsPlugin } from '../../types';
import { AccordionButton } from './toolbarButtons';
import editorWrapper from './editorWrapper';
import { IToolbarButtonProps } from '../../../components/Toolbar/types';
import elements from './elements';
import normalizeNodes from './normalizeNodes';

interface AccordionPlugin extends ElementsPlugin {
  toolbarButtons: {
    accordion: IToolbarButtonProps | React.ReactNode;
  }
}

const accordionPlugin: AccordionPlugin = {
  name: 'Accordion plugin',
  editorWrapper,
  toolbarButtons: {
    accordion: AccordionButton,
  },
  elements,
  onMounted: () => {
    const openAccordionWithAnchorIfNeeded = () => {
      const anchorId = window.location.hash?.slice(1);
      const anchorElement = document.getElementById(anchorId);

      if (!anchorElement) return;

      let parent = anchorElement.parentElement;
      while (parent) {
        if (parent.classList.contains('re-accordion')) parent.classList.add('opened');
        parent = parent.parentElement;
      }

      anchorElement.scrollIntoView();
    };

    openAccordionWithAnchorIfNeeded();

    window.addEventListener('hashchange', openAccordionWithAnchorIfNeeded, false);
  },
  normalizeNodes,
};

export default accordionPlugin;
