import { strings } from '../../../helpers';
import { IToolbarButtonProps } from '../../../components/Toolbar/types';

const AccordionButton: IToolbarButtonProps = {
  icon: 'circle-caret-down',
  tooltip: strings.accordion,
  onClick: (editor) => editor.insertAccordion(),
};
export {
  // eslint-disable-next-line import/prefer-default-export
  AccordionButton,
};
