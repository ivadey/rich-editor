import { BaseElement } from 'slate';
import { BlockFormatEnum } from '../../../baseTypes';

export interface AccordionEditor {
  insertAccordion: () => void;
  removeAccordion: () => void;
}

export interface AccordionElement extends BaseElement {
  type: BlockFormatEnum.Accordion;
}

export interface AccordionTitleElement extends BaseElement {
  type: BlockFormatEnum.AccordionTitle;
}

export interface AccordionContentElement extends BaseElement {
  type: BlockFormatEnum.AccordionContent;
}
