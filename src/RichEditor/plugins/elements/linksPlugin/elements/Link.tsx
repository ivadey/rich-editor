/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { RenderElementProps, useReadOnly } from 'slate-react';
import { LinkElement } from '../types';

export interface LinkRenderElementProps extends RenderElementProps {
  element: LinkElement;
}

const Link: React.FC<LinkRenderElementProps> = ({ element, attributes, children }) => {
  const readonly = useReadOnly();

  return (
    <a
      className="re-link"
      href={element.link}
      target={element.link.startsWith('#') ? undefined : '_blank'}
      rel="noreferrer noopener nofollow"
      onClick={readonly ? undefined : (event) => {
        // eslint-disable-next-line deprecation/deprecation
        const isCtrlPressed = navigator.platform.toUpperCase().indexOf('MAC') > -1 ? event.metaKey : event.ctrlKey;

        if (isCtrlPressed) {
          if (element.link.startsWith('#')) {
            const { origin, pathname } = window.location;
            const newUrl = `${origin}${pathname}${element.link}`;

            window.location.replace(newUrl);
          } else window.open(element.link, '_blank');
        }
      }}
      {...attributes}
    >
      <span contentEditable={false}>‎</span>
      {children}
    </a>
  );
};

Link.displayName = 'Link';

export default Link;
