import React from 'react';
import { Content, Link as MdastLinkType } from 'mdast';
import { BlockElementDescription, InlineElementDescription } from '../../../types';
import Link, { LinkRenderElementProps } from './Link';
import { strings } from '../../../../helpers';
import showLinkPrompt from '../showLinkPrompt';
import { BlockFormatEnum, InlineFormatEnum } from '../../../../baseTypes';
import { LinkElement } from '../types';
import DecoratedLink from './DecoratedLink';

export default [{
  type: 'block',
  formatName: BlockFormatEnum.Link,
  render: (props) => React.createElement(Link, props as LinkRenderElementProps),
  floatingMenu: [
    [
      {
        icon: 'edit',
        tooltip: strings.edit,
        onClick: (editor) => {
          const currentLinkUrl = editor.getCurrentLinkUrl();
          showLinkPrompt(editor, currentLinkUrl);
        },
      },
    ],
    [
      {
        icon: 'trash',
        tooltip: strings.remove,
        onClick: (editor) => editor.removeLink(),
      },
    ],
  ],
  convertors: {
    fromRemark: {
      mdastNodeType: 'link',
      convert: (source, next, decoration) => ({
        type: BlockFormatEnum.Link,
        link: (source as MdastLinkType).url,
        children: next((source as MdastLinkType).children, decoration),
      }),
    },
    toRemark: (element: LinkElement, next) => [{
      type: 'link',
      url: element.link,
      children: next(element.children),
    }] as Content[],
  },
}, {
  type: 'inline',
  formatName: InlineFormatEnum.DecoratedLink,
  render: DecoratedLink,
  convertors: {
    fromRemark: null,
    toRemark: null,
  },
}] as Array<BlockElementDescription | InlineElementDescription>;
