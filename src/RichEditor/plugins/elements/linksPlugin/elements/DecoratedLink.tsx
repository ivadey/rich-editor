/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { RenderLeafProps } from 'slate-react';
import { InlineFormatEnum } from '../../../../baseTypes';

const DecoratedLink: React.FC<RenderLeafProps> = ({ leaf, children }) => (
  <a
    className="re-decorated-link"
    href={leaf[InlineFormatEnum.DecoratedLink]}
    target="_blank"
    rel="noreferrer noopener nofollow"
  >
    <span contentEditable={false}>‎</span>
    {children}
  </a>
);

DecoratedLink.displayName = 'DecoratedLink';

export default DecoratedLink;
