import { Editor } from 'slate';
import isUrl from 'is-url';
import dialog from '../../../../dialog';
import { strings } from '../../../helpers';

const emailRegex = /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/i;

export default (editor: Editor, currentLink?: string) => {
  const selectedText = editor.getSelectedText();
  let initialValue = currentLink || '#';

  if (!currentLink) {
    if (emailRegex.test(selectedText)) initialValue = `mailto:${selectedText}`;
    else if (isUrl(selectedText)) initialValue = selectedText;
    else if (isUrl(`https://${selectedText}`)) initialValue = `https://${selectedText}`;
  }

  dialog.prompt({ message: strings.enterLink, initialValue })
    .then((link) => {
      if (link && currentLink) editor.changeLink(link);
      else if (link) editor.addLink(link);
      else if (currentLink) editor.removeLink();
    })
    .catch(() => null);
};
