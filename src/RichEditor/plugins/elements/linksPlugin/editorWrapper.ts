/* eslint-disable no-param-reassign */
import {
  Editor,
  Element as SlateElement,
  Range as SlateRange,
  Transforms,
} from 'slate';
import { LinkElement } from './types';
import { BlockFormatEnum } from '../../../baseTypes';

const isLinkActive = (editor: Editor) => {
  const [link] = Editor.nodes(editor, {
    match: (n) => !Editor.isEditor(n) && SlateElement.isElement(n) && n.type === BlockFormatEnum.Link,
  });
  return !!link;
};

const unwrapLink = (editor: Editor) => {
  Transforms.unwrapNodes(editor, {
    match: (n) => !Editor.isEditor(n) && SlateElement.isElement(n) && n.type === BlockFormatEnum.Link,
  });
};

const editorWrapper = (editor: Editor) => {
  const { isInline } = editor;

  editor.isInline = (element) => element.type === BlockFormatEnum.Link || isInline(element);

  const getSelectedLink = () => {
    const [selectedLink] = Editor.nodes<LinkElement>(editor, {
      match: (node) => !Editor.isEditor(node)
        && SlateElement.isElement(node)
        && node.type === BlockFormatEnum.Link,
    });

    return selectedLink;
  };

  editor.addLink = (linkUrl) => {
    if (!editor.selection || SlateRange.isCollapsed(editor.selection)) return;

    if (!linkUrl) return;

    if (isLinkActive(editor)) {
      unwrapLink(editor);
    }

    const linkElement: LinkElement = {
      type: BlockFormatEnum.Link,
      link: linkUrl,
      children: [],
    };

    Transforms.wrapNodes(editor, linkElement, { split: true });
    Transforms.collapse(editor, { edge: 'end' });
  };
  editor.changeLink = (linkUrl) => {
    const [selectedLink, path] = getSelectedLink() || [];

    if (selectedLink && linkUrl) {
      Transforms.setNodes(editor, { link: linkUrl }, { at: path });
    }
  };
  editor.removeLink = () => {
    const [selectedLink] = getSelectedLink() || [];

    if (selectedLink) unwrapLink(editor);
  };
  editor.getCurrentLinkUrl = () => getSelectedLink()?.[0]?.link;

  return editor;
};

export default editorWrapper;
