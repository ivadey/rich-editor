import { BaseElement, BaseText } from 'slate';
import { BlockFormatEnum, InlineFormatEnum } from '../../../baseTypes';

export interface LinkEditor {
  addLink: (link: string) => void;
  removeLink: () => void;
  changeLink: (link: string) => void;
  getCurrentLinkUrl: () => string | undefined;
}

export interface LinkElement extends BaseElement {
  type: BlockFormatEnum.Link;
  link: string;
}

export interface DecoratedLinkText extends BaseText {
  [InlineFormatEnum.DecoratedLink]?: string;
}
