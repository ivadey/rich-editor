import React from 'react';
import { ElementsPlugin } from '../../types';
import editorWrapper from './editorWrapper';
import elements from './elements';
import { LinkButton } from './toolbarButtons';
import { IToolbarButtonProps } from '../../../components/Toolbar/types';
import showLinkPrompt from './showLinkPrompt';
import decorateLinks from './decorateLinks';

interface LinksPlugin extends ElementsPlugin {
  toolbarButtons: {
    link: IToolbarButtonProps | React.ReactNode;
  }
}

const linksPlugin: LinksPlugin = {
  name: 'Links plugin',
  editorWrapper,
  toolbarButtons: {
    link: LinkButton,
  },
  decorate: decorateLinks,
  elements,
  hotkeys: {
    'mod+shift+k': (editor) => showLinkPrompt(editor),
  },
};

export default linksPlugin;
