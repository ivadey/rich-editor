import { strings } from '../../../helpers';
import { BlockFormatEnum } from '../../../baseTypes';
import showLinkPrompt from './showLinkPrompt';
import { IToolbarButtonProps } from '../../../components/Toolbar/types';

const LinkButton: IToolbarButtonProps = {
  icon: 'link',
  tooltip: strings.link,
  active: (editor) => editor.isElementSelected(BlockFormatEnum.Link),
  onClick: (editor) => showLinkPrompt(editor),
  hotkeyHint: 'mod+shift+K',
};

export {
  // eslint-disable-next-line import/prefer-default-export
  LinkButton,
};
