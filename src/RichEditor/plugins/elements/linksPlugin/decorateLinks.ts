import {
  Editor,
  NodeEntry,
  Node as SlateNode,
  Text as SlateText,
  Range as SlateRange,
  Element as SlateElement,
} from 'slate';
import isUrl from 'is-url';
import { ReactEditor } from 'slate-react';
import { BlockFormatEnum, InlineFormatEnum } from '../../../baseTypes';

export default (editor: Editor, [node, path]: NodeEntry<SlateNode>): SlateRange[] => {
  const ranges: SlateRange[] = [];

  let linkStart = 0;
  if (ReactEditor.isReadOnly(editor) && SlateText.isText(node)) {
    const [parent] = Editor.above(editor, { at: path }) ?? [];
    if (parent && !(SlateElement.isElement(parent) && parent.type === BlockFormatEnum.Link)) {
      node.text.match(/(http|https):\/\/[\S]+/g)?.forEach((entry: string) => {
        if (isUrl(entry)) {
          let decoratedLink = entry;

          linkStart = node.text.indexOf(entry, linkStart);
          let end = linkStart + entry.length;

          if (/[.,;!?]$/.test(entry)) {
            end -= 1;
            decoratedLink = decoratedLink.slice(0, decoratedLink.length - 1);
          }

          ranges.push({
            // todo: investigate how to fix ts error
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            [InlineFormatEnum.DecoratedLink]: decoratedLink,
            anchor: { path, offset: linkStart },
            focus: { path, offset: end },
          });
        }
      });
    }
  }

  return ranges;
};
