import { BaseElement } from 'slate';
import { BlockFormatEnum } from '../../../baseTypes';

export interface HeadingElement extends BaseElement {
  type: BlockFormatEnum.Heading;
  level: number;
}

export interface HeadingEditor {
  toggleHeading: (level: number) => void;
}
