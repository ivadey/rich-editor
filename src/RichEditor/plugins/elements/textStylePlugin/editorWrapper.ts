/* eslint-disable no-param-reassign */
import {
  Editor,
  Element as SlateElement,
  Transforms,
} from 'slate';
import { BlockFormatEnum } from '../../../baseTypes';
import isHeadingActive from './isHeadingActive';

const editorWrapper = (editor: Editor): Editor => {
  editor.toggleHeading = (level) => {
    if (!editor.selection) return;

    if (!isHeadingActive(editor, level)) {
      Transforms.setNodes<SlateElement>(editor, {
        type: BlockFormatEnum.Heading,
        level,
      });
    } else Transforms.setNodes<SlateElement>(editor, { type: BlockFormatEnum.Paragraph });
  };

  return editor;
};

export default editorWrapper;
