import { RenderElementProps } from 'slate-react';
import React from 'react';
import { Heading } from 'mdast';
import { BlockElementDescription } from '../../types';
import { BlockFormatEnum } from '../../../baseTypes';
import { HeadingElement } from './types';

export default [{
  type: 'block',
  formatName: BlockFormatEnum.Heading,
  clearFormatOnEnter: true,
  render: ({ element, children, attributes }: RenderElementProps) => {
    if (element.type === BlockFormatEnum.Heading) {
      let TagName: keyof JSX.IntrinsicElements;

      if (element.level === 1) TagName = 'h1';
      else if (element.level === 2) TagName = 'h2';
      else if (element.level === 3) TagName = 'h3';
      else if (element.level === 4) TagName = 'h4';
      else if (element.level === 5) TagName = 'h5';
      else TagName = 'h6';

      return React.createElement(TagName, { className: `re-heading ${TagName}`, ...attributes }, children);
    }

    return null;
  },
  convertors: {
    fromRemark: {
      mdastNodeType: 'heading',
      convert: (source: Heading, next) => ({
        type: BlockFormatEnum.Heading,
        level: source.depth,
        children: next(source.children),
      }),
    },
    toRemark: (element: HeadingElement, next) => [{
      type: 'heading',
      depth: element.level,
      children: next(element.children),
    }],
  },
}] as BlockElementDescription[];
