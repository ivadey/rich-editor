import React from 'react';
import { useSlate } from 'slate-react';
import { Element as SlateElement, Transforms } from 'slate';
import { BlockFormatEnum } from '../../../baseTypes';
import { strings } from '../../../helpers';
import { Select } from '../../../components';
import isHeadingActive from './isHeadingActive';

export type TFormatOption = {
  id: string;
  name: BlockFormatEnum;
  level?: number;
  label: string;
}

const formatOptions: TFormatOption[] = [
  { id: BlockFormatEnum.Paragraph, name: BlockFormatEnum.Paragraph, label: strings.normalText },
];

Array.from(Array(6)).forEach((_, ind) => {
  formatOptions.push({
    id: `${BlockFormatEnum.Heading}-${ind + 1}`,
    name: BlockFormatEnum.Heading,
    level: ind + 1,
    label: strings.formatString(strings.headingN, ind + 1) as string,
  });
});

const BlockFormatSelect = () => {
  const editor = useSlate();

  let activeOption = formatOptions.find((f) => f.level && isHeadingActive(editor, f.level)) as TFormatOption;
  if (!activeOption) activeOption = formatOptions[0] as TFormatOption;

  const handleSelect = (formatId: string | number) => {
    const selectedFormat = formatOptions.find((f) => f.id === formatId);

    if (!selectedFormat) return;

    if (selectedFormat.name === BlockFormatEnum.Heading && formatId !== activeOption.id) {
      Transforms.setNodes<SlateElement>(editor, {
        type: BlockFormatEnum.Heading,
        level: selectedFormat.level,
      });
    } else if (selectedFormat.name === BlockFormatEnum.Paragraph) {
      Transforms.setNodes<SlateElement>(editor, { type: BlockFormatEnum.Paragraph });
    }
  };

  return (
    <Select
      options={formatOptions}
      value={activeOption.id}
      onChange={handleSelect}
    />
  );
};

export default <BlockFormatSelect />;
