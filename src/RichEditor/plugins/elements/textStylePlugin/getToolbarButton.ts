import { IToolbarButtonProps } from '../../../components/Toolbar/types';
import { strings } from '../../../helpers';
import isHeadingActive from './isHeadingActive';

export default (level: 1 | 2 | 3 | 4 | 5 | 6) => ({
  icon: `h${level}`,
  active: (editor) => isHeadingActive(editor, level),
  tooltip: strings.formatString(strings.headingN, level) as string,
  hotkeyHint: `mod+alt+${level}`,
  onClick: (editor) => editor.toggleHeading(level),
} as IToolbarButtonProps);
