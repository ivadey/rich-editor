import { Editor, Element as SlateElement } from 'slate';
import { BlockFormatEnum } from '../../../baseTypes';

export default (editor: Editor, level: number) => {
  const [currentHeading] = Editor.nodes(editor, {
    match: (node) => !Editor.isEditor(node)
      && SlateElement.isElement(node)
      && node.type === BlockFormatEnum.Heading
      && node.level === level,
  });

  return !!currentHeading;
};
