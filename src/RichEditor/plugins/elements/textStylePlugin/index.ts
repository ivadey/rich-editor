import React from 'react';
import { Element as SlateElement, Transforms } from 'slate';
import { ElementsPlugin } from '../../types';
import TextStylesListToolbarSelect from './TextStylesListToolbarSelect';
import elements from './elements';
import { IToolbarButtonProps } from '../../../components/Toolbar/types';
import getToolbarButton from './getToolbarButton';
import editorWrapper from './editorWrapper';
import { BlockFormatEnum } from '../../../baseTypes';

interface TextStylesPlugin extends ElementsPlugin {
  toolbarButtons: {
    textStylesList: React.ReactNode;
    headingOne: IToolbarButtonProps | React.ReactNode;
    headingTwo: IToolbarButtonProps | React.ReactNode;
    headingThree: IToolbarButtonProps | React.ReactNode;
    headingFour: IToolbarButtonProps | React.ReactNode;
    headingFive: IToolbarButtonProps | React.ReactNode;
    headingSix: IToolbarButtonProps | React.ReactNode;
  }
}

const textStylesPlugin: TextStylesPlugin = {
  name: 'Text styles plugin',
  toolbarButtons: {
    textStylesList: TextStylesListToolbarSelect,
    headingOne: getToolbarButton(1),
    headingTwo: getToolbarButton(2),
    headingThree: getToolbarButton(3),
    headingFour: getToolbarButton(4),
    headingFive: getToolbarButton(5),
    headingSix: getToolbarButton(6),
  },
  editorWrapper,
  elements,
  hotkeys: {
    'mod+alt+1': (editor) => editor.toggleHeading(1),
    'mod+alt+2': (editor) => editor.toggleHeading(2),
    'mod+alt+3': (editor) => editor.toggleHeading(3),
    'mod+alt+4': (editor) => editor.toggleHeading(4),
    'mod+alt+5': (editor) => editor.toggleHeading(5),
    'mod+alt+6': (editor) => editor.toggleHeading(6),
    'mod+alt+0': (editor) => Transforms.setNodes<SlateElement>(editor, { type: BlockFormatEnum.Paragraph }),
  },
};

export default textStylesPlugin;
