import React from 'react';
import {
  Editor, Element as SlateElement, Range as SlateRange, Transforms,
} from 'slate';
import isHotkey from 'is-hotkey';
import { ElementsPlugin } from '../../types';
import editorWrapper from './editorWrapper';
import elements from './elements';
import { ImageButton } from './toolbarButtons';
import { IToolbarButtonProps } from '../../../components/Toolbar/types';
import normalizeNodes from './normalizeNodes';
import { BlockFormatEnum } from '../../../baseTypes';

interface ImagesPlugin extends ElementsPlugin {
  toolbarButtons: {
    image: IToolbarButtonProps | React.ReactNode;
  }
}

const imagesPlugin: ImagesPlugin = {
  name: 'Images plugin',
  editorWrapper,
  toolbarButtons: {
    image: ImageButton,
  },
  elements,
  onKeyDownHandler: (event, editor) => {
    if (!editor.selection) return false;

    if (isHotkey('backspace', event) || isHotkey('delete', event)) {
      if (!SlateRange.isCollapsed(editor.selection) || editor.selection.anchor.offset !== 0) return false;

      const currentString = editor.getCurrentString();
      if (currentString) return false;

      const targetNodeEntry = isHotkey('backspace', event)
        ? Editor.previous(editor)
        : Editor.next(editor);

      if (!targetNodeEntry) return false;
      if (!SlateElement.isElementType(targetNodeEntry[0], BlockFormatEnum.Image)) return false;

      const { path } = editor.getCurrentElement();
      if (!path) return false;

      event.preventDefault();
      Transforms.select(editor, targetNodeEntry[1]);
      Transforms.removeNodes(editor, { at: path });

      return true;
    }

    return false;
  },
  normalizeNodes,
};

export default imagesPlugin;
