/* eslint-disable no-param-reassign */
import {
  Editor,
  Element as SlateElement,
  NodeEntry,
  Transforms,
} from 'slate';
import { BlockFormatEnum } from '../../../baseTypes';
import { ImageElement, ImagePosition } from './types';

const editorWrapper = (editor: Editor) => {
  const { isVoid, insertBreak } = editor;

  editor.isVoid = (element) => (element.type === BlockFormatEnum.Image ? true : isVoid(element));
  editor.insertBreak = () => {
    const { selection } = editor;

    if (selection) {
      const [image] = Editor.nodes(editor, {
        match: (n) => !Editor.isEditor(n) && SlateElement.isElement(n) && n.type === BlockFormatEnum.Image,
      });

      if (image) {
        editor.insertNode({
          type: BlockFormatEnum.Paragraph,
          children: [{ text: '' }],
        });
        return;
      }
    }

    insertBreak();
  };

  const getSelectedImage = (): NodeEntry<ImageElement> | undefined => {
    const [selectedImage] = Editor.nodes<ImageElement>(editor, {
      match: (node) => !Editor.isEditor(node)
        && SlateElement.isElement(node)
        && node.type === BlockFormatEnum.Image,
      mode: 'lowest',
    });

    return selectedImage;
  };

  editor.insertImage = (imageUrl: string) => {
    Transforms.insertNodes(editor, {
      type: BlockFormatEnum.Image,
      imageUrl,
      position: ImagePosition.AlignCenter,
      children: [{ text: '' }],
    });
  };
  editor.removeImage = () => {
    const [image, path] = getSelectedImage() || [];

    if (image) Transforms.removeNodes(editor, { at: path });
  };

  editor.getImageUrl = () => {
    const [image] = getSelectedImage() || [];
    return image?.imageUrl;
  };
  editor.changeImageUrl = (imageUrl: string) => {
    if (!imageUrl) return;

    const [image] = getSelectedImage() || [];
    if (image) Transforms.setNodes(editor, { imageUrl });
  };

  editor.getImagePosition = () => {
    const [image] = getSelectedImage() || [];
    return image?.position;
  };
  editor.setImagePosition = (position) => {
    const [image] = getSelectedImage() || [];
    if (image) Transforms.setNodes(editor, { position });
  };

  editor.getCurrentImageCaption = () => {
    const [image] = getSelectedImage() || [];
    return image?.caption;
  };
  editor.changeImageCaption = (caption) => (caption
    ? Transforms.setNodes(editor, { caption })
    : Transforms.unsetNodes(editor, 'caption'));

  editor.getCurrentImageWidthInPercent = () => {
    const [image] = getSelectedImage() || [];
    return image?.widthInPercent;
  };
  editor.setCurrentImageWidthInPercent = (widthInPercent) => {
    if (widthInPercent <= 0 || widthInPercent > 100) return;

    const [image] = getSelectedImage() || [];
    if (image) Transforms.setNodes(editor, { widthInPercent });
  };

  return editor;
};

export default editorWrapper;
