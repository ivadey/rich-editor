import {
  Editor,
  Element as SlateElement,
  NodeEntry, Transforms,
} from 'slate';
import { BlockFormatEnum } from '../../../baseTypes';

// Rules
// 1. Image shouldn't be wrapped into paragraph because it cause dom validation error (figure should not be nested in p)

export default (editor: Editor, entry: NodeEntry) => {
  const [node, path] = entry;

  if (SlateElement.isElementType(node, BlockFormatEnum.Image)) {
    const [parent] = Editor.parent(editor, path);
    if (SlateElement.isElementType(parent, BlockFormatEnum.Paragraph)) {
      Transforms.liftNodes(editor, { at: path });

      return true;
    }
  }

  return false;
};
