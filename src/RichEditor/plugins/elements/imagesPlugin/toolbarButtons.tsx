import { strings } from '../../../helpers';
import { BlockFormatEnum } from '../../../baseTypes';
import { IToolbarButtonProps } from '../../../components/Toolbar/types';
import dialog from '../../../../dialog';

const ImageButton: IToolbarButtonProps = {
  icon: 'image',
  tooltip: strings.image,
  active: (editor) => editor.isElementSelected(BlockFormatEnum.Image),
  onClick: (editor) => {
    dialog.prompt({ message: strings.enterImageLink }).then((imageUrl) => {
      if (imageUrl && !/^(http)|(https)/.test(imageUrl)) {
        dialog.alert({ message: strings.wrongLink }).catch(() => null);
        return;
      }

      if (imageUrl) editor.insertImage(imageUrl);
    }).catch(() => null);
  },
};

export {
  // eslint-disable-next-line import/prefer-default-export
  ImageButton,
};
