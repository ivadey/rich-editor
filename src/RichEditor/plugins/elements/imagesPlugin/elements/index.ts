import React from 'react';
import { Content, Paragraph } from 'mdast';
import { BlockElementDescription, InlineElementDescription } from '../../../types';
import Image, { ImageRenderElementProps } from './Image';
import { IToolbarProps } from '../../../../components/Toolbar/types';
import { strings } from '../../../../helpers';
import { BlockFormatEnum } from '../../../../baseTypes';
import { ImageElement, ImagePosition } from '../types';
import dialog from '../../../../../dialog';

const floatingMenu = [
  [
    {
      icon: 'image-align-left',
      tooltip: strings.alignLeft,
      active: (editor) => editor.getImagePosition() === ImagePosition.AlignLeft,
      onClick: (editor) => editor.setImagePosition(ImagePosition.AlignLeft),
    },
    {
      icon: 'image-align-center',
      tooltip: strings.alignCenter,
      active: (editor) => editor.getImagePosition() === ImagePosition.AlignCenter,
      onClick: (editor) => editor.setImagePosition(ImagePosition.AlignCenter),
    },
    {
      icon: 'image-align-right',
      tooltip: strings.alignRight,
      active: (editor) => editor.getImagePosition() === ImagePosition.AlignRight,
      onClick: (editor) => editor.setImagePosition(ImagePosition.AlignRight),
    },
  ],
  [
    {
      icon: 'image-float-left',
      tooltip: strings.wrapLeft,
      active: (editor) => editor.getImagePosition() === ImagePosition.FloatLeft,
      onClick: (editor) => (editor.setImagePosition(ImagePosition.FloatLeft)),
    },
    {
      icon: 'image-float-right',
      tooltip: strings.wrapRight,
      active: (editor) => editor.getImagePosition() === ImagePosition.FloatRight,
      onClick: (editor) => (editor.setImagePosition(ImagePosition.FloatRight)),
    },
  ],
  [
    {
      icon: React.createElement('span', {}, strings.changeCaption),
      onClick: (editor) => dialog.prompt({ initialValue: editor.getCurrentImageCaption() })
        .then((caption) => editor.changeImageCaption(caption))
        .catch(() => null),
    },
  ],
  [
    {
      icon: 'edit',
      tooltip: strings.edit,
      onClick: (editor) => dialog.prompt({ initialValue: editor.getImageUrl() })
        .then((imageUrl) => editor.changeImageUrl(imageUrl))
        .catch(() => null),
    },
    {
      icon: 'trash',
      tooltip: strings.remove,
      onClick: (editor) => editor.removeImage(),
    },
  ],
] as IToolbarProps['buttons'];

export default [{
  type: 'block',
  formatName: BlockFormatEnum.Image,
  floatingMenu,
  enableAboveExitBreak: true,
  render: (props) => React.createElement(Image, props as ImageRenderElementProps),
  convertors: {
    fromRemark: {
      mdastNodeType: ['image', 'containerDirective'],
      check: (source) => source.type === 'image'
        || (source.type === 'containerDirective' && source.name === 'imageWithMetaInfo'),
      convert: (source) => {
        let imageUrl = '';
        let caption = '';
        let position: ImagePosition = ImagePosition.AlignCenter;
        let widthInPercent: number | undefined;

        if (source.type === 'image') {
          imageUrl = source.url;
          caption = source.alt ?? '';
          position = ImagePosition.AlignCenter;
        } else if (source.type === 'containerDirective') {
          const [paragraph] = source.children;
          const [image] = (paragraph as Paragraph).children;

          if (image.type === 'image') {
            imageUrl = image.url;
            caption = source.attributes?.caption ?? image.alt ?? '';
            position = (source.attributes?.position as ImagePosition) ?? ImagePosition.AlignCenter;
            widthInPercent = source.attributes?.widthInPercent
              ? Number(source.attributes.widthInPercent)
              : undefined;
          }
        }

        return ({
          type: BlockFormatEnum.Image,
          imageUrl,
          position,
          caption,
          widthInPercent,
          children: [{ text: '' }],
        });
      },
    },
    toRemark: (element: ImageElement) => [{
      type: 'containerDirective',
      name: 'imageWithMetaInfo',
      attributes: {
        position: element.position || ImagePosition.AlignCenter,
        caption: element.caption,
        widthInPercent: String(element.widthInPercent),
      },
      children: [{
        type: 'image',
        url: element.imageUrl,
        alt: element.caption ?? '',
      }] as Content[],
    }] as Content[],
  },
}] as Array<BlockElementDescription | InlineElementDescription>;
