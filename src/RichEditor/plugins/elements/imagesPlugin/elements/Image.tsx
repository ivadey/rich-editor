/* eslint-disable react/jsx-props-no-spreading */
import React, { useRef, useState } from 'react';
import { RenderElementProps, useReadOnly, useSelected } from 'slate-react';
import { ImageElement, ImagePosition } from '../types';
import useImageResize from './useImageResize';
import { Icon } from '../../../../components';

export interface ImageRenderElementProps extends RenderElementProps {
  element: ImageElement;
}

const Image: React.FC<ImageRenderElementProps> = ({ element, attributes, children }) => {
  const selected = useSelected();
  const readonly = useReadOnly();
  const className = ['re-image'];

  const leftResizeHandlerRef = useRef<HTMLDivElement>(null);
  const rightResizeHandlerRef = useRef<HTMLDivElement>(null);

  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);

  useImageResize({
    element,
    imageContainerRef: attributes.ref,
    leftResizeBarRef: leftResizeHandlerRef,
    rightResizeBarRef: rightResizeHandlerRef,
  });

  if (element.position === ImagePosition.FloatLeft) className.push('re-image__float-left');
  else if (element.position === ImagePosition.FloatRight) className.push('re-image__float-right');
  else if (element.position === ImagePosition.AlignLeft) className.push('re-image__align-left');
  else if (element.position === ImagePosition.AlignRight) className.push('re-image__align-right');
  else className.push('re-image__align-center');

  const figureStyle = element.widthInPercent ? { width: `${element.widthInPercent}%` } : {};

  return (
    <figure
      className={className.join(' ')}
      style={figureStyle}
      {...attributes}
    >
      {children}

      {loading && (
        <div className="loading-placeholder" contentEditable={false}>
          <Icon icon="image" />
        </div>
      )}

      {error && (
        <div className="loading-error" contentEditable={false}>
          <Icon icon="image-slash" />
        </div>
      )}

      <img
        className={selected ? 'selected' : ''}
        src={element.imageUrl}
        onLoad={() => setLoading(false)}
        onError={() => {
          setLoading(false);
          setError(true);
        }}
        alt={element.caption ?? ''}
      />
      {element.caption && (
        <figcaption contentEditable={false}>{element.caption}</figcaption>
      )}

      {selected && !readonly && (
        <div className="re-image__resize-handler" contentEditable={false}>
          <div ref={leftResizeHandlerRef} className="re-image__resize-handler__left" />
          <div ref={rightResizeHandlerRef} className="re-image__resize-handler__right" />
        </div>
      )}
    </figure>
  );
};

Image.displayName = 'Image';

export default Image;
