import React, { useEffect, useRef } from 'react';
import { useSelected, useSlate } from 'slate-react';
import { ImageElement, ImagePosition } from '../types';

const MIN_ALLOWED_WIDTH_RATIO = 0.2;

interface UseImageResizeOptions {
  element: ImageElement;
  imageContainerRef: React.RefObject<HTMLDivElement>;
  leftResizeBarRef: React.RefObject<HTMLDivElement>;
  rightResizeBarRef: React.RefObject<HTMLDivElement>;
}

export default ({
  element, imageContainerRef, leftResizeBarRef, rightResizeBarRef,
}: UseImageResizeOptions) => {
  const selected = useSelected();
  const editor = useSlate();

  const data = useRef({
    maxAllowedWidth: 0,
    minAllowedWidth: 0,
    ratio: 1,

    isHandlingResize: false,
    initialOffset: 0,
    initialWidth: 0,

    isInverted: false, // We need to invert logic with adding shift if resize was started from left bar
  });
  const handlers = useRef({
    onResizeStart: (event: MouseEvent) => {
      const { current: imageContainer } = imageContainerRef;

      if (!imageContainer) return;

      data.current.isHandlingResize = true;
      data.current.isInverted = (event.target as HTMLDivElement).classList.contains('re-image__resize-handler__left');

      data.current.initialOffset = event.screenX;
      data.current.initialWidth = imageContainer.getBoundingClientRect().width;

      // eslint-disable-next-line no-param-reassign
      imageContainer.style.width = `${data.current.initialWidth}px`;
    },
    onResize: (event: MouseEvent) => {
      const { current: imageContainer } = imageContainerRef;
      const {
        isHandlingResize, initialWidth, initialOffset, minAllowedWidth,
        maxAllowedWidth, ratio, isInverted,
      } = data.current;

      if (!isHandlingResize || !imageContainer) return;

      const shift = (event.screenX - initialOffset) * ratio;
      const newWidth = isInverted ? initialWidth - shift : initialWidth + shift;

      if (!shift || newWidth > maxAllowedWidth || newWidth < minAllowedWidth) return;

      // eslint-disable-next-line no-param-reassign
      imageContainer.style.width = `${newWidth}px`;
    },
    onResizeEnd: () => {
      data.current.isHandlingResize = false;

      const { maxAllowedWidth } = data.current;
      const { current: imageContainer } = imageContainerRef;

      if (imageContainer) {
        const currentWidth = imageContainer.getBoundingClientRect().width;

        const newWidthInPercent = Math.round((currentWidth / maxAllowedWidth) * 100);
        editor.setCurrentImageWidthInPercent(newWidthInPercent);
      }
    },
  });

  useEffect(() => {
    const { onResizeStart, onResize, onResizeEnd } = handlers.current;
    const { current: imageContainer } = imageContainerRef;
    const { current: leftResizeBar } = leftResizeBarRef;
    const { current: rightResizeBar } = rightResizeBarRef;

    if (selected && imageContainer && imageContainer.parentElement) {
      const maxAllowedWidth = imageContainer.parentElement.getBoundingClientRect().width;
      const minAllowedWidth = maxAllowedWidth * MIN_ALLOWED_WIDTH_RATIO;

      data.current.maxAllowedWidth = maxAllowedWidth;
      data.current.minAllowedWidth = Math.round(minAllowedWidth);

      leftResizeBar?.addEventListener('mousedown', onResizeStart);
      document.addEventListener('mousemove', onResize);
      document.addEventListener('mouseup', onResizeEnd);

      rightResizeBar?.addEventListener('mousedown', onResizeStart);
      document.addEventListener('mousemove', onResize);
      document.addEventListener('mouseup', onResizeEnd);
    }

    return () => {
      leftResizeBar?.removeEventListener('mousedown', onResizeStart);
      document.removeEventListener('mousemove', onResize);
      document.removeEventListener('mouseup', onResizeEnd);

      rightResizeBar?.removeEventListener('mousedown', onResizeStart);
      document.removeEventListener('mousemove', onResize);
      document.removeEventListener('mouseup', onResizeEnd);
    };
  }, [imageContainerRef, leftResizeBarRef, rightResizeBarRef, selected]);

  useEffect(() => {
    if (element.position === ImagePosition.AlignCenter) data.current.ratio = 2;
    else data.current.ratio = 1;
  }, [element.position]);
};
