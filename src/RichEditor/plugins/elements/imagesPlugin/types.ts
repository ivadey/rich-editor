import { BaseElement } from 'slate';
import { BlockFormatEnum } from '../../../baseTypes';

export interface ImageEditor {
  insertImage: (imageUrl: string) => void;
  removeImage: () => void;

  getImageUrl: () => string | undefined;
  changeImageUrl: (imageUrl: string) => void;

  getImagePosition: () => ImagePosition | undefined;
  setImagePosition: (align: ImagePosition) => void;

  getCurrentImageCaption: () => string | undefined;
  changeImageCaption: (caption?: string) => void;

  getCurrentImageWidthInPercent: () => number | undefined;
  setCurrentImageWidthInPercent: (widthInPercent: number) => void;
}

export enum ImagePosition {
  AlignLeft = 'align-left',
  AlignCenter = 'align-center',
  AlignRight = 'align-right',
  FloatLeft = 'float-left',
  FloatRight = 'float-right',
}

export interface ImageElement extends BaseElement {
  type: BlockFormatEnum.Image;
  imageUrl: string;
  position: ImagePosition;
  caption?: string;
  widthInPercent?: number;
}
