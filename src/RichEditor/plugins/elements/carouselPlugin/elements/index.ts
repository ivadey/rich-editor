import React from 'react';
import { ContainerDirective } from 'mdast-util-directive';
import {
  Content, Image, Paragraph, PhrasingContent,
} from 'mdast';
import { BlockElementDescription, InlineElementDescription } from '../../../types';
import Carousel, { CarouselRenderElementProps } from './Carousel';
import { IToolbarProps } from '../../../../components/Toolbar/types';
import { strings } from '../../../../helpers';
import { BlockFormatEnum } from '../../../../baseTypes';
import { CarouselElement } from '../types';
import dialog from '../../../../../dialog';

const floatingMenu = [
  [
    {
      icon: 'plus',
      tooltip: strings.addImage,
      onClick: (editor) => {
        dialog.prompt({ message: strings.enterImageLink }).then((imageUrl) => {
          if (imageUrl) editor.addImageInCarousel(imageUrl);
        }).catch(() => null);
      },
    },
    {
      icon: 'xmark',
      tooltip: strings.removeImage,
      onClick: (editor) => editor.removeImageFromCarousel(),
    },
  ],
  [
    {
      icon: 'trash',
      tooltip: strings.remove,
      onClick: (editor) => editor.removeCarousel(),
    },
  ],
] as IToolbarProps['buttons'];

export default [{
  type: 'block',
  formatName: BlockFormatEnum.Carousel,
  floatingMenu,
  render: (props) => React.createElement(Carousel, props as CarouselRenderElementProps),
  convertors: {
    fromRemark: {
      mdastNodeType: 'containerDirective',
      check: (source) => (source as ContainerDirective).name === 'carousel',
      convert: (source) => {
        const images: string[] = [];
        (source as ContainerDirective).children.forEach((child) => {
          (child as Paragraph).children.forEach((carouselItem) => {
            if (carouselItem.type === 'image') images.push((carouselItem as Image).url);
          });
        });

        return {
          type: BlockFormatEnum.Carousel,
          images,
          children: [{ text: '' }],
        };
      },
    },
    toRemark: (element: CarouselElement) => {
      const carouselImages: PhrasingContent[] = [];

      element.images.forEach((image, ind) => {
        carouselImages.push({
          type: 'image',
          url: image,
        });
        if (ind < element.images.length - 1) {
          carouselImages.push({
            type: 'text',
            value: '\n',
          });
        }
      });

      return [{
        type: 'containerDirective',
        name: 'carousel',
        children: [{
          type: 'paragraph',
          children: carouselImages,
        }],
      }] as Content[];
    },
  },
}] as Array<BlockElementDescription | InlineElementDescription>;
