/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { RenderElementProps, useSelected } from 'slate-react';
import { Navigation, Pagination } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import { CarouselElement } from '../types';
import { classNames } from '../../../../helpers';
import carouselStore from '../carouselStore';

export type CarouselRenderElementProps = RenderElementProps & {
  element: CarouselElement;
}

const Carousel: React.FC<CarouselRenderElementProps> = ({ element, attributes, children }) => {
  const selected = useSelected();

  return (
    <div className="re-carousel" {...attributes}>
      {children}

      <Swiper
        className={classNames('re-carousel__swiper', selected ? 'selected' : '')}
        modules={[Navigation, Pagination]}
        navigation
        pagination={{ clickable: true }}
        scrollbar={{ draggable: true }}
        onSwiper={(swiper) => carouselStore.setActiveImageIndex(swiper.activeIndex)}
        onSlideChange={(swiper) => carouselStore.setActiveImageIndex(swiper.activeIndex)}
      >
        {element.images.map((image) => (
          <SwiperSlide
            key={image}
            className="re-carousel__swiper_slide-item"
            contentEditable={false}
          >
            <img src={image} alt="" />
          </SwiperSlide>
        ))}
      </Swiper>
    </div>
  );
};

Carousel.displayName = 'Carousel';

export default Carousel;
