import { makeAutoObservable } from 'mobx';

class CarouselStore {
  activeImageIndex?: number;

  constructor() {
    makeAutoObservable(this);
  }

  setActiveImageIndex(activeIndex: number) {
    this.activeImageIndex = activeIndex;
  }
}

export default new CarouselStore();
