import { strings } from '../../../helpers';
import { BlockFormatEnum } from '../../../baseTypes';
import { IToolbarButtonProps } from '../../../components/Toolbar/types';

const CarouselButton: IToolbarButtonProps = {
  icon: 'rectangle-vertical-history',
  tooltip: strings.carousel,
  active: (editor) => editor.isElementSelected(BlockFormatEnum.Carousel),
  onClick: (editor) => editor.insertCarousel(),
};

export {
  // eslint-disable-next-line import/prefer-default-export
  CarouselButton,
};
