import React from 'react';
import { ElementsPlugin } from '../../types';
import editorWrapper from './editorWrapper';
import elements from './elements';
import { CarouselButton } from './toolbarButtons';
import { IToolbarButtonProps } from '../../../components/Toolbar/types';

interface CarouselPlugin extends ElementsPlugin {
  toolbarButtons: {
    carousel: IToolbarButtonProps | React.ReactNode;
  }
}

const carouselPlugin: CarouselPlugin = {
  name: 'Carousel plugin',
  editorWrapper,
  toolbarButtons: {
    carousel: CarouselButton,
  },
  elements,
};

export default carouselPlugin;
