/* eslint-disable no-param-reassign */
import { Editor, Element as SlateElement, Transforms } from 'slate';
import carouselStore from './carouselStore';
import { BlockFormatEnum } from '../../../baseTypes';
import { CarouselElement } from './types';

const editorWrapper = (editor: Editor) => {
  const { isVoid, insertBreak } = editor;

  editor.isVoid = (element) => (element.type === BlockFormatEnum.Carousel ? true : isVoid(element));
  editor.insertBreak = () => {
    const { selection } = editor;

    if (selection) {
      const [carousel] = Editor.nodes(editor, {
        match: (n) => !Editor.isEditor(n) && SlateElement.isElement(n) && n.type === BlockFormatEnum.Carousel,
      });

      if (carousel) {
        editor.insertNode({
          type: BlockFormatEnum.Paragraph,
          children: [{ text: '' }],
        });
        return;
      }
    }

    insertBreak();
  };

  const getSelectedCarousel = () => {
    const [selectedCarousel] = Editor.nodes<CarouselElement>(editor, {
      match: (node) => !Editor.isEditor(node)
        && SlateElement.isElement(node)
        && node.type === BlockFormatEnum.Carousel,
    });

    return selectedCarousel;
  };

  editor.insertCarousel = () => {
    Transforms.insertNodes(editor, {
      type: BlockFormatEnum.Carousel,
      images: [],
      children: [{ text: '' }],
    });
  };
  editor.removeCarousel = () => {
    const [carousel, path] = getSelectedCarousel() || [];

    if (carousel) Transforms.removeNodes(editor, { at: path });
  };

  editor.addImageInCarousel = (url) => {
    const [carousel, path] = getSelectedCarousel() || [];

    if (!carousel) return;

    Transforms.setNodes(editor, {
      images: [...carousel.images, url],
    }, { at: path });
  };
  editor.removeImageFromCarousel = (index) => {
    const imageToRemoveInd = index ?? carouselStore.activeImageIndex;
    if (typeof imageToRemoveInd !== 'number' || imageToRemoveInd < 0) return;

    const [carousel, path] = getSelectedCarousel() || [];

    if (!carousel || imageToRemoveInd > carousel.images.length - 1) return;

    Transforms.setNodes(editor, {
      images: [
        ...carousel.images.slice(0, imageToRemoveInd),
        ...carousel.images.slice(imageToRemoveInd + 1, carousel.images.length),
      ],
    }, { at: path });
  };

  return editor;
};

export default editorWrapper;
