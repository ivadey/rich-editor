import { BaseElement } from 'slate';
import { BlockFormatEnum } from '../../../baseTypes';

export interface CarouselEditor {
  insertCarousel: () => void;
  removeCarousel: () => void;
  addImageInCarousel: (url: string) => void;
  removeImageFromCarousel: (index?: number) => void;
}

export interface CarouselElement extends BaseElement {
  type: BlockFormatEnum.Carousel;
  images: string[];
}
