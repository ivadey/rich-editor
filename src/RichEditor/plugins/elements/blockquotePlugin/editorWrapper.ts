/* eslint-disable no-param-reassign */
import { Editor } from 'slate';
import { BlockFormatEnum } from '../../../baseTypes';

const editorWrapper = (editor: Editor) => {
  editor.insertBlockquote = () => editor.toggleFormat(BlockFormatEnum.Blockquote, { wrapNonEmpty: true });

  editor.removeBlockquote = () => editor.toggleFormat(BlockFormatEnum.Blockquote);

  return editor;
};

export default editorWrapper;
