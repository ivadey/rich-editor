import React from 'react';
import { Blockquote } from 'mdast';
import { Editor, Element as SlateElement, Range as SlateRange } from 'slate';
import { ElementsPlugin } from '../../types';
import { BlockquoteButton } from './toolbarButtons';
import { BlockFormatEnum } from '../../../baseTypes';
import editorWrapper from './editorWrapper';
import { IToolbarButtonProps } from '../../../components/Toolbar/types';

interface BlockquotePlugin extends ElementsPlugin {
  toolbarButtons: {
    blockquote: IToolbarButtonProps | React.ReactNode;
  }
}

const blockquotePlugin: BlockquotePlugin = {
  name: 'Blockquote plugin',
  editorWrapper,
  toolbarButtons: { blockquote: BlockquoteButton },
  elements: [{
    type: 'block',
    formatName: BlockFormatEnum.Blockquote,
    clearFormatOnEnter: true,
    render: (props) => {
      const newRenderProps = {
        ...props,
        className: 're-blockquote',
      };

      return React.createElement('blockquote', newRenderProps);
    },
    convertors: {
      fromRemark: {
        mdastNodeType: 'blockquote',
        convert: (
          source,
          next,
          decoration,
        ) => [{
          type: BlockFormatEnum.Blockquote,
          children: next((source as Blockquote).children, decoration),
        }],
      },
      toRemark: (element, next) => [{
        type: 'blockquote',
        children: next((element as SlateElement).children) as Blockquote['children'],
      }],
    },
  }],
  // todo: add 'shortcut' property to add block instead of custom onKeyDown handler and check it in onChange handler
  onKeyDownHandler: (event, editor) => {
    if (editor.selection && SlateRange.isCollapsed(editor.selection)) {
      const [currentNodeEntry] = Editor.nodes<SlateElement>(editor, {
        match: (node) => !Editor.isEditor(node) && SlateElement.isElement(node),
        at: editor.selection.focus.path,
      });

      if (currentNodeEntry && currentNodeEntry[0].type === BlockFormatEnum.Paragraph) {
        const elementText = Editor.string(editor, currentNodeEntry[1]);
        if (elementText === '>' && event.code === 'Space') {
          event.preventDefault();

          editor.insertBlockquote();
          Editor.deleteBackward(editor, { unit: 'line' });

          return true;
        }
      }
    }

    return false;
  },
  hotkeys: {
    'mod+alt+9': (editor) => (!editor.isElementSelected(BlockFormatEnum.Blockquote)
      ? editor.insertBlockquote()
      : editor.removeBlockquote()),
  },
};

export default blockquotePlugin;
