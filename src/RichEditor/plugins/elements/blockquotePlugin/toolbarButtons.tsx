import { BlockFormatEnum } from '../../../baseTypes';
import { strings } from '../../../helpers';
import { IToolbarButtonProps } from '../../../components/Toolbar/types';

const BlockquoteButton: IToolbarButtonProps = {
  icon: 'block-quote',
  active: (editor) => editor.isElementSelected(BlockFormatEnum.Blockquote),
  tooltip: strings.blockquote,
  onClick: (editor) => (!editor.isElementSelected(BlockFormatEnum.Blockquote)
    ? editor.insertBlockquote()
    : editor.removeBlockquote()),
  hotkeyHint: 'mod+alt+9',
};

export {
  // eslint-disable-next-line import/prefer-default-export
  BlockquoteButton,
};
