import { BaseElement } from 'slate';
import { BlockFormatEnum } from '../../../baseTypes';

export interface BlockquoteEditor {
  insertBlockquote: () => void;
  removeBlockquote: () => void;
}

export interface BlockquoteElement extends BaseElement {
  type: BlockFormatEnum.Blockquote;
}
