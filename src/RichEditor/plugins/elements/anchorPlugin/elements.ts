import React from 'react';
import { TextDirective } from 'mdast-util-directive';
import { Content } from 'mdast';
import { BlockFormatEnum } from '../../../baseTypes';
import { BlockElementDescription } from '../../types';
import Anchor from './Anchor';
import { AnchorElement } from './types';
import { strings } from '../../../helpers';
import setOrChangeAnchor from './setOrChangeAnchor';

export default [{
  type: 'block',
  formatName: BlockFormatEnum.AnchorSlug,
  render: (props) => React.createElement(Anchor, props),
  floatingMenu: [
    [
      {
        icon: 'edit',
        tooltip: strings.edit,
        onClick: setOrChangeAnchor,
      },
    ],
    [
      {
        icon: 'trash',
        tooltip: strings.remove,
        onClick: (editor) => editor.removeAnchor(),
      },
    ],
  ],
  convertors: {
    fromRemark: {
      mdastNodeType: 'textDirective',
      check: (source) => (source as TextDirective).name === 'anchor',
      convert: (source, next, decoration) => ({
        type: BlockFormatEnum.AnchorSlug,
        slug: (source as TextDirective).attributes?.slug ?? '',
        children: next((source as TextDirective).children, decoration),
      }),
    },
    toRemark: (element: AnchorElement, next) => [{
      type: 'textDirective',
      attributes: { slug: element.slug },
      name: 'anchor',
      children: next(element.children),
    }] as Content[],
  },
}] as BlockElementDescription[];
