import { strings } from '../../../helpers';
import { IToolbarButtonProps } from '../../../components/Toolbar/types';
import setOrChangeAnchor from './setOrChangeAnchor';
import { BlockFormatEnum } from '../../../baseTypes';

const AnchorButton: IToolbarButtonProps = {
  icon: 'anchor',
  active: (editor) => editor.isElementSelected(BlockFormatEnum.AnchorSlug),
  onClick: (editor) => (editor.isElementSelected(BlockFormatEnum.AnchorSlug)
    ? editor.removeAnchor()
    : setOrChangeAnchor(editor)),
  tooltip: strings.anchor,
  disabled: (editor) => !editor.getSelectedText(),
};

export {
  // eslint-disable-next-line import/prefer-default-export
  AnchorButton,
};
