import { BaseElement } from 'slate';
import { BlockFormatEnum } from '../../../baseTypes';

export interface AnchorElement extends BaseElement {
  type: BlockFormatEnum.AnchorSlug,
  slug: string;
}

export interface AnchorEditor {
  setAnchor: (slug: string) => void;
  changeAnchor: (slug: string) => void;
  removeAnchor: () => void;
  getCurrentAnchorSlug: () => string | undefined;
}
