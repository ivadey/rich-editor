import React from 'react';
import { ElementsPlugin } from '../../types';
import { AnchorButton } from './toolbarButtons';
import elements from './elements';
import { IToolbarButtonProps } from '../../../components/Toolbar/types';
import editorWrapper from './editorWrapper';

interface FormattingPlugin extends ElementsPlugin {
  toolbarButtons: {
    anchor: IToolbarButtonProps | React.ReactNode;
  }
}

const formattingPlugin: FormattingPlugin = {
  name: 'Anchor plugin',
  editorWrapper,
  toolbarButtons: {
    anchor: AnchorButton,
  },
  elements,
};

export default formattingPlugin;
