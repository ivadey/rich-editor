/* eslint-disable no-param-reassign */
import {
  Editor, Element as SlateElement, Range as SlateRange, Transforms,
} from 'slate';
import { BlockFormatEnum } from '../../../baseTypes';
import { AnchorElement } from './types';

const getSelectedAnchor = (editor: Editor) => {
  const [selectedTooltip] = Editor.nodes<AnchorElement>(editor, {
    match: (node) => !Editor.isEditor(node)
      && SlateElement.isElement(node)
      && node.type === BlockFormatEnum.AnchorSlug,
  });

  return selectedTooltip;
};

const unwrapAnchor = (editor: Editor) => {
  Transforms.unwrapNodes(editor, {
    match: (n) => !Editor.isEditor(n) && SlateElement.isElement(n) && n.type === BlockFormatEnum.AnchorSlug,
  });
};

const editorWrapper = (editor: Editor) => {
  const { isInline } = editor;
  editor.isInline = (element) => element.type === BlockFormatEnum.AnchorSlug || isInline(element);

  editor.setAnchor = (slug) => {
    if (!editor.selection || SlateRange.isCollapsed(editor.selection)) return;

    if (!slug) return;

    if (getSelectedAnchor(editor)) unwrapAnchor(editor);

    const tooltipElement: AnchorElement = {
      type: BlockFormatEnum.AnchorSlug,
      slug,
      children: [],
    };

    Transforms.wrapNodes(editor, tooltipElement, { split: true });
    Transforms.collapse(editor, { edge: 'end' });
  };
  editor.changeAnchor = (slug) => {
    const [selectedAnchor, path] = getSelectedAnchor(editor) || [];

    if (selectedAnchor && slug) {
      Transforms.setNodes(editor, { slug }, { at: path });
    }
  };
  editor.removeAnchor = () => {
    const [selectedAnchor] = getSelectedAnchor(editor) || [];

    if (selectedAnchor) unwrapAnchor(editor);
  };
  editor.getCurrentAnchorSlug = () => {
    const [selectedAnchor] = getSelectedAnchor(editor) || [];
    return selectedAnchor?.slug;
  };

  return editor;
};

export default editorWrapper;
