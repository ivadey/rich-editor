import React from 'react';
import { RenderElementProps } from 'slate-react';
import { Icon } from '../../../components';
import { AnchorElement } from './types';

const Anchor: React.FC<RenderElementProps> = ({ element, children, attributes }) => (
  // eslint-disable-next-line react/jsx-props-no-spreading
  <span id={(element as AnchorElement).slug} className="re-anchor" {...attributes}>
    <Icon icon="anchor" />
    <span contentEditable={false}>‎</span>
    {children}
  </span>
);

Anchor.displayName = 'Anchor';

export default Anchor;
