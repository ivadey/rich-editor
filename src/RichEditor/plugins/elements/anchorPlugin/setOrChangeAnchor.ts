import { Editor } from 'slate';
import dialog from '../../../../dialog';
import { getTextSlug, strings } from '../../../helpers';

export default (editor: Editor) => {
  const selectedText = editor.getSelectedText();
  if (!selectedText) return;

  const currentAnchor = editor.getCurrentAnchorSlug();
  const initialValue = currentAnchor || getTextSlug(selectedText);

  dialog.prompt({ message: strings.specifyAnchorValue, initialValue })
    .then((anchor) => {
      if (document.getElementById(anchor)) {
        dialog.alert({ message: strings.suchAnchorAlreadyExist }).catch(() => null);
        return;
      }

      if (/^-.+/.test(anchor) || /.+-$/.test(anchor)) {
        dialog.alert({ message: strings.anchorSlugCanStartOrEndOnlyWithSymbolOrDigit }).catch(() => null);
        return;
      }

      if (!/[a-z0-9-]/.test(anchor.toLowerCase())) {
        dialog.alert({ message: strings.anchorSlugCanContainOnlySymbolsAndDigits }).catch(() => null);
        return;
      }

      if (currentAnchor) editor.changeAnchor(anchor.toLowerCase());
      else editor.setAnchor(anchor.toLowerCase());
    })
    .catch(() => null);
};
