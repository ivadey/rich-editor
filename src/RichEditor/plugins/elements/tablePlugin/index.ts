import React from 'react';
import { Descendant, Element as SlateElement } from 'slate';
import { Table, TableCell, TableRow } from 'mdast';
import { ElementsPlugin } from '../../types';
import editorWrapper from './editorWrapper';
import { TableButton } from './toolbarButtons';
import TableElement from './TableElement';
import { TableCellElement } from './types';
import { BlockFormatEnum } from '../../../baseTypes';
import { IToolbarButtonProps } from '../../../components/Toolbar/types';
import normalizeNodes from './normalizeNodes';
import { listToHtml } from './convertHelpers';
import { ListElement } from '../listsPlugin/types';

interface TablePlugin extends ElementsPlugin {
  toolbarButtons: {
    table: IToolbarButtonProps | React.ReactNode;
  }
}

const tablePlugin: TablePlugin = {
  name: 'Table plugin',
  editorWrapper,
  toolbarButtons: {
    table: TableButton,
  },
  elements: [
    {
      type: 'block',
      formatName: BlockFormatEnum.Table,
      render: (props) => React.createElement(TableElement, props),
      enableAboveExitBreak: true,
      enableBelowExitBreak: true,
      convertors: {
        fromRemark: {
          mdastNodeType: 'table',
          convert: (source, next, decoration) => {
            const rowsCount = (source as Table).children.length;
            const colsCount = (source as Table).children[0].children.length;

            const modifiedDecoration = {
              ...decoration,
              block: {
                ...decoration.block,
                columnWidth: 100 / colsCount,
              },
            };

            return {
              type: BlockFormatEnum.Table,
              rowsCount,
              colsCount,
              children: next((source as Table).children, modifiedDecoration),
            };
          },
        },
        toRemark: (element, next) => [{
          type: 'table',
          children: next((element as SlateElement).children) as Table['children'],
        }],
      },
    },
    {
      type: 'block',
      formatName: BlockFormatEnum.TableRow,
      render: (props) => React.createElement(TableElement, props),
      convertors: {
        fromRemark: {
          mdastNodeType: 'tableRow',
          convert: (source, next, decoration) => ({
            type: BlockFormatEnum.TableRow,
            children: next((source as TableRow).children, decoration),
          }),
        },
        toRemark: (element, next) => [{
          type: 'tableRow',
          children: next((element as SlateElement).children) as TableRow['children'],
        }],
      },
    },
    {
      type: 'block',
      formatName: BlockFormatEnum.TableCell,
      render: (props) => React.createElement(TableElement, props),
      convertors: {
        fromRemark: {
          mdastNodeType: 'tableCell',
          convert: (source, next, decoration) => ({
            type: BlockFormatEnum.TableCell,
            columnWidth: decoration.block && 'columnWidth' in decoration.block
              ? (decoration.block as TableCellElement).columnWidth
              : 0,
            children: next((source as Table).children, decoration),
          }),
        },
        toRemark: (element, next) => [{
          type: 'tableCell',
          children: ((element as SlateElement).children
            .map((child: Descendant) => {
              if (!SlateElement.isElement(child)) return null;

              if (child.type === BlockFormatEnum.TableCellParagraph) {
                return next(child.children);
              }

              return { type: 'html', value: listToHtml(child as ListElement) };
            })
            .filter(Boolean)
            .flat()) as TableCell['children'],
        }],
      },
    },
    {
      type: 'block',
      formatName: BlockFormatEnum.TableCellParagraph,
      render: (props) => React.createElement(TableElement, props),
      convertors: {
        fromRemark: null,
        toRemark: null,
      },
    },
  ],
  hotkeys: {
    'alt+shift+t': (editor) => (editor.isElementSelected(BlockFormatEnum.Table)
      ? editor.removeTable()
      : editor.insertTable(2, 2)),
  },
  onKeyDownHandler: (event, editor) => {
    if (!editor.isElementSelected(BlockFormatEnum.Table)) return false;
    if (['ArrowUp', 'ArrowRight', 'ArrowDown', 'ArrowLeft'].indexOf(event.code) === -1) return false;

    let rowInd = editor.selection?.anchor?.path?.[1];
    let colInd = editor.selection?.anchor?.path?.[2];
    const selectedTable = editor.getSelectedTable();

    if (rowInd === undefined || colInd === undefined || !selectedTable) return false;

    event.preventDefault();

    if (event.code === 'ArrowUp') rowInd = Math.max(rowInd - 1, 0);
    if (event.code === 'ArrowRight') colInd = Math.min(colInd + 1, selectedTable[0].colsCount - 1);
    if (event.code === 'ArrowDown') rowInd = Math.min(rowInd + 1, selectedTable[0].rowsCount - 1);
    if (event.code === 'ArrowLeft') colInd = Math.max(colInd - 1, 0);

    editor.selectCell(rowInd, colInd);

    return true;
  },
  normalizeNodes,
};

export default tablePlugin;
