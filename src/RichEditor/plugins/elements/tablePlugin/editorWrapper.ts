/* eslint-disable no-param-reassign */
import {
  Editor,
  Element as SlateElement,
  Transforms,
} from 'slate';
import { BlockFormatEnum } from '../../../baseTypes';
import { TableCellElement, TableElement } from './types';

const editorWrapper = (editor: Editor): Editor => {
  editor.insertTable = (rowsCount, colsCount) => {
    const table: TableElement = {
      type: BlockFormatEnum.Table,
      rowsCount: rowsCount ?? 2,
      colsCount: colsCount ?? 2,
      children: Array.from(Array(rowsCount)).map(() => ({
        type: BlockFormatEnum.TableRow,
        children: Array.from(Array(colsCount)).map(() => ({
          type: BlockFormatEnum.TableCell,
          columnWidth: 100 / colsCount,
          children: [{
            type: BlockFormatEnum.TableCellParagraph,
            children: [{ text: '' }],
          }],
        })),
      })),
    };

    if (editor.selection && editor.isBlockEmpty(editor.selection.anchor.path)) {
      Transforms.setNodes(editor, table);
      Transforms.insertNodes(editor, table.children, { at: editor.selection.focus.path });
    } else Transforms.insertNodes(editor, table);

    const selectedTable = editor.getSelectedTable();

    if (selectedTable) editor.selectCell(0, 0);
  };
  editor.removeTable = () => {
    const selectedTable = editor.getSelectedTable();

    if (selectedTable) Transforms.removeNodes(editor, { at: selectedTable[1] });
  };

  editor.addRow = (atPosition?: number) => {
    const selectedTable = editor.getSelectedTable();

    if (!selectedTable) return;

    const row: SlateElement = {
      type: BlockFormatEnum.TableRow,
      children: Array.from(Array(selectedTable[0].colsCount)).map(() => ({
        type: BlockFormatEnum.TableCell,
        columnWidth: 100 / selectedTable[0].colsCount,
        children: [{
          type: BlockFormatEnum.TableCellParagraph,
          children: [{ text: '' }],
        }],
      })),
    };

    Transforms.insertNodes(editor, row, {
      at: [selectedTable[1][0], atPosition ?? selectedTable[0].rowsCount],
    });
    Transforms.setNodes<TableElement>(
      editor,
      { rowsCount: selectedTable[0].rowsCount + 1 },
      {
        at: [selectedTable[1][0]],
      },
    );
  };
  editor.addColumn = (atPosition?: number) => {
    const selectedTable = editor.getSelectedTable();

    if (!selectedTable) return;

    const newColsCount = selectedTable[0].colsCount + 1;
    const newColumnWidth = 100 / newColsCount;

    const tableCells = Editor.nodes(editor, {
      at: selectedTable[1], // Path of Editor
      match: (node) => SlateElement.isElement(node) && node.type === BlockFormatEnum.TableCell,
    });

    // eslint-disable-next-line no-restricted-syntax
    for (const nodeEntry of tableCells) {
      Transforms.select(editor, nodeEntry[1]);

      Transforms.setNodes<TableCellElement>(
        editor,
        { columnWidth: newColumnWidth },
        { at: nodeEntry[1] },
      );

      if (nodeEntry[1][2] === (atPosition ?? newColsCount - 2)) {
        const at = [
          nodeEntry[1][0],
          nodeEntry[1][1],
          atPosition ?? newColsCount - 1,
        ];

        Transforms.insertNodes(editor, {
          type: BlockFormatEnum.TableCell,
          columnWidth: newColumnWidth,
          children: [{
            type: BlockFormatEnum.TableCellParagraph,
            children: [{ text: '' }],
          }],
        } as TableCellElement, { at });
      }
    }

    Transforms.setNodes<TableElement>(
      editor,
      { colsCount: newColsCount },
      { at: [selectedTable[1][0]] },
    );
  };

  editor.deleteRow = (rowIndex: number) => {
    const selectedTable = editor.getSelectedTable();

    if (!selectedTable || selectedTable[0].rowsCount === 1) return;
    if (rowIndex === undefined || rowIndex < 0 || rowIndex > selectedTable[0].rowsCount - 1) return;

    Transforms.removeNodes(editor, { at: [selectedTable[1][0], rowIndex] });
    Transforms.setNodes<TableElement>(editor, {
      selectedRow: undefined,
      selectedColumn: undefined,
      selectedForRemove: false,
      rowsCount: selectedTable[0].rowsCount - 1,
    }, { at: selectedTable[1] });
  };
  editor.deleteColumn = (columnIndex: number) => {
    const selectedTable = editor.getSelectedTable();

    if (!selectedTable || selectedTable[0].colsCount === 1) return;
    if (columnIndex === undefined || columnIndex < 0 || columnIndex > selectedTable[0].colsCount - 1) return;

    Array.from(Array(selectedTable[0].rowsCount)).forEach((_, rowInd) => {
      Transforms.removeNodes(editor, {
        at: [selectedTable[1][0], rowInd, columnIndex],
      });
    });

    Transforms.setNodes<TableElement>(editor, {
      selectedRow: undefined,
      selectedColumn: undefined,
      selectedForRemove: false,
      colsCount: selectedTable[0].colsCount - 1,
    }, { at: selectedTable[1] });
  };

  editor.setSelectRow = (rowIndex?: number, selectedForRemove = false) => {
    const [selectedTable] = Editor.nodes<TableElement>(editor, {
      match: (node) => !Editor.isEditor(node)
        && SlateElement.isElement(node)
        && node.type === BlockFormatEnum.Table,
    });

    if (!selectedTable) return;

    Transforms.setNodes<TableElement>(editor, {
      selectedRow: rowIndex,
      selectedColumn: undefined,
      selectedForRemove,
    }, { at: selectedTable[1] });
  };
  editor.setSelectColumn = (columnIndex?: number, selectedForRemove = false) => {
    const [selectedTable] = Editor.nodes<TableElement>(editor, {
      match: (node) => !Editor.isEditor(node)
        && SlateElement.isElement(node)
        && node.type === BlockFormatEnum.Table,
    });

    if (!selectedTable) return;

    Transforms.setNodes<TableElement>(editor, {
      selectedColumn: columnIndex,
      selectedRow: undefined,
      selectedForRemove,
    }, { at: selectedTable[1] });
  };
  editor.selectCell = (rowIndex: number, columnIndex: number) => {
    const selectedTable = editor.getSelectedTable();

    if (!selectedTable) return;

    Transforms.select(editor, [...selectedTable[1], rowIndex, columnIndex]);
  };

  editor.getSelectedTable = () => {
    const [selectedTable] = Editor.nodes<TableElement>(editor, {
      match: (node) => !Editor.isEditor(node)
        && SlateElement.isElement(node)
        && node.type === BlockFormatEnum.Table,
    });

    return selectedTable;
  };

  return editor;
};

export default editorWrapper;
