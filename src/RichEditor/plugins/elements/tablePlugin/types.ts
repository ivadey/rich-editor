import { BaseElement, NodeEntry } from 'slate';
import { BlockFormatEnum } from '../../../baseTypes';

export interface TableEditor {
  insertTable: (rowsCount: number, colsCount: number) => void;
  removeTable: () => void;
  addRow: (atPosition?: number) => void;
  addColumn: (atPosition?: number) => void;
  deleteRow: (rowIndex: number) => void;
  deleteColumn: (columnIndex: number) => void;
  setSelectRow: (rowIndex?: number, isForRemove?: boolean) => void;
  setSelectColumn: (columnIndex?: number, isForRemove?: boolean) => void;
  selectCell: (rowIndex: number, columnIndex: number) => void;
  getSelectedTable: () => NodeEntry<TableElement> | undefined;
}

export interface TableElement extends BaseElement {
  type: BlockFormatEnum.Table;
  rowsCount: number;
  colsCount: number;
  selectedRow?: number;
  selectedColumn?: number;
  selectedForRemove?: boolean;
}

export interface TableRowElement extends BaseElement {
  type: BlockFormatEnum.TableRow;
}

export interface TableCellElement extends BaseElement {
  type: BlockFormatEnum.TableCell;
  columnWidth: number;
}

export interface TableCellParagraphElement extends BaseElement {
  type: BlockFormatEnum.TableCellParagraph;
}
