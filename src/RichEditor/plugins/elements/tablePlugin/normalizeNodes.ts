import {
  Editor, Element as SlateElement, Node as SlateNode, NodeEntry, Text as SlateText, Transforms,
} from 'slate';
import { BlockFormatEnum } from '../../../baseTypes';

export default (editor: Editor, entry: NodeEntry) => {
  const [node, path] = entry;

  // todo: is it ok?
  if (SlateElement.isElementType(node, BlockFormatEnum.Table)) {
    for (const [child, childPath] of SlateNode.children(editor, path)) {
      const isValidElement = SlateElement.isElement(child)
        && (child.type === BlockFormatEnum.TableRow);

      if (!isValidElement) {
        Transforms.unwrapNodes(editor, { at: childPath });

        return true;
      }
    }
  }

  if (SlateElement.isElementType(node, BlockFormatEnum.TableCell)) {
    for (const [child, childPath] of SlateNode.children(editor, path)) {
      const isValidElement = SlateElement.isElement(child)
        && (
          child.type === BlockFormatEnum.TableCellParagraph
          || child.type === BlockFormatEnum.BulletedList
          || child.type === BlockFormatEnum.NumberedList
        );

      if (!isValidElement) {
        Transforms.wrapNodes(editor, {
          type: BlockFormatEnum.TableCellParagraph,
          children: [],
        }, { at: childPath });

        return true;
      }
    }
  }

  if (SlateElement.isElementType(node, BlockFormatEnum.TableCellParagraph)) {
    for (const [child, childPath] of SlateNode.children(editor, path)) {
      const isInline = SlateText.isText(child) || editor.isInline(child);

      if (!isInline) {
        Transforms.unwrapNodes(editor, { at: childPath });
        return true;
      }
    }
  }

  return false;
};
