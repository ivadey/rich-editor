import { BlockFormatEnum } from '../../../baseTypes';
import { strings } from '../../../helpers';
import { IToolbarButtonProps } from '../../../components/Toolbar/types';

const TableButton: IToolbarButtonProps = {
  icon: 'table',
  active: (editor) => editor.isElementSelected(BlockFormatEnum.Table),
  tooltip: strings.table,
  onClick: (editor) => (editor.isElementSelected(BlockFormatEnum.Table)
    ? editor.removeTable()
    : editor.insertTable(2, 2)),
  hotkeyHint: 'alt+shift+T',
};

export {
  // eslint-disable-next-line import/prefer-default-export
  TableButton,
};
