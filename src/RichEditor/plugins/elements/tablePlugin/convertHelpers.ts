import { Descendant, Element as SlateElement, Text as SlateText } from 'slate';
import { BlockFormatEnum, InlineFormatEnum } from '../../../baseTypes';

// eslint-disable-next-line import/prefer-default-export
export const listToHtml = (node: Descendant | Descendant[]): string => {
  if (SlateText.isText(node)) {
    let res = node.text;

    if (node[InlineFormatEnum.Bold]) res = `<b>${res}</b>`;
    if (node[InlineFormatEnum.Italic]) res = `<i>${res}</i>`;
    if (node[InlineFormatEnum.Strikethrough]) res = `<del>${res}</del>`;
    if (node[InlineFormatEnum.Subscript]) res = `<sub>${res}</sub>`;
    if (node[InlineFormatEnum.Superscript]) res = `<sup>${res}</sup>`;
    if (node[InlineFormatEnum.Underline]) res = `<u>${res}</u>`;

    return res;
  }

  if (SlateElement.isElementType(node, BlockFormatEnum.ListItem)) {
    return `<li>${listToHtml(node.children)}</li>`;
  }

  if (SlateElement.isElementType(node, BlockFormatEnum.NumberedList)) {
    return `<ol>${listToHtml(node.children)}</ol>`;
  }

  if (SlateElement.isElementType(node, BlockFormatEnum.BulletedList)) {
    return `<ul>${listToHtml(node.children)}</ul>`;
  }

  if (Array.isArray(node)) return node.map(listToHtml).join('');

  return '';
};
