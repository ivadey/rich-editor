/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { RenderElementProps } from 'slate-react';
import Table from './Table';
import TableRow from './TableRow';
import TableCell from './TableCell';
import { BlockFormatEnum } from '../../../../baseTypes';
import TableCellParagraph from './TableCellParagraph';

const TableElement = ({ attributes, children, element }: RenderElementProps): JSX.Element => {
  let TableComponent = TableCellParagraph;

  if (element.type === BlockFormatEnum.Table) TableComponent = Table;
  else if (element.type === BlockFormatEnum.TableRow) TableComponent = TableRow;
  else if (element.type === BlockFormatEnum.TableCell) TableComponent = TableCell;

  return (
    <TableComponent
      element={element}
      attributes={attributes}
    >
      {children}
    </TableComponent>
  );
};

TableElement.displayName = 'TableElement';

export default TableElement;
