/* eslint-disable react/jsx-props-no-spreading */
import React, { useEffect } from 'react';
import {
  ReactEditor, RenderElementProps, useReadOnly, useSelected, useSlate,
} from 'slate-react';
import { classNames } from '../../../../helpers';
import { Icon } from '../../../../components';
import { TableCellElement } from '../types';

const TableCell = ({ attributes, children, element }: RenderElementProps): JSX.Element => {
  let selected = useSelected();
  const isReadOnly = useReadOnly();
  const editor = useSlate();
  const selectedTable = editor.getSelectedTable();

  const path = ReactEditor.findPath(editor, element);
  const columnIndex = path[path.length - 1];
  const rowIndex = path[path.length - 2];
  const needToShowControls = !isReadOnly && selectedTable?.[1]?.join('') === path.slice(0, path.length - 2).join('');

  const isLastRow = rowIndex === (selectedTable && selectedTable[0].rowsCount - 1);
  const isLastColumn = columnIndex === (selectedTable && selectedTable[0].colsCount - 1);

  let fullSelection;
  let selectedForRemove;
  if (
    selectedTable
    && needToShowControls
    && (selectedTable[0].selectedRow !== undefined || selectedTable[0].selectedColumn !== undefined)
  ) {
    selected = selectedTable[0].selectedRow === rowIndex || selectedTable[0].selectedColumn === columnIndex;
    fullSelection = true;
    selectedForRemove = selectedTable[0].selectedForRemove;
  }

  useEffect(() => {
    editor.setSelectColumn(undefined);
    editor.setSelectRow(undefined);
  }, [editor, editor.selection]);

  return (
    <td
      className="re-table__cell"
      style={{ width: `${(element as TableCellElement).columnWidth}%` }}
      {...attributes}
    >
      {needToShowControls && selectedTable && rowIndex === 0 ? (
        <>
          <button
            contentEditable={false}
            type="button"
            className={classNames(
              're-table__cell__column-selection-button',
              selectedTable[0].selectedColumn === columnIndex && selectedForRemove ? 'selected-for-remove' : '',
            )}
            onClick={() => editor.setSelectColumn(columnIndex)}
          >
            &nbsp;
          </button>

          <button
            contentEditable={false}
            type="button"
            className="re-table__cell__add-column-button group"
            onClick={() => editor.addColumn(columnIndex)}
          >
            <span className="re-table__cell_add-button__placeholder" />
            <span className="re-table__cell_add-button__hidden">+</span>
          </button>

          {isLastColumn ? (
            <button
              contentEditable={false}
              type="button"
              className={classNames('re-table__cell__add-column-button group', 'last')}
              onClick={() => editor.addColumn()}
            >
              <span className="re-table__cell_add-button__placeholder" />
              <span className="re-table__cell_add-button__hidden">+</span>
            </button>
          ) : null}

          {selectedTable[0].selectedColumn === columnIndex ? (
            <button
              contentEditable={false}
              type="button"
              className="re-table__cell_remove-column-button"
              onClick={() => editor.deleteColumn(columnIndex)}
              onMouseEnter={() => editor.setSelectColumn(columnIndex, true)}
              onMouseLeave={() => editor.setSelectColumn(columnIndex, false)}
            >
              <Icon icon="times" />
            </button>
          ) : null}
        </>
      ) : null}

      {needToShowControls && selectedTable && columnIndex === 0 ? (
        <>
          <button
            contentEditable={false}
            type="button"
            className={classNames(
              're-table__cell__row-selection-button',
              selectedTable[0].selectedRow === rowIndex && selectedForRemove ? 'selected-for-remove' : '',
            )}
            onClick={() => editor.setSelectRow(rowIndex)}
          >
            &nbsp;
          </button>

          <button
            contentEditable={false}
            type="button"
            className="re-table__cell__add-row-button group"
            onClick={() => editor.addRow(rowIndex)}
          >
            <span className="re-table__cell_add-button__placeholder" />
            <span className="re-table__cell_add-button__hidden">+</span>
          </button>

          {isLastRow ? (
            <button
              contentEditable={false}
              type="button"
              className={classNames('re-table__cell__add-row-button group', 'last')}
              onClick={() => editor.addRow()}
            >
              <span className="re-table__cell_add-button__placeholder" />
              <span className="re-table__cell_add-button__hidden">+</span>
            </button>
          ) : null}

          {selectedTable[0].selectedRow === rowIndex ? (
            <button
              contentEditable={false}
              type="button"
              className="re-table__cell_remove-row-button"
              onClick={() => editor.deleteRow(rowIndex)}
              onMouseEnter={() => editor.setSelectRow(rowIndex, true)}
              onMouseLeave={() => editor.setSelectRow(rowIndex, false)}
            >
              <Icon icon="times" />
            </button>
          ) : null}
        </>
      ) : null}

      <span className={classNames(
        're-table__cell__content',
        selected ? 'selected' : '',
        selectedForRemove ? 'selected-for-remove' : '',
        selected && fullSelection ? 'full-selection' : '',
      )}
      >
        {children}
      </span>
    </td>
  );
};

TableCell.displayName = 'TableCell';

export default TableCell;
