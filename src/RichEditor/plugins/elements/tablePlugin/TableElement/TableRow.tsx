/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { RenderElementProps } from 'slate-react';

const TableRow = ({ attributes, children }: RenderElementProps): JSX.Element => (
  <tr className="re-table__row" {...attributes}>{children}</tr>
);

TableRow.displayName = 'TableRow';

export default TableRow;
