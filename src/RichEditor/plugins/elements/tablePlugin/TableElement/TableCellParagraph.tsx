/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { RenderElementProps } from 'slate-react';

const TableCellParagraph = ({ attributes, children }: RenderElementProps): JSX.Element => (
  <p
    className="re-table__cell__paragraph"
    {...attributes}
  >
    {children}
  </p>
);

TableCellParagraph.displayName = 'TableCellParagraph';

export default TableCellParagraph;
