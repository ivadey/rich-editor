/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { RenderElementProps } from 'slate-react';

const Table = ({ attributes, children }: RenderElementProps): JSX.Element => (
  <div className="re-table">
    <table>
      <tbody {...attributes}>
        {children}
      </tbody>
    </table>
  </div>
);

Table.displayName = 'Table';

export default Table;
