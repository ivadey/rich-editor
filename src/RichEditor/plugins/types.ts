import React from 'react';
import {
  Editor,
  Element as SlateElement,
  Text as SlateText,
  Node as SlateNode,
  NodeEntry,
  Range as SlateRange,
} from 'slate';
import { RenderElementProps, RenderLeafProps } from 'slate-react';
import { ElementConvertors } from '../../Converter/types';
import { IToolbarButtonProps } from '../components/Toolbar/types';
import { IFloatingMenuProps } from '../components/FloatingMenu';
import { InlineMenuDescription } from '../inlineMenuExtension';

export type BlockElementDescription = {
  type: 'block';
  formatName: SlateElement['type'];
  floatingMenu?: IFloatingMenuProps['buttons'];
  hoveringMenu?: IFloatingMenuProps['buttons'];
  clearFormatOnEnter?: boolean;
  /**
   * Will insert empty block above and set cursor there by pressing 'mod+shift+enter' hotkey
   */
  enableAboveExitBreak?: boolean;
  /**
   * Will insert empty block below and set cursor there by pressing 'mod+enter' hotkey
   */
  enableBelowExitBreak?: boolean;
  // todo: remove className parameter?
  render: (props: RenderElementProps, className?: string | string[]) => React.ReactNode;
  convertors: ElementConvertors;
}

export type InlineElementDescription = {
  type: 'inline';
  formatName: keyof Omit<SlateText, 'text'>;
  render: (props: RenderLeafProps) => React.ReactNode;
  convertors: ElementConvertors;
}

export interface ElementsPlugin {
  name: string;
  editorWrapper?: (editor: Editor) => Editor;
  onKeyDownHandler?: (event: React.KeyboardEvent<HTMLDivElement>, editor: Editor) => boolean;
  onPaste?: (event: React.ClipboardEvent<HTMLDivElement>) => boolean;
  toolbarButtons?: { [key: string]: React.ReactNode | IToolbarButtonProps };
  elements: Array<BlockElementDescription | InlineElementDescription>;
  /**
   * Will be called when editor was initialized and mounted
   */
  onMounted?: (editor: Editor) => void;
  hotkeys?: {
    [hotKey: string]: (editor: Editor) => void;
  }
  inlineMenu?: (editor: Editor) => InlineMenuDescription;
  decorate?: (editor: Editor, nodeEntry: NodeEntry<SlateNode>) => SlateRange[];
  /**
   * Should return true if normalization was proceeded and false if we need to fall back to the original method
   * @param editor
   * @param entry
   */
  normalizeNodes?: (editor: Editor, entry: NodeEntry) => boolean;
}

type UtilityExcludedProperties = 'elements';
export interface UtilityOnlyPlugin extends Omit<ElementsPlugin, UtilityExcludedProperties>{
  utilityOnly: true;
}

export type RichEditorPlugin = ElementsPlugin | UtilityOnlyPlugin;
