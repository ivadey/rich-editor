import { withHistory } from 'slate-history';
import { UtilityOnlyPlugin } from '../types';
import { IToolbarButtonProps } from '../../components/Toolbar/types';
import { strings } from '../../helpers';

interface HistoryPlugin extends UtilityOnlyPlugin {
  toolbarButtons: {
    undo: IToolbarButtonProps;
    redo: IToolbarButtonProps;
  }
}

const historyPlugin: HistoryPlugin = {
  name: 'History plugin',
  utilityOnly: true,
  editorWrapper: withHistory,
  toolbarButtons: {
    undo: {
      icon: 'undo',
      tooltip: strings.undo,
      hotkeyHint: 'mod+Z',
      onClick: (editor) => editor.undo(),
      disabled: (editor) => !editor.history.undos.length,
    },
    redo: {
      icon: 'redo',
      tooltip: strings.redo,
      hotkeyHint: 'mod+shift+Z',
      onClick: (editor) => editor.redo(),
      disabled: (editor) => !editor.history.redos.length,
    },
  },
  hotkeys: {
    'mod+z': (editor) => editor.undo(),
    'mod+shift+z': (editor) => editor.redo(),
  },
};

export default historyPlugin;
