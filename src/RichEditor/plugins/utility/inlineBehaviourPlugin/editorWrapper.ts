/* eslint-disable no-param-reassign */
import { Editor, Range as SlateRange, Transforms } from 'slate';
import { InlineFormatEnum } from '../../../baseTypes';

const editorWrapper = (editor: Editor): Editor => {
  const { insertText, deleteBackward, insertBreak } = editor;

  editor.insertText = (text: string) => {
    // Reset inline element if we are typing in the end
    const { element: currentElement } = editor.getCurrentElement('lowest');
    const isCurrentElementInline = currentElement && editor.isInline(currentElement);
    const isFocusedAndCollapsed = editor.selection && SlateRange.isCollapsed(editor.selection);

    if (isCurrentElementInline && isFocusedAndCollapsed) {
      const elementEdges = editor.getCurrentElementEdges();
      const location = elementEdges && { anchor: elementEdges[0], focus: elementEdges[1] };
      const [, nextNodePath] = Editor.next(editor) || [];
      const isTheEndOfElement = editor.selection && location
        && Editor.isEnd(editor, editor.selection.anchor, location);

      if (isTheEndOfElement && nextNodePath) {
        Transforms.insertText(editor, text, { at: { path: nextNodePath, offset: 0 } });
        Transforms.move(editor, { distance: text.length });

        return;
      }
    }

    // Fallback to default insert text
    insertText(text);
  };

  editor.deleteBackward = (unit) => {
    const currentMarks = Editor.marks(editor) || {};
    deleteBackward(unit);

    if (unit === 'character' && editor.selection && Object.keys(currentMarks).length) {
      const textEdges = editor.getCurrentTextEdges();
      const location = textEdges && { anchor: textEdges[0], focus: textEdges[1] };

      if (location && Editor.isStart(editor, editor.selection.anchor, location)) {
        Transforms.move(editor, { reverse: true });
        Transforms.move(editor);

        (Object.keys(currentMarks) as (keyof typeof currentMarks)[])
          .forEach((mark) => Editor.addMark(editor, mark, currentMarks[mark]));
      }
    }
  };

  editor.insertBreak = () => {
    if (!editor.selection) return;

    const textEdges = editor.getCurrentTextEdges();
    const location = textEdges && { anchor: textEdges[0], focus: textEdges[1] };

    const formattingToReset = [
      InlineFormatEnum.InlineCode,
      InlineFormatEnum.Subscript,
      InlineFormatEnum.Superscript,
    ];
    const isFocusedAndCollapsed = SlateRange.isCollapsed(editor.selection);
    const isInTheStartOfText = location && Editor.isStart(editor, editor.selection.anchor, location);
    const isInTheEndOfText = location && Editor.isEnd(editor, editor.selection.anchor, location);

    if (isFocusedAndCollapsed && isInTheStartOfText) {
      formattingToReset.forEach((formatting) => {
        Editor.removeMark(editor, formatting);
      });

      editor.insertText(' ');
      insertBreak();
      Transforms.move(editor, { unit: 'character', reverse: true, distance: 1 });
      editor.deleteBackward('character');
      Transforms.move(editor, { unit: 'character', distance: 1 });

      return;
    }

    insertBreak();

    if (isFocusedAndCollapsed && isInTheEndOfText) {
      formattingToReset.forEach((formatting) => {
        Editor.removeMark(editor, formatting);
      });
    }
  };

  return editor;
};

export default editorWrapper;
