import { UtilityOnlyPlugin } from '../../types';
import editorWrapper from './editorWrapper';

const inlineBehaviourPlugin: UtilityOnlyPlugin = {
  name: 'Inline behavior plugin',
  editorWrapper,
  utilityOnly: true,
};

export default inlineBehaviourPlugin;
