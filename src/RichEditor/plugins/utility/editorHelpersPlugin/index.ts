import { UtilityOnlyPlugin } from '../../types';
import editorWrapper from './editorWrapper';

const editorHelpersPlugin: UtilityOnlyPlugin = {
  name: 'Editor helper plugin',
  utilityOnly: true,
  editorWrapper,
};

export default editorHelpersPlugin;
