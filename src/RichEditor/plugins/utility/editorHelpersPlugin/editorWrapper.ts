/* eslint-disable no-param-reassign */
import {
  Editor, EditorBeforeOptions,
  Element as SlateElement, Location,
  Location as SlateLocation,
  Transforms,
  Path as SlatePath,
  Text as SlateText,
} from 'slate';
import { SelectionMode } from 'slate/dist/interfaces/types';
import { BlockFormatEnum } from '../../../baseTypes';

const editorWrapper = (editor: Editor): Editor => {
  editor.isElementSelected = (type, forSelection) => {
    const selection = forSelection ?? editor.selection;

    if (!selection) return false;

    const [selectedElement] = Editor.nodes(editor, {
      match: (node) => !Editor.isEditor(node)
        && SlateElement.isElement(node)
        && node.type === type,
      at: selection,
      mode: 'highest',
    });

    return !!selectedElement;
  };

  editor.insertSoftBreak = () => editor.insertText('\n');

  editor.isBlockEmpty = (path) => !Editor.string(editor, path);

  editor.toggleFormat = (format, options = {}) => {
    if (!editor.selection) return;

    const isBlockEmpty = editor.isBlockEmpty(editor.selection.anchor.path);
    const element = {
      type: format,
      children: options.children || [{ text: '' }],
    } as SlateElement;

    if (editor.isElementSelected(element.type)) Transforms.setNodes(editor, { type: BlockFormatEnum.Paragraph });
    else if (isBlockEmpty || options.wrapNonEmpty) {
      // todo: investigate why are we getting here ts error
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      Transforms.setNodes(editor, { type: element.type });
      if (isBlockEmpty) {
        Transforms.insertNodes<SlateElement>(editor, element.children, { at: editor.selection.focus.path });
      }
    } else Transforms.insertNodes(editor, element);
  };

  editor.isMarkActive = (mark) => {
    const marks = Editor.marks(editor);
    return marks ? !!marks[mark] : false;
  };

  editor.toggleMark = (mark) => {
    const isActive = editor.isMarkActive(mark);

    if (isActive) Editor.removeMark(editor, mark);
    else Editor.addMark(editor, mark, true);
  };

  editor.getCurrentElement = <ElementType extends SlateElement>(mode: SelectionMode = 'all') => {
    const { selection } = editor;
    if (!selection) return { element: null, path: null };

    const [match] = Array.from(
      Editor.nodes<ElementType>(editor, {
        at: Editor.unhangRange(editor, selection),
        match: (node) => !Editor.isEditor(node) && SlateElement.isElement(node),
        mode,
      }),
    );

    return match ? { element: match[0], path: match[1] } : { element: null, path: null };
  };

  editor.resetBlockFormat = (forPath) => {
    const path = forPath;

    const options: { at?: SlateLocation } = {};
    if (forPath) options.at = path;

    Transforms.setNodes(editor, { type: BlockFormatEnum.Paragraph }, options);
  };

  editor.getCurrentString = () => {
    const currentPath = editor.selection?.anchor?.path || [0, 0];
    return Editor.string(editor, currentPath);
  };

  editor.getTextBefore = (start: Location, unit: EditorBeforeOptions['unit']) => {
    const wordBeforePoint = Editor.before(editor, start, { unit });
    const beforeRange = wordBeforePoint && Editor.range(editor, wordBeforePoint, start);

    return beforeRange && Editor.string(editor, beforeRange);
  };

  editor.getTextAfter = (start: Location, unit: EditorBeforeOptions['unit']) => {
    const wordAfterPoint = Editor.after(editor, start, { unit });
    const afterRange = wordAfterPoint && Editor.range(editor, start, wordAfterPoint);

    return afterRange && Editor.string(editor, afterRange);
  };

  editor.getSelectedText = () => {
    if (!editor.selection) return '';
    return Editor.string(editor, editor.selection);
  };

  // todo: doesn't work in some cases (at lest with images)
  editor.insertExitBreakAbove = (at: SlatePath) => {
    if (!at) return;

    Transforms.insertNodes(
      editor,
      [{ type: BlockFormatEnum.Paragraph, children: [{ text: '' }] }],
      { at },
    );
    Transforms.select(editor, at);
  };

  // todo: doesn't work in some cases (at lest with images)
  editor.insertExitBreakBelow = (at: SlatePath) => {
    if (!at) return;

    const belowPath = [...at];
    belowPath[at.length - 1] += 1;

    Transforms.insertNodes(
      editor,
      [{ type: BlockFormatEnum.Paragraph, children: [{ text: '' }] }],
      { at: belowPath },
    );

    Transforms.select(editor, belowPath);
  };

  editor.getCurrentElementEdges = () => {
    const { path } = editor.getCurrentElement('lowest');

    return path ? [
      Editor.start(editor, path),
      Editor.end(editor, path),
    ] : null;
  };
  editor.getCurrentTextEdges = () => {
    if (!editor.selection) return null;

    const [match] = Array.from(
      Editor.nodes<SlateText>(editor, {
        at: Editor.unhangRange(editor, editor.selection),
        match: (node) => SlateText.isText(node),
        mode: 'lowest',
      }),
    );
    if (!match) return null;

    const [, textPath] = match;

    return textPath ? [
      Editor.start(editor, textPath),
      Editor.end(editor, textPath),
    ] : null;
  };

  return editor;
};

export default editorWrapper;
