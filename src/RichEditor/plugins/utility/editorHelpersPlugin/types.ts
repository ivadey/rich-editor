import {
  Descendant,
  EditorBeforeOptions,
  Element as SlateElement,
  Location,
  Path,
  Point,
  Selection as SlateSelection,
  Text as SlateText,
} from 'slate';
import { SelectionMode } from 'slate/dist/interfaces/types';

type CurrentElementInfo<ElementType extends SlateElement> = {
  element: ElementType;
  path: Path;
} | {
  element: null;
  path: null;
}

interface ToggleFormatOptions {
  wrapNonEmpty?: boolean;
  children?: Descendant[];
}

export interface EditorHelpersEditor {
  insertSoftBreak: () => void;
  isBlockEmpty: (path: Path) => boolean;
  toggleFormat: (format: SlateElement['type'], options?: ToggleFormatOptions) => void;
  isElementSelected: (type: SlateElement['type'], selection?: SlateSelection) => boolean;
  toggleMark: (mark: keyof Omit<SlateText, 'text'>) => void;
  isMarkActive: (mark: keyof Omit<SlateText, 'text'>) => boolean;
  /**
   * @param [mode='all']
   */
  getCurrentElement: <ElementType extends SlateElement>(mode?: SelectionMode) => CurrentElementInfo<ElementType>;
  resetBlockFormat: (forPath?: Path) => void;
  getCurrentString: () => string;
  getTextBefore: (start: Location, unit: EditorBeforeOptions['unit']) => string | undefined;
  getTextAfter: (start: Location, unit: EditorBeforeOptions['unit']) => string | undefined;
  getSelectedText: () => string;
  insertExitBreakAbove: (at: Path) => void;
  insertExitBreakBelow: (at: Path) => void;
  getCurrentElementEdges: () => [Point, Point] | null;
  getCurrentTextEdges: () => [Point, Point] | null;
}
