/* eslint-disable react/destructuring-assignment,react/jsx-props-no-spreading */
import React, { useRef, useState } from 'react';
import { Range as SlateRange, Node as SlateNode } from 'slate';
import {
  RenderElementProps,
  useReadOnly,
  useSelected,
  useSlate,
} from 'slate-react';
import { BlockElementDescription } from '../plugins/types';
import { FloatingMenu, Placeholder } from '../components';

export default (blocks: BlockElementDescription[], placeholder: string) => (
  { attributes, children, element }: RenderElementProps,
): JSX.Element => {
  const editor = useSlate();
  const selected = useSelected();
  const isReadonly = useReadOnly();

  const [isHovering, setIsHovering] = useState(false);
  const hoveringTimer = useRef<ReturnType<typeof setTimeout>>();

  const customBlock = blocks.find((block) => block.formatName === element.type);

  if (customBlock) {
    let content = customBlock.render({ attributes, children, element }) as JSX.Element;

    const { floatingMenu, hoveringMenu } = customBlock;
    const { selection } = editor;

    if ((floatingMenu || hoveringMenu) && !isReadonly && selection && SlateRange.isCollapsed(selection)) {
      const TagName = editor.isInline(element) ? 'span' : 'div';
      content = (
        <TagName
          className="relative"
          onMouseEnter={() => setIsHovering(true)}
          onMouseLeave={() => {
            hoveringTimer.current = setTimeout(() => setIsHovering(false), 300);
          }}
        >
          {content}
        </TagName>
      );

      let menuButtons;
      if (isHovering && hoveringMenu) menuButtons = hoveringMenu;
      else if (selected && floatingMenu) menuButtons = floatingMenu;

      if (menuButtons) {
        content = (
          <FloatingMenu
            buttons={menuButtons}
            onMouseEnter={() => {
              if (isHovering && hoveringTimer.current) clearTimeout(hoveringTimer.current);
            }}
            onMouseLeave={() => setIsHovering(false)}
            element={element}
            elementRef={attributes.ref}
          >
            {content}
          </FloatingMenu>
        );
      }
    }

    if (content) return content;
  }

  const currentElement = editor.getCurrentElement('lowest').element;
  const elementText = currentElement ? SlateNode.string(currentElement) : '';

  return (
    <p className="re-paragraph" {...attributes}>
      {children}
      {!!placeholder && !isReadonly && !elementText && <Placeholder text={placeholder} />}
    </p>
  );
};
