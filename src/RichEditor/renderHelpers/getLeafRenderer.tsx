/* eslint-disable react/destructuring-assignment,react/jsx-props-no-spreading */
import React from 'react';
import { RenderLeafProps } from 'slate-react';
import { InlineElementDescription } from '../plugins/types';

export default (leafs: InlineElementDescription[]) => (renderProps: RenderLeafProps): JSX.Element => {
  let leafChildren = renderProps.children;

  leafs.forEach((leaf) => {
    if (renderProps.leaf[leaf.formatName]) leafChildren = leaf.render({ ...renderProps, children: leafChildren });
  });

  return (
    <span {...renderProps.attributes}>
      {leafChildren}
    </span>
  );
};
