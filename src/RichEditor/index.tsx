import React from 'react';
import {
  Descendant,
  Node as SlateNode,
  NodeEntry,
  Range as SlateRange,
  Element as SlateElement,
  Editor,
} from 'slate';
import { Editable, Slate } from 'slate-react';
import isHotkey from 'is-hotkey';
import Toolbar from './components/Toolbar';
import createCustomEditor from './helpers/createCustomEditor';
import {
  BlockElementDescription, InlineElementDescription, RichEditorPlugin,
} from './plugins/types';
import {
  parsePlugins, ParsedPlugins, strings, classNames, getNormalizedValue,
} from './helpers';
import { getElementRenderer, getLeafRenderer } from './renderHelpers';
import { InlineMenuPopover, keyDownHandler as inlineMenuKeyDownHandler } from './inlineMenuExtension';
import { BlockFormatEnum } from './baseTypes';
import { IToolbarProps } from './components/Toolbar/types';
import TableOfContent from './components/TableOfContent';
import generateAndGetTableOfContent from './components/TableOfContent/generateAndGetTableOfContent';
import { TableOfContentInfo } from './components/TableOfContent/types';
import { SelectionFloatingMenu } from './components';
import { SelectionFloatingMenuProps } from './components/SelectionFloatingMenu';

export interface RichEditorProps {
  toolbarClassName?: string;
  editableAreaClassName?: string;
  editableClassName?: string;
  initialValue?: Descendant[];
  plugins?: RichEditorPlugin[];
  toolbarButtons?: IToolbarProps['buttons'];
  toolbarPosition?: IToolbarProps['position'];
  language?: 'ru';
  readonly?: boolean;
  beforeEditable?: React.ReactNode;
  afterEditable?: React.ReactNode;
  onChange?: (editorValue: Descendant[]) => void;
  onPaste?: (event: React.ClipboardEvent<HTMLDivElement>, editor: Editor) => void;
  placeholder?: string;
  tableOfContents?: {
    className?: string;
    itemClassName?: string;
    maxDepth: number;
  }
  selectionFloatingMenu?: SelectionFloatingMenuProps['buttons'];
  autoFocus?: boolean;
}
interface IRichEditorState {
  editorValue: Descendant[];
  tableOfContents: TableOfContentInfo[];
}

class RichEditor extends React.Component<RichEditorProps, IRichEditorState> {
  readonly editor: Editor;
  readonly plugins: RichEditorPlugin[];
  readonly leafs: InlineElementDescription[];
  readonly blocks: BlockElementDescription[];
  readonly hotkeysList: ParsedPlugins['hotkeysList'];
  readonly hotkeysHandlers: ParsedPlugins['hotkeysHandlers'];
  readonly formatsToClearOnEnter: ParsedPlugins['formatsToClearOnEnter'];
  readonly aboveExitBreaks: ParsedPlugins['aboveExitBreaks'];
  readonly belowExitBreaks: ParsedPlugins['belowExitBreaks'];
  readonly inlineMenuTriggers: ParsedPlugins['inlineMenuTriggers'];
  readonly onMountedHandlers: ParsedPlugins['onMountedHandlers'];
  readonly decorators: Required<RichEditorPlugin>['decorate'][];

  mounted: boolean;
  tocRecalcTimer: ReturnType<typeof setTimeout> | null;

  constructor(props: RichEditorProps) {
    super(props);

    strings.setLanguage(props.language || 'ru');

    this.plugins = props.plugins || [];
    this.editor = createCustomEditor(this.plugins);
    const parsedPlugins = parsePlugins(this.editor, this.plugins);

    this.state = {
      editorValue: getNormalizedValue(props.initialValue || [{
        type: BlockFormatEnum.Paragraph,
        children: [{ text: '' }],
      }], this.plugins),
      tableOfContents: [],
    };

    this.blocks = parsedPlugins.blocks;
    this.leafs = parsedPlugins.leafs;
    this.hotkeysList = parsedPlugins.hotkeysList;
    this.hotkeysHandlers = parsedPlugins.hotkeysHandlers;
    this.formatsToClearOnEnter = parsedPlugins.formatsToClearOnEnter;
    this.aboveExitBreaks = parsedPlugins.aboveExitBreaks;
    this.belowExitBreaks = parsedPlugins.belowExitBreaks;
    this.inlineMenuTriggers = parsedPlugins.inlineMenuTriggers;
    this.decorators = parsedPlugins.decorators;
    this.onMountedHandlers = parsedPlugins.onMountedHandlers;

    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.handlePaste = this.handlePaste.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.decorate = this.decorate.bind(this);

    this.mounted = false;
    this.tocRecalcTimer = null;
  }

  componentDidMount() {
    this.mounted = true;

    this.onMountedHandlers.forEach((handler) => handler(this.editor));

    const { tableOfContents } = this.props;
    if (!tableOfContents || !tableOfContents.maxDepth) return;

    this.setState({ tableOfContents: generateAndGetTableOfContent(tableOfContents.maxDepth) });
  }

  componentDidUpdate(prevProps: Readonly<RichEditorProps>) {
    const { props: nextProps } = this;

    if (prevProps.language !== nextProps.language) {
      strings.setLanguage(nextProps.language || 'ru');
    }

    if (nextProps.tableOfContents && prevProps.tableOfContents?.maxDepth !== nextProps.tableOfContents?.maxDepth) {
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({ tableOfContents: generateAndGetTableOfContent(nextProps.tableOfContents.maxDepth) });
    }
  }

  handleKeyDown(event: React.KeyboardEvent<HTMLDivElement>) {
    if (inlineMenuKeyDownHandler(this.editor, this.inlineMenuTriggers, event)) return;

    if (this.plugins.some((p) => p.onKeyDownHandler?.(event, this.editor))) return;

    const pressedHotkey = this.hotkeysList.find((hotkey) => isHotkey(hotkey, event));
    if (pressedHotkey && this.hotkeysHandlers[pressedHotkey]) {
      event.preventDefault();
      this.hotkeysHandlers[pressedHotkey](this.editor);
      return;
    }

    const { element: currentElement, path: currentElementPath } = this.editor.getCurrentElement('lowest');

    // Clear formatting on enter
    if (
      currentElement
      && this.editor.selection
      && Editor.isEnd(this.editor, this.editor.selection.focus, currentElementPath)
      && !event.shiftKey
      && event.code === 'Enter'
      && this.formatsToClearOnEnter.indexOf(currentElement.type) > -1
    ) {
      event.preventDefault();
      this.editor.insertBreak();
      this.editor.resetBlockFormat();
    }

    const isAboveExitBreakHotkey = isHotkey('mod+shift+enter', event);
    const isBelowExitBreakHotkey = isHotkey('mod+enter', event);
    if (isAboveExitBreakHotkey || isBelowExitBreakHotkey) {
      const elementTypeForBreak = isAboveExitBreakHotkey
        ? this.aboveExitBreaks.find((elementType) => this.editor.isElementSelected(elementType))
        : this.belowExitBreaks.find((elementType) => this.editor.isElementSelected(elementType));
      if (!elementTypeForBreak) return;

      const [elementForBreakMatch] = Editor.nodes(this.editor, {
        match: (node) => SlateElement.isElementType(node, elementTypeForBreak),
        mode: 'lowest',
      });
      if (!elementForBreakMatch) return;

      event.preventDefault();

      const [, elementForBreakPath] = elementForBreakMatch;
      if (isAboveExitBreakHotkey) this.editor.insertExitBreakAbove(elementForBreakPath);
      else this.editor.insertExitBreakBelow(elementForBreakPath);
    }

    if (event.key === 'Tab') {
      event.preventDefault();
      this.editor.insertText('  ');
    }
  }

  handlePaste(event: React.ClipboardEvent<HTMLDivElement>) {
    const { onPaste = () => null } = this.props;
    onPaste(event, this.editor);

    if (event.isDefaultPrevented()) return;

    const elementType = (this.editor.getFragment() as SlateElement[])[0]?.type;

    // todo: add ability to hande paste in plugins
    if (elementType === BlockFormatEnum.Code) {
      event.preventDefault();
      this.editor.insertText(event.clipboardData.getData('Text'));
    }
  }

  handleChange(value: Descendant[]) {
    const { tableOfContents: tableOfContentsConfig, onChange } = this.props;
    onChange?.(value);

    if (!tableOfContentsConfig || !tableOfContentsConfig.maxDepth) return;

    if (this.tocRecalcTimer) clearTimeout(this.tocRecalcTimer);
    this.tocRecalcTimer = setTimeout(() => {
      this.setState({ tableOfContents: generateAndGetTableOfContent(tableOfContentsConfig.maxDepth) });
    }, 300);
  }

  decorate(nodeEntry: NodeEntry<SlateNode>): SlateRange[] {
    const ranges: SlateRange[] = [];

    this.decorators.forEach((decorator) => {
      ranges.push(...decorator(this.editor, nodeEntry));
    });

    return ranges;
  }

  render() {
    const { editorValue, tableOfContents } = this.state;
    const {
      toolbarPosition, toolbarButtons, readonly, beforeEditable, afterEditable, tableOfContents: tableOfContentsConfig,
      placeholder, toolbarClassName, editableAreaClassName, editableClassName, selectionFloatingMenu, autoFocus,
    } = this.props;

    return (
      <Slate
        editor={this.editor}
        value={editorValue}
        onChange={this.handleChange}
      >
        <SelectionFloatingMenu buttons={selectionFloatingMenu} />

        {Array.isArray(toolbarButtons) && toolbarButtons.length && toolbarPosition !== 'bottom' ? (
          <Toolbar
            className={toolbarClassName}
            buttons={toolbarButtons}
            position={toolbarPosition}
          />
        ) : null}

        {beforeEditable && React.isValidElement(beforeEditable) ? beforeEditable : null}

        <div className={classNames('re-editable-area', editableAreaClassName)}>
          <Editable
            className={classNames('re-editable', editableClassName)}
            onKeyDown={this.handleKeyDown}
            onPaste={this.handlePaste}
            renderElement={getElementRenderer(this.blocks, placeholder || strings.typeSomething)}
            // todo: add 2 types of placeholder – when focused and non focused
            renderLeaf={getLeafRenderer(this.leafs)}
            decorate={this.decorate}
            autoFocus={!!autoFocus}
            readOnly={!!readonly}
          />

          {this.mounted && tableOfContents.length > 0 && (
            <TableOfContent
              className={tableOfContentsConfig?.className}
              itemClassName={tableOfContentsConfig?.itemClassName}
              tableOfContent={tableOfContents}
            />
          )}
        </div>

        {afterEditable && React.isValidElement(afterEditable) ? afterEditable : null}

        {Array.isArray(toolbarButtons) && toolbarButtons.length && toolbarPosition === 'bottom' ? (
          <Toolbar
            className={toolbarClassName}
            buttons={toolbarButtons}
            position={toolbarPosition}
          />
        ) : null}

        <InlineMenuPopover />
      </Slate>
    );
  }
}

export default RichEditor;
