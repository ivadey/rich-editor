# RichEditor

## Props

| Name             | Required | Default | Description                                                     |
|------------------|---------:|--------:|-----------------------------------------------------------------|
| initialValue     |       No |         |                                                                 |
| onSave           |       No |         |                                                                 |
| enableAutoSave   |       No | `false` |                                                                 |
| autoSaveInterval |       No |    5000 | Interval to call onSave handler in ms if auto saving is enabled |

