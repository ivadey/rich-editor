export enum InlineFormatEnum {
  Bold = 'bold',
  Italic = 'italic',
  Underline = 'underline',
  InlineCode = 'inline-code',
  Subscript = 'subscript',
  Superscript = 'superscript',
  Strikethrough = 'strikethrough',

  DecoratedLink = 'decorated-link',
  // todo: think how to reorganize
  CodeBlockHighlighting = 'codeBlockHighlighting',
}

export enum BlockFormatEnum {
  Paragraph = 'paragraph',

  // textStylePlugin
  Heading = 'heading',

  // codePlugin
  Code = 'code',
  CodeLine = 'code-line',

  Link = 'link',

  Image = 'image',

  // blockquotePlugin
  Blockquote = 'block-quote',

  // listsPlugin
  NumberedList = 'numbered-list',
  BulletedList = 'bulleted-list',
  TodoList = 'todo-list',
  ListItem = 'list-item',

  // alignPlugin
  AlignLeft = 'align-left',
  AlignCenter = 'align-center',
  AlignRight = 'align-right',
  AlignJustify = 'align-justify',

  // ChecklistItem = 'checklist-item',

  // tablePlugin
  Table = 'table',
  TableRow = 'table-row',
  TableCell = 'table-cell',
  TableCellParagraph = 'table-cell-paragraph',

  // Admonition
  Admonition = 'admonition',
  AdmonitionTitle = 'admonition-title',
  AdmonitionContent = 'admonition-content',

  // Carousel
  Carousel = 'carousel',

  // Tooltip
  Tooltip = 'tooltip',

  // Mentions
  Mention = 'mention',

  // Horizontal divider
  Divider = 'divider',

  Accordion = 'accordion',
  AccordionTitle = 'accordion-title',
  AccordionContent = 'accordion-content',

  AnchorSlug = 'anchor-slug',
}
