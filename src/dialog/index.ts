import prompt from './prompt';
import alert from './alert';

interface Dialog {
  prompt: typeof prompt;
  alert: typeof alert;
}

const dialog: Dialog = {
  prompt,
  alert,
};

interface OverrideDialogMethods {
  prompt?: typeof prompt;
  alert?: typeof alert;
}

const overrideDialog = (overrides: OverrideDialogMethods) => {
  if (overrides.prompt) dialog.prompt = overrides.prompt;
  if (overrides.alert) dialog.alert = overrides.alert;
};

export default dialog;
export { overrideDialog };
