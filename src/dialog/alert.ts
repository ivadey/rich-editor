interface AlertOptions {
  title?: string;
  message?: string;
}

export default (options: AlertOptions): Promise<void> => new Promise((resolve) => {
  // eslint-disable-next-line no-alert
  alert(options.message ?? '');
  resolve();
});
