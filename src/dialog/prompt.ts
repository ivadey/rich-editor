export type PromptOptions = {
  title?: string;
  message?: string;
  initialValue?: unknown;
}

export default (options: PromptOptions): Promise<string> => new Promise((resolve, reject) => {
  // eslint-disable-next-line no-alert
  const res = prompt(options.message, options.initialValue as string);

  if (res === null) reject();
  else resolve(res);
});
