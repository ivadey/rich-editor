type ExtendableTypes = 'CustomElement' | 'CustomSlateEditor' | 'CustomText';

export interface RichEditorCustomTypes {
  [key: string]: unknown
}

export type RichEditorExtendedType<
  K extends ExtendableTypes,
  B
> = unknown extends RichEditorCustomTypes[K] ? B : RichEditorCustomTypes[K]
