import { BaseEditor, BaseElement } from 'slate';
import { ReactEditor } from 'slate-react';
import { HistoryEditor } from 'slate-history';
import { BlockFormatEnum } from '../RichEditor/baseTypes';
import {
  AdmonitionContentElement,
  AdmonitionEditor,
  AdmonitionElement,
  AdmonitionTitleElement,
} from '../RichEditor/plugins/elements/admonitionPlugin/types';
import { BlockquoteEditor, BlockquoteElement } from '../RichEditor/plugins/elements/blockquotePlugin/types';
import { CarouselEditor, CarouselElement } from '../RichEditor/plugins/elements/carouselPlugin/types';
import {
  CodeEditor, CodeElement, CodeLineElement, CodeSnippetHighlightedText,
} from '../RichEditor/plugins/elements/codePlugin/types';
import { DividerEditor, DividerElement } from '../RichEditor/plugins/elements/dividerPlugin/types';
import { ImageEditor, ImageElement } from '../RichEditor/plugins/elements/imagesPlugin/types';
import { DecoratedLinkText, LinkEditor, LinkElement } from '../RichEditor/plugins/elements/linksPlugin/types';
import { ListEditor, ListElement, ListItemElement } from '../RichEditor/plugins/elements/listsPlugin/types';
import {
  TableCellElement, TableCellParagraphElement, TableEditor, TableElement, TableRowElement,
} from '../RichEditor/plugins/elements/tablePlugin/types';
import { TooltipEditor, TooltipElement } from '../RichEditor/plugins/elements/tooltipPlugin/types';
import { HeadingEditor, HeadingElement } from '../RichEditor/plugins/elements/textStylePlugin/types';
import { InlineMenuEditor } from '../RichEditor/inlineMenuExtension';
import { AlignEditor, AlignElement } from '../RichEditor/plugins/elements/alignPlugin/types';
import { FormattedText } from '../RichEditor/plugins/elements/formattingPlugin/types';
import { EditorHelpersEditor } from '../RichEditor/plugins/utility/editorHelpersPlugin/types';
import { RichEditorExtendedType } from './customTypes';
import { AnchorEditor, AnchorElement } from '../RichEditor/plugins/elements/anchorPlugin/types';
import {
  AccordionEditor,
  AccordionElement,
  AccordionTitleElement,
  AccordionContentElement,
} from '../RichEditor/plugins/elements/accordionPlugin/types';

export type CustomText = FormattedText
  & CodeSnippetHighlightedText
  & DecoratedLinkText;

interface BaseCustomElement extends BaseElement {
  type: BlockFormatEnum.Paragraph;
}

export type CustomElement = BaseCustomElement
  | AdmonitionElement
  | CarouselElement
  | CodeElement
  | ImageElement
  | LinkElement
  | TooltipElement
  | HeadingElement
  | AdmonitionTitleElement
  | AdmonitionContentElement
  | BlockquoteElement
  | CodeLineElement
  | DividerElement
  | ListElement
  | ListItemElement
  | TableElement
  | TableRowElement
  | TableCellElement
  | TableCellParagraphElement
  | AlignElement
  | AnchorElement
  | AccordionElement
  | AccordionTitleElement
  | AccordionContentElement

export type CustomSlateEditor = BaseEditor
  & ReactEditor
  & HistoryEditor
  & AdmonitionEditor
  & CarouselEditor
  & CodeEditor
  & ImageEditor
  & LinkEditor
  & TooltipEditor
  & TableEditor
  & InlineMenuEditor
  & AlignEditor
  & DividerEditor
  & EditorHelpersEditor
  & BlockquoteEditor
  & ListEditor
  & HeadingEditor
  & AnchorEditor
  & AccordionEditor

declare module 'slate' {
  interface CustomTypes {
    Editor: RichEditorExtendedType<'CustomSlateEditor', CustomSlateEditor>;
    Element: RichEditorExtendedType<'CustomElement', CustomElement>;
    Text: RichEditorExtendedType<'CustomText', CustomText>;
  }
}

export * from './customTypes';
