import RichEditor from './RichEditor';
import * as plugins from './RichEditor/plugins';

export * from './@types';

export default RichEditor;
export type { RichEditorProps } from './RichEditor';
export { BlockFormatEnum, InlineFormatEnum } from './RichEditor/baseTypes';

export { default as Toolbar } from './RichEditor/components/Toolbar';
export type { IToolbarProps, IToolbarButtonProps } from './RichEditor/components/Toolbar/types';

export { plugins };
export * from './RichEditor/plugins';
export type {
  BlockElementDescription,
  InlineElementDescription,
  ElementsPlugin,
  UtilityOnlyPlugin,
  RichEditorPlugin,
} from './RichEditor/plugins/types';

export type {
  AdmonitionElement,
  AdmonitionTitleElement,
  AdmonitionContentElement,
} from './RichEditor/plugins/elements/admonitionPlugin/types';
export type { AlignElement } from './RichEditor/plugins/elements/alignPlugin/types';
export type { BlockquoteElement } from './RichEditor/plugins/elements/blockquotePlugin/types';
export type { CarouselElement } from './RichEditor/plugins/elements/carouselPlugin/types';
export type { CodeElement, CodeLineElement } from './RichEditor/plugins/elements/codePlugin/types';
export type { DividerElement } from './RichEditor/plugins/elements/dividerPlugin/types';
export type { ImageElement } from './RichEditor/plugins/elements/imagesPlugin/types';
export type { LinkElement } from './RichEditor/plugins/elements/linksPlugin/types';
export type { ListElement, ListItemElement } from './RichEditor/plugins/elements/listsPlugin/types';
export type { TableElement, TableRowElement, TableCellElement } from './RichEditor/plugins/elements/tablePlugin/types';
export type { HeadingElement } from './RichEditor/plugins/elements/textStylePlugin/types';
export type { TooltipElement } from './RichEditor/plugins/elements/tooltipPlugin/types';

export { default as Converter } from './Converter';
export type {
  ConvertDecoration,
  MdastNodeType,
  TRemarkToSlateConverter,
  TElementToRemarkConverter,
  TextToRemarkConvertorInfo,
  ElementToRemarkConvertorInfo,
  ElementConvertors,
  FromRemarkConverters,
  ToRemarkConverters,
  Converters,
} from './Converter/types';

export * from 'slate';
export * from 'slate-react';

export { default as editorDialog, overrideDialog } from './dialog';
export * from './RichEditor/helpers';

export { default as Carousel } from './RichEditor/plugins/elements/carouselPlugin/elements/Carousel';
export type { CarouselRenderElementProps } from './RichEditor/plugins/elements/carouselPlugin/elements/Carousel';

export { default as Image } from './RichEditor/plugins/elements/imagesPlugin/elements/Image';
export type { ImageRenderElementProps } from './RichEditor/plugins/elements/imagesPlugin/elements/Image';

export { default as Link } from './RichEditor/plugins/elements/linksPlugin/elements/Link';
export { default as showLinkPrompt } from './RichEditor/plugins/elements/linksPlugin/showLinkPrompt';
export type { LinkRenderElementProps } from './RichEditor/plugins/elements/linksPlugin/elements/Link';

export type { AnchorElement } from './RichEditor/plugins/elements/anchorPlugin/types';

export type { InlineMenuOption } from './RichEditor/inlineMenuExtension/types';
