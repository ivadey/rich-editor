#!/usr/bin/env sh

rollup -c &&
npx tailwindcss -i src/styles/elements.css -o styles/elements.css &&
cp src/styles/lightPalette.css styles/lightPalette.css &&
cp src/styles/darkPalette.css styles/darkPalette.css
