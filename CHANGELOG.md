# Rich Editor changelog

Разделы:
- `Added` – новый функционал и возможности
- `Breaking changes` – изменения, несовместимые с предыдущими версиями
- `Changed` – изменения в текущем функционале, возможностях, компонентах, логике, которые совместимы с предыдущей версией
- `Removed` – удаление каких либо возможностей, компонентов, логики
- `Fixed` – исправление ошибок
- `Other` – все, что не вписывается ни в одну из перечисленных категорий

## v0.7.0 – 2023-01-08

### Changed

- Rollback to build just with tsc
- Make public
